//
//  UserProfileView.swift
//  Inspiry
//
//  Created by Alexander Danilyak on 19/07/16.
//  Copyright © 2016 Inspiry. All rights reserved.
//

import Foundation
import UIKit
import AlamofireImage 

enum LayoutType {
    case list
    case grid
}

protocol LayoutSwitchProtocol {
    func switchTo(type: LayoutType)
}

// General view for displaying profile
class UserProfileView: UIView, UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    var layoutType: LayoutType = .list
    
    var user: User?
    var userStatistics: UserStatistics?
    var socialNetworks: [SocialNetwork] = LinkedSocialNetworks.allNetworksNotLinked()
    var isStoredUser: Bool = false
    
    var delegate: LayoutSwitchProtocol?
    
    @IBOutlet var contentView: UIView!
    @IBOutlet weak var userImage: UIImageView!
    @IBOutlet weak var userCoverImage: UIImageView!
    @IBOutlet weak var userFullname: UILabel!
    @IBOutlet weak var userStatus: UILabel!
    @IBOutlet weak var edit: UIButton!
    @IBOutlet weak var more: UIButton!
    
    @IBOutlet weak var linkedSocialCollectionView: UICollectionView!
    @IBOutlet weak var statsCollectionView: UICollectionView!
    @IBOutlet weak var linkedSocialLayout: UICollectionViewFlowLayout!
    @IBOutlet weak var statsLayout: UICollectionViewFlowLayout!
    
    @IBOutlet weak var listImage: UIImageView!
    @IBOutlet weak var listLabel: UILabel!
    @IBOutlet weak var listContainer: UIView!
    
    @IBOutlet weak var gridImage: UIImageView!
    @IBOutlet weak var gridLabel: UILabel!
    @IBOutlet weak var gridContainer: UIView!
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.commonInit()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        self.commonInit()
    }
    
    private func commonInit() {
        Bundle.main.loadNibNamed("UserProfileView", owner: self, options: nil)
        guard let content = contentView else { return }
        content.frame = self.bounds
        content.autoresizingMask = [.flexibleHeight, .flexibleWidth]
        self.addSubview(content)
        
        let socialCellNib = UINib(nibName: "SocialCollectionViewCell", bundle:nil)
        self.linkedSocialCollectionView.register(socialCellNib, forCellWithReuseIdentifier: socialCollectionCellId)
        self.linkedSocialCollectionView.scrollsToTop = false
        
        let statsCellNib = UINib(nibName: "StatsCollectionViewCell", bundle:nil)
        self.statsCollectionView.register(statsCellNib, forCellWithReuseIdentifier: statsCollectionCellId)
        self.statsCollectionView.scrollsToTop = false

        self.userImage.layer.cornerRadius = 13
        self.userImage.layer.masksToBounds = true
        self.userCoverImage.layer.masksToBounds = true
        
        let tapUserImage = UITapGestureRecognizer.init(target: self, action: #selector(onUserImage))
        self.userImage.addGestureRecognizer(tapUserImage)
        self.userImage.isUserInteractionEnabled = true

        self.listLabel.text = Localize(string: "listLayout")
        self.gridLabel.text = Localize(string: "gridLayout")
        
        let tapList = UITapGestureRecognizer.init(target: self, action: #selector(onList))
        self.listContainer.addGestureRecognizer(tapList)
        self.listContainer.isUserInteractionEnabled = true
        
        let tapGrid = UITapGestureRecognizer.init(target: self, action: #selector(onGrid))
        self.gridContainer.addGestureRecognizer(tapGrid)
        self.gridContainer.isUserInteractionEnabled = true
        
        NotificationCenter.default.addObserver(self, selector: #selector(reload), name: reloadNotification, object: nil)
    }
    
    // MARK: Layout Switch
    
    func onList() {
        self.changeLayout(type: .list)
    }
    
    func onGrid() {
        self.changeLayout(type: .grid)
    }
    
    func changeLayout(type: LayoutType) {
        if(self.layoutType == type) { return }
        self.layoutType = type
        
        self.delegate?.switchTo(type: self.layoutType)
        self.setupLayoutSwitch()
    }
    
    private func setupLayoutSwitch() {
        UIView.animate(withDuration: 0.3) { 
            self.listImage.image = self.layoutType == .list ? #imageLiteral(resourceName: "listOn") : #imageLiteral(resourceName: "listOff")
            self.gridImage.image = self.layoutType == .list ? #imageLiteral(resourceName: "gridOff") : #imageLiteral(resourceName: "gridOn")
            
            self.listLabel.textColor = self.layoutType == .list ? UIHelpers.green() : UIHelpers.gray()
            self.gridLabel.textColor = self.layoutType == .list ? UIHelpers.gray() : UIHelpers.green()
        }
    }
    
    // MARK: Updates
    
    func updateWith(user: User!, isStored: Bool!) {
        self.user = user
        self.isStoredUser = isStored
        
        if(self.isStoredUser) {
            self.socialNetworks = StoredLinkedSocialNetworks.shared.socialNetworks
            self.linkedSocialCollectionView.reloadData()
        }
        
        LinkManager().getSocialNetworks(user: self.user!) { (isSuccess, socialNetworks, errorDescription) in
            if(isSuccess) {
                self.socialNetworks = socialNetworks!
                self.linkedSocialCollectionView.reloadData()
            } else {
                UIHelpers.shared.showErrorAlert(text: errorDescription, isAutoClosable: true)
            }
        }
        
        self.userStatistics = UserStatistics.init(with: self.user!)
        self.statsCollectionView.reloadData()
        
        self.updateUI()
        self.loadImages()
    }
    
    func updateUI() {
        if(self.user == nil) { return }
        
        let firstname = user!.firstname == nil ? "" : user!.firstname!
        let surname = user!.surname == nil ? "" : user!.surname!
        userFullname.text = firstname + " " + surname
        
        if user!.status != nil { userStatus.text = user!.status! }
        
        edit.isHidden = !self.isStoredUser
        more.isHidden = self.isStoredUser
        
        self.linkedSocialCollectionView.reloadData()
        self.statsCollectionView.reloadData()
    }
    
    func loadImages() {
        if(self.user == nil) { return }
        
        if(self.user!.avatar.count > 0) {
            let request = URLRequest.init(url: (PhotoSizeCollection.getBest(photos: self.user!.avatar)!).url)
            UIHelpers.shared.loadImage(for: request, with: avaCompletion)
        } else {
            self.userImage.image = #imageLiteral(resourceName: "profile")
        }
        
        if(self.user!.background.count > 0) {
            let request = URLRequest.init(url: (PhotoSizeCollection.getBest(photos: self.user!.background)!).url)
            UIHelpers.shared.loadImage(for: request, with: backCompletion)
        } else {
            self.userCoverImage.image = #imageLiteral(resourceName: "back")
        }
    }
    
    func backCompletion(isSuccess: Bool, image: UIImage) -> Void {
        self.userCoverImage.image = isSuccess ? image : #imageLiteral(resourceName: "back")
    }
    
    func avaCompletion(isSuccess: Bool, image: UIImage) -> Void {
        self.userImage.image = isSuccess ? image : #imageLiteral(resourceName: "profile")
    }
    
    // MARK: Collection View
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if(collectionView == self.linkedSocialCollectionView) { return self.socialNetworks.count - 1 }
        if(collectionView == self.statsCollectionView) { return self.userStatistics == nil ? 0 : self.userStatistics!.statistics.count }
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if(collectionView == self.linkedSocialCollectionView) {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: socialCollectionCellId, for: indexPath) as! SocialCollectionViewCell
            cell.setup(for: self.socialNetworks[indexPath.row], and: self.isStoredUser)
            return cell
        } else {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: statsCollectionCellId, for: indexPath) as! StatsCollectionViewCell
            
            cell.setup(statistic: self.userStatistics!.statistics[indexPath.row])
            return cell
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        if collectionView == self.linkedSocialCollectionView {
            let count: CGFloat = CGFloat(min(self.socialNetworks.count, 2))
            let width = UIScreen.main.bounds.width / count
            let height: CGFloat = 60.0
            
            return CGSize.init(width: width, height: height)
        } else {
            let count: CGFloat = CGFloat(min(self.userStatistics!.statistics.count, 4))
            let width = UIScreen.main.bounds.width / count
            let height: CGFloat = 60.0
            
            return CGSize.init(width: width, height: height)
        }   
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if collectionView == self.linkedSocialCollectionView {
            let cell = collectionView.cellForItem(at: indexPath) as! SocialCollectionViewCell
            cell.animateSelection()
            onSocialNetwork(index: indexPath.row)
        } else {
            (collectionView.cellForItem(at: indexPath) as! StatsCollectionViewCell).doAction(user: self.user)
        }
    }
    
    private func openSocialNetwork(socialNetwork: SocialNetwork) {
        let urlString = "http://" + socialNetwork.name.rawValue + ".com/" + socialNetwork.username
        Router.getTopMostVisibleController().present(UIHelpers.inAppBrowser(with: urlString),
                                                     animated: true,
                                                     completion: nil)
    }
    
    
    // MARK: On Social Network
    
    func onSocialNetwork(index: Int) {
        if self.isStoredUser {
            if socialNetworks[index].isLinked == true {
                let alert = UIAlertController(title: Localize(string: "mediaAction"), message: nil, preferredStyle: .actionSheet)
                let show = UIAlertAction(title: Localize(string: "mediaShow"), style: .default, handler: { (action) in
                    self.openSocialNetwork(socialNetwork: self.socialNetworks[index])
                })
                let unlink = UIAlertAction(title: Localize(string: "mediaUnlink"), style: .destructive, handler: { (action) in
                    LinkManager().unlinkSocialNetwork(user: self.user!, socialNetworkName: self.socialNetworks[index].name.rawValue, completion: { (isSuccess, errorDescription) in
                        if !isSuccess {
                            UIHelpers.shared.showErrorAlert(text: errorDescription, isAutoClosable: true)
                        } else {
                            self.socialNetworks[index].isLinked = false
                            self.linkedSocialCollectionView.reloadData()
                        }
                    })
                })
                let cancel = UIAlertAction(title: Localize(string: "mediaCancel"), style: .cancel, handler: nil)
                
                alert.addAction(show)
                alert.addAction(unlink)
                alert.addAction(cancel)
                
                Router.getTopMostVisibleController().present(alert, animated: true, completion: nil)
                
            } else {
                LinkManager().linkSocialNetwork(socialNetworkName: self.socialNetworks[index].name.rawValue, completion: { isSuccess, errorDescription in
                    UIHelpers.shared.closeAlert()
                })
            }
        } else {
            if socialNetworks[index].isLinked == false { return }
            self.openSocialNetwork(socialNetwork: self.socialNetworks[index])
        }
    }
    
    // MARK: Reload
    
    func reload() {
        if isStoredUser { self.socialNetworks = StoredLinkedSocialNetworks.shared.socialNetworks }
        self.linkedSocialCollectionView.reloadData()
    }
    
    // MARK: On User Image
    
    func onUserImage() {
        if(self.userImage.image == nil) { return }
        if(self.userImage.image!.isEqual(#imageLiteral(resourceName: "profile"))) { return }
        
        let tripStoryboard: UIStoryboard? = UIStoryboard(name: "Trip", bundle: nil)
        let zoomImageController: ZoomImageController = tripStoryboard?.instantiateViewController(withIdentifier: "zoomImage") as! ZoomImageController
        
        zoomImageController.image = self.userImage.image
        
        Router.getTopMostVisibleController().present(zoomImageController, animated: true, completion: nil)
    }
}
