//
//  SocialCollectionViewCell.swift
//  Inspiry
//
//  Created by Alexander Danilyak on 23/08/16.
//  Copyright © 2016 Inspiry. All rights reserved.
//

import Foundation
import UIKit

let socialCollectionCellId = "socialCollectionCellId"

class SocialCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var socialImageView: UIImageView!
    @IBOutlet weak var linkLabel: UILabel!
    @IBOutlet weak var linkedImageView: UIImageView!
    
    func setup(for network: SocialNetwork, and isStored: Bool) {
        let type: SocialNetworkName = network.name
        switch type {
        case .instagram:
            self.socialImageView.image = #imageLiteral(resourceName: "instagramIcon")
            break
        case .twitter:
            self.socialImageView.image = #imageLiteral(resourceName: "twitterIcon")
            break
        case .facebook:
            self.socialImageView.image = #imageLiteral(resourceName: "facebookIcon")
            break
        }
        
        let username = network.username == nil ? "" : network.username!
        let notLinkedText = isStored ? Localize(string: "linkSocialLink") : Localize(string: "linkSocialNotLinked")
        
        self.linkLabel.text = network.isLinked == true ? username : notLinkedText
        self.linkedImageView.isHidden = !network.isLinked
        self.linkLabel.textColor = network.isLinked == true ? UIHelpers.green() : UIHelpers.gray()
    }
    
    func animateSelection() {
        DispatchQueue.main.async {
            UIView.animate(withDuration: 0.3, animations: {
                self.backgroundColor = UIHelpers.green(alpha: 0.2)
            }, completion: { finished in
                UIView.animate(withDuration: 0.3, animations: {
                    self.backgroundColor = UIColor.white
                })
            })
        }
    }
}
