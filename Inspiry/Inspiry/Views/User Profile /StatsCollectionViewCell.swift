//
//  StatsCollectionViewCell.swift
//  Inspiry
//
//  Created by Alexander Danilyak on 23/08/16.
//  Copyright © 2016 Inspiry. All rights reserved.
//

import Foundation
import UIKit

let statsCollectionCellId = "statsCollectionCellId"

class StatsCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var countLabel: UILabel!
    @IBOutlet weak var nameLabel: UILabel!
    
    var statistic: Statistic?
    
    func setup(statistic: Statistic) {
        self.statistic = statistic
        self.countLabel.text = String(statistic.count)
        self.nameLabel.text = Localize(string: statistic.type.rawValue)
    }
    
    func doAction(user: User?) {
        if(self.statistic == nil) { return }
        if(user == nil) { return }
        
        let mainStoryboard: UIStoryboard? = UIStoryboard(name: "Main", bundle: nil)
        let controller = mainStoryboard?.instantiateViewController(withIdentifier: statistic!.type.rawValue)
        (controller as! FFNController).user = user
        
        Router.getTopNavigationController().pushViewController(controller!, animated: true)
    }
}
