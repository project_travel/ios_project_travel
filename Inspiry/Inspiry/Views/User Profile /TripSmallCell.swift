//
//  TripSmallCell.swift
//  Inspiry
//
//  Created by Alexander Danilyak on 25/08/16.
//  Copyright © 2016 Inspiry. All rights reserved.
//

import Foundation
import UIKit
import AlamofireImage

class TripSmallCell: UICollectionViewCell {
    
    @IBOutlet weak var tripCoverImage: UIImageView!
    
    var trip: Trip? = nil
    
    func setAll(with trip: Trip!) {
        self.trip = trip
        
        self.tripCoverImage.layer.cornerRadius = 13
        self.tripCoverImage.layer.masksToBounds = true
        
        loadCover()
    }
    
    private func loadCover() {
        if (self.trip!.photos.count == 0) {
            self.tripCoverImage.image = #imageLiteral(resourceName: "noImage")
            return
        }
        
        let best = PhotoWrap.getBestQuality(photos: trip!.photos)
        let request = URLRequest.init(url: best!.url)
        
        self.tripCoverImage.layer.cornerRadius = 13
        self.tripCoverImage.layer.masksToBounds = true
        
        UIHelpers.shared.loadImage(for: request, with: completion)
    }
    
    func completion(isSuccess: Bool, image: UIImage) -> Void {
        self.tripCoverImage.image = isSuccess ? image : #imageLiteral(resourceName: "noImage")
    }
    
    override func prepareForReuse() {
        self.tripCoverImage.image = #imageLiteral(resourceName: "noImage")
    }
}
