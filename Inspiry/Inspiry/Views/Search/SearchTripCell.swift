//
//  SearchTripCell.swift
//  Inspiry
//
//  Created by Alexander Danilyak on 19/09/16.
//  Copyright © 2016 Inspiry. All rights reserved.
//

import Foundation
import UIKit
import AlamofireImage

class SearchTripCell: UITableViewCell {
    @IBOutlet weak var containerView: RoundedView!
    @IBOutlet weak var tripCoverImage: UIImageView!
    @IBOutlet weak var authorImage: UIImageView!
    @IBOutlet weak var tripNameLabel: UILabel!
    @IBOutlet weak var likesCountLabel: UILabel!
    @IBOutlet weak var likeButton: UIButton!
    @IBOutlet weak var authorUsernameLabel: UILabel!
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var labelsContainer: UIView!
    @IBOutlet weak var moreButton: UIButton!
    
    var trip: Trip? = nil
    var authorUser: User? = nil
    
    func setAll(with trip: Trip!) {
        self.trip = trip
        
        self.tripCoverImage.layer.cornerRadius = 13
        self.tripCoverImage.layer.masksToBounds = true
        
        self.authorImage.layer.cornerRadius = 6
        self.authorImage.layer.masksToBounds = true
        
        let tapOnCount = UITapGestureRecognizer.init(target: self, action: #selector(likeButtonPressed(_:)))
        self.likesCountLabel.isUserInteractionEnabled = true
        self.likesCountLabel.addGestureRecognizer(tapOnCount)
        
        self.labelsContainer.layer.cornerRadius = 13
        self.labelsContainer.layer.masksToBounds = true
        
        let tapOnUsername = UITapGestureRecognizer.init(target: self, action: #selector(onUsernameTapped))
        let tapOnUserImage = UITapGestureRecognizer.init(target: self, action: #selector(onUsernameTapped))
        self.authorUsernameLabel.addGestureRecognizer(tapOnUsername)
        self.authorImage.addGestureRecognizer(tapOnUserImage)
        self.authorUsernameLabel.isUserInteractionEnabled = true
        self.authorImage.isUserInteractionEnabled = true
        
        loadCover()
        setupLabels()
        loadAuthor()
        setupLike()
    }
    
    private func loadCover() {
        if (self.trip!.photos.count == 0) {
            self.tripCoverImage.image = #imageLiteral(resourceName: "noImage")
            return
        }
        
        let best = PhotoWrap.getBestQuality(photos: trip!.photos)
        let request = URLRequest.init(url: best!.url)
        
        self.tripCoverImage.layer.cornerRadius = 13
        self.tripCoverImage.layer.masksToBounds = true
        
        UIHelpers.shared.loadImage(for: request, with: coverCompletion)
    }
    
    private func setupLabels() {
        self.tripNameLabel.text = trip!.name
        self.authorUsernameLabel.text = trip!.owners[0]
        self.dateLabel.text = durationLabel(from: trip!.start, to: trip!.end)
    }
    
    private func loadAuthor() {
        UserManager().getUser(with: trip!.owners[0], completion: authorLoadedCompletion)
    }
    
    private func authorLoadedCompletion(isSuccess: Bool, errorDescription: String, authorUser: User?) -> Void {
        if(isSuccess) {
            self.authorUser = authorUser!
            if(self.authorUser!.avatar.count > 0) {
                let url = PhotoSizeCollection.getBest(photos: self.authorUser!.avatar)!.url
                self.loadAuthorImage(url: url)
            }
        } else {
            print("[ERROR] can't load user " + trip!.owners[0])
        }
    }
    
    private func loadAuthorImage(url: URL) {
        let request = URLRequest.init(url: url)
        UIHelpers.shared.loadImage(for: request, with: avaCompletion)
    }
    
    private func setupLike() {
        let likeImage = self.trip!.isLikedByMe == true ? #imageLiteral(resourceName: "likeOn") : #imageLiteral(resourceName: "likeOff")
        self.likeButton.setImage(likeImage, for: .normal)
        
        self.likesCountLabel.text = String(self.trip!.likes)
    }
    
    @IBAction func likeButtonPressed(_ sender: AnyObject) {
        trip!.isLikedByMe = !trip!.isLikedByMe
        trip!.likes = trip?.isLikedByMe ==  true ? trip!.likes + 1 : trip!.likes - 1
        
        TripManager().setLike(with: trip!.isLikedByMe, and: trip!.id) { (isSuccess, errorDescription) in
            if(!isSuccess) {
                self.trip!.isLikedByMe = !self.trip!.isLikedByMe
                self.trip!.likes = self.trip!.isLikedByMe ==  true ? self.trip!.likes + 1 : self.trip!.likes - 1
                UIHelpers.shared.showErrorAlert(text: errorDescription, isAutoClosable: true)
                self.setupLike()
            }
        }
        
        setupLike()
    }
    
    override func prepareForReuse() {
        self.tripCoverImage.image = #imageLiteral(resourceName: "noImage")
        self.authorImage.image = #imageLiteral(resourceName: "profile")
    }
    
    // MARK: Navigation
    
    func onUsernameTapped() {
        if authorUser == nil { return }
        let profileController = UIHelpers.getProfileController(with: self.authorUser!)
        if profileController == nil { return }
        Router.getTopNavigationController().pushViewController(profileController!, animated: true)
    }
    
    @IBAction func onMore(_ sender: AnyObject) {
        let searchController = Router.getTopMostVisibleController() as! GlobalSearchController
        UIHelpers.onMore(moreType: .trip, data: trip!.id, completion: searchController.addToBlackListCompletion)
    }
    
    
    func avaCompletion(isSuccess: Bool, image: UIImage) -> Void {
        self.authorImage.image = isSuccess ? image : #imageLiteral(resourceName: "profile")
    }
    
    func coverCompletion(isSuccess: Bool, image: UIImage) -> Void {
        self.tripCoverImage.image = isSuccess ? image : #imageLiteral(resourceName: "noImage")
    }
}
