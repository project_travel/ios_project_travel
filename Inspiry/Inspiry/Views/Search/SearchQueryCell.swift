//
//  SearchQueryCell.swift
//  Inspiry
//
//  Created by Alexander Danilyak on 17/09/16.
//  Copyright © 2016 Inspiry. All rights reserved.
//

import Foundation
import UIKit

protocol SearchCellProtocol {
    func tableViewUpdates(isBegin: Bool)
    func searchBarEditining(isStart: Bool)
    func onChooseCityCountry(type: ChooseType)
    func onSearch()
    func onTypeChanged(newType: SearchType)
}

class SearchQueryCell: UITableViewCell, UISearchBarDelegate, UITextFieldDelegate {
    var searchQueryData: SearchQueryData?
    
    var type: SearchType = .trips
    var delegate: SearchCellProtocol?
    let startDatePicker = UIDatePicker()
    let endDatePicker = UIDatePicker()
    var forceToOpenStart: Bool = true
    var forceToOpenEnd: Bool = true
    
    @IBOutlet weak var searchBar: UISearchBar!
    @IBOutlet weak var searchTypeSegment: UISegmentedControl!
    
    @IBOutlet weak var countryLabel: UILabel!
    @IBOutlet weak var cityLabel: UILabel!
    @IBOutlet weak var datesLabel: UILabel!
    
    @IBOutlet weak var chosenCountryLabel: UILabel!
    @IBOutlet weak var chosenCityLabel: UILabel!
    @IBOutlet weak var startDateTextField: UITextField!
    @IBOutlet weak var endDateTextField: UITextField!
    
    @IBOutlet weak var searchButton: UIButton!
    
    @IBOutlet weak var segmentToBottom: NSLayoutConstraint!
    @IBOutlet weak var tripAdditionalsContainer: UIView!
    @IBOutlet var tripAdditionalsContainerConstraints: [NSLayoutConstraint]!
    
    func setup() {
        self.searchTypeSegment.setTitle(Localize(string: "searchScopeTitleTrips"), forSegmentAt: 0)
        self.searchTypeSegment.setTitle(Localize(string: "searchScopeTitleUsers"), forSegmentAt: 1)
        self.setSearchBarPlaceholder()
        self.searchBar.delegate = self
        self.setupLabels()
        self.setupTextFields()
        self.setupButton()
        self.checkDataAndSetButton()
        self.setFonts()
    }
    
    func setFonts() {
        self.searchTypeSegment.setTitleTextAttributes([NSFontAttributeName : UIHelpers.searchQueryFont()], for: .normal)
        self.countryLabel.font = UIHelpers.searchQueryFont()
        self.cityLabel.font = UIHelpers.searchQueryFont()
        self.datesLabel.font = UIHelpers.searchQueryFont()
        self.searchButton.titleLabel?.font = UIHelpers.searchQueryFont()
    }
    
    @IBAction func typeChanged(_ sender: AnyObject) {
        self.delegate?.tableViewUpdates(isBegin: true)
        self.type = SearchType(rawValue: self.searchTypeSegment.selectedSegmentIndex)!
        self.showAdditionals(show: self.type == .trips)
        self.setSearchBarPlaceholder()
        self.delegate?.onTypeChanged(newType: self.type)
        self.delegate?.tableViewUpdates(isBegin: false)
    }
    
    private func showAdditionals(show: Bool) {
        for constraint in self.tripAdditionalsContainerConstraints {
            constraint.isActive = show
        }
        self.segmentToBottom.isActive = !show
        self.tripAdditionalsContainer.isHidden = !show
    }
    
    private func setSearchBarPlaceholder() {
        self.searchBar.placeholder = self.type == .users ? Localize(string: "searchPlaceholderUsers") : Localize(string: "searchPlaceholderTrips")
    }
    
    private func setupButton() {
        let titleSearch = Localize(string: "findTrips")
        self.searchButton.setTitle(titleSearch, for: .normal)
        self.searchButton.layer.cornerRadius = 5.0
        self.searchButton.layer.masksToBounds = true
        self.searchButton.layer.borderWidth = 1.0
        self.searchButton.layer.borderColor = UIHelpers.green().cgColor
        self.searchButton.tintColor = UIHelpers.green()
    }
    
    private func setupLabels() {
        self.countryLabel.text = Localize(string: "searchCountry")
        self.cityLabel.text = Localize(string: "searchCity")
        self.datesLabel.text = Localize(string: "searchDates")
        
        let tapOnCountry = UITapGestureRecognizer(target: self, action: #selector(onChooseCountry))
        self.chosenCountryLabel.addGestureRecognizer(tapOnCountry)
        self.chosenCountryLabel.isUserInteractionEnabled = true
        
        let tapOnCity = UITapGestureRecognizer(target: self, action: #selector(onChooseCity))
        self.chosenCityLabel.addGestureRecognizer(tapOnCity)
        self.chosenCityLabel.isUserInteractionEnabled = true
        
        self.chosenCountryLabel.text = self.searchQueryData!.country == nil ? Localize(string: "searchCountryPlaceholder") : self.searchQueryData!.country!
        self.chosenCityLabel.text = self.searchQueryData!.city == nil ? Localize(string: "searchCityPlaceholder") : self.searchQueryData!.city!
    }
    
    private func setupTextFields() {
        self.startDateTextField.placeholder = Localize(string: "searchStartDate")
        self.endDateTextField.placeholder = Localize(string: "searchEndDate")
        self.startDateTextField.text = self.searchQueryData!.start == nil ? "" : self.searchQueryData!.start!.tdDayMonthYear
        self.endDateTextField.text = self.searchQueryData!.end == nil ? "" : self.searchQueryData!.end!.tdDayMonthYear
        
        self.startDateTextField.tintColor = UIColor.clear
        self.startDateTextField.delegate = self
        startDatePicker.backgroundColor = UIColor.white
        startDatePicker.datePickerMode = .date
        if self.searchQueryData!.start != nil { startDatePicker.setDate(self.searchQueryData!.start!, animated: false) }
        startDatePicker.addTarget(self, action: #selector(onStartDateChanged), for: .valueChanged)
        self.startDateTextField.inputView = startDatePicker
        
        self.endDateTextField.tintColor = UIColor.clear
        self.endDateTextField.delegate = self
        endDatePicker.backgroundColor = UIColor.white
        endDatePicker.datePickerMode = .date
        if self.searchQueryData!.end != nil { endDatePicker.setDate(self.searchQueryData!.end!, animated: false) }
        endDatePicker.addTarget(self, action: #selector(onEndDateChanged), for: .valueChanged)
        self.endDateTextField.inputView = endDatePicker
    }
    
    func onStartDateChanged() {
        self.startDateTextField.text = startDatePicker.date.tdDayMonthYear
        self.searchQueryData!.start = self.startDatePicker.date
        self.endDatePicker.minimumDate = self.startDatePicker.date
        self.checkDataAndSetButton()
    }
    
    func onEndDateChanged() {
        self.endDateTextField.text = endDatePicker.date.tdDayMonthYear
        self.searchQueryData!.end = self.endDatePicker.date
        self.startDatePicker.maximumDate = self.endDatePicker.date
        self.checkDataAndSetButton()
    }
    
    // MARK: Button
    
    func checkDataAndSetButton() {
        // Always enabled, if request is empty - show all
        self.setSearchButtonEnabled(to: true)
    }
    
    func setSearchButtonEnabled(to enabled: Bool) {
        self.searchButton.isEnabled = enabled
        let color = enabled ? UIHelpers.green() : UIHelpers.gray()
        self.searchButton.layer.borderColor = color.cgColor
        self.searchButton.tintColor = color
    }
    
    // MARK: Actions
    
    func onChooseCountry() {
        self.delegate?.onChooseCityCountry(type: .country)
    }
    
    func onChooseCity() {
        self.delegate?.onChooseCityCountry(type: .city)
    }
    
    // MARK: Text Field
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        if(self.forceToOpenEnd && textField == self.endDateTextField) {
            self.forceToOpenEnd = textField == self.endDateTextField ? false : self.forceToOpenEnd
            return true
        }
        
        if(self.forceToOpenStart && textField == self.startDateTextField) {
            self.forceToOpenStart = textField == self.startDateTextField ? false : self.forceToOpenStart
            return true
        }
        
        self.openAlertController(isStartTextField: self.startDateTextField == textField)
        return false
    }
    
    private func openAlertController(isStartTextField: Bool) {
        let alertController = UIAlertController.init(title: Localize(string: "chooseActionTitle"), message: nil, preferredStyle: .actionSheet)
        
        let change = UIAlertAction.init(title: Localize(string: "chooseChange"), style: .default, handler: { action in
            self.forceToOpenStart = isStartTextField ? true : self.forceToOpenStart
            self.forceToOpenEnd = isStartTextField ? self.forceToOpenEnd : true
            _ = isStartTextField ? self.startDateTextField.becomeFirstResponder() : self.endDateTextField.becomeFirstResponder()
        })
        
        let clear = UIAlertAction.init(title: Localize(string: "chooseClear"), style: .destructive, handler: { action in
            if(isStartTextField) {
                self.searchQueryData?.start = nil
                self.startDateTextField.text = ""
                self.forceToOpenStart = true
                self.endDatePicker.minimumDate = nil
            } else {
                self.searchQueryData?.end = nil
                self.endDateTextField.text = ""
                self.forceToOpenEnd = true
                self.startDatePicker.maximumDate = nil
            }
        })
        
        let cancel = UIAlertAction.init(title: Localize(string: "chooseCancel"), style: .cancel, handler: nil)
        
        alertController.addAction(change)
        alertController.addAction(clear)
        alertController.addAction(cancel)
        
        Router.getTopMostVisibleController().present(alertController, animated: true, completion: nil)
    }
    
    // MARK: Search Bar
    
    func searchBarShouldBeginEditing(_ searchBar: UISearchBar) -> Bool {
        self.delegate?.searchBarEditining(isStart: true)
        self.searchBar.showsCancelButton = true
        return true
    }
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        if(self.type == .trips) {
            self.searchQueryData!.name = searchText
        } else {
            self.searchQueryData!.q = searchText
        }
    }
    
    func searchBarShouldEndEditing(_ searchBar: UISearchBar) -> Bool {
        self.delegate?.searchBarEditining(isStart: false)
        self.searchBar.showsCancelButton = false
        return true
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        self.searchBar.endEditing(true)
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        self.delegate?.onSearch()
    }
    
    // MARK: On Search
    
    @IBAction func onSearch(_ sender: AnyObject) {
        self.delegate?.onSearch()
    }
}
