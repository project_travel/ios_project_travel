//
//  ChooseCountryCityCell.swift
//  Inspiry
//
//  Created by Alexander Danilyak on 18/09/16.
//  Copyright © 2016 Inspiry. All rights reserved.
//

import Foundation
import UIKit

class ChooseCountryCityCell: UITableViewCell {
    @IBOutlet weak var itemNameLabel: UILabel!
    
    func update(with name: String, isChosen: Bool) {
        self.itemNameLabel.text = name
        self.accessoryType = isChosen ? .checkmark : .none
    }
}
