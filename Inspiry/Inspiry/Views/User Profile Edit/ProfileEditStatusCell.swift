//
//  ProfileEditStatusCell.swift
//  Inspiry
//
//  Created by Alexander Danilyak on 31/08/16.
//  Copyright © 2016 Inspiry. All rights reserved.
//

import Foundation
import UIKit

class ProfileEditStatusCell: UITableViewCell {
    @IBOutlet weak var statusLabel: UILabel!
    @IBOutlet weak var counterLabel: UILabel!
    @IBOutlet weak var statusTextView: UITextView!
    @IBOutlet weak var containerView: UIView!
    
    static let maximumCount: Int = 140
    
    func setup() {
        self.containerView.isUserInteractionEnabled = true
        self.containerView.layer.cornerRadius = 13
        self.containerView.layer.borderWidth = 1
        self.setBorderColor(color: UIHelpers.gray())
        
        self.statusLabel.text = Localize(string: "editStatus")
        self.statusTextView.text = StoredUser.shared.user!.status
        self.statusTextView.returnKeyType = .next
        
        self.updateCounter()
    }
    
    func setBorderColor(color: UIColor) {
        self.containerView.layer.borderColor = color.cgColor
    }
    
    func updateCounter() {
        let count: Int = self.statusTextView.text.characters.count
        self.counterLabel.text = String("\(count)/\(ProfileEditStatusCell.maximumCount)")
        
        self.counterLabel.textColor = count > ProfileEditStatusCell.maximumCount ? UIHelpers.destructiveColor() : UIHelpers.gray()
    }
}
