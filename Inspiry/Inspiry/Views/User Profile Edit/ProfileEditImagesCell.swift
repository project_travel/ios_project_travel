//
//  ProfileEditImagesCell.swift
//  Inspiry
//
//  Created by Alexander Danilyak on 31/08/16.
//  Copyright © 2016 Inspiry. All rights reserved.
//

import Foundation
import UIKit
import AlamofireImage

enum ChoseImageType {
    case avatar
    case cover
    case notset
}

class ProfileEditImagesCell: UITableViewCell, UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    @IBOutlet weak var avatarContainer: RoundedView!
    @IBOutlet weak var coverContainer: RoundedView!
    @IBOutlet weak var avatarImageView: UIImageView!
    @IBOutlet weak var coverImageView: UIImageView!
    
    var choseImageType: ChoseImageType = .notset
    var delegate: EditSaveButtonProtocol?
    
    func setup() {
        self.avatarImageView.layer.cornerRadius = 13
        self.avatarImageView.layer.masksToBounds = true
        self.avatarImageView.image = #imageLiteral(resourceName: "profile")
        
        self.coverImageView.layer.cornerRadius = 13
        self.coverImageView.layer.masksToBounds = true
        self.coverImageView.image = #imageLiteral(resourceName: "back")
        
        self.loadImages()
        
        self.imagePickersSetup()
    }
    
    func loadImages() {
        let user = StoredUser.shared.user
        
        if(user == nil) { return }
        
        if(user!.avatar.count > 0) {
            let request = URLRequest.init(url: (PhotoSizeCollection.getBest(photos: user!.avatar)!).url)
            UIHelpers.shared.loadImage(for: request, with: avaCompletion)
        }
        
        if(user!.background.count > 0) {
            let request = URLRequest.init(url: (PhotoSizeCollection.getBest(photos: user!.background)!).url)
            UIHelpers.shared.loadImage(for: request, with: backCompletion)
        }
    }
    
    func avaCompletion(isSuccess: Bool, image: UIImage) -> Void {
        self.avatarImageView.image = isSuccess ? image : #imageLiteral(resourceName: "profile")
    }
    
    func backCompletion(isSuccess: Bool, image: UIImage) -> Void {
        self.coverImageView.image = isSuccess ? image : #imageLiteral(resourceName: "back")
    }
    
    // MARK: Image Picker Routines
    
    func imagePickersSetup() {
        let tapAvatar = UITapGestureRecognizer(target: self, action: #selector(onChoseAvatar))
        self.avatarImageView.addGestureRecognizer(tapAvatar)
        self.avatarImageView.isUserInteractionEnabled = true
        
        let tapCover = UITapGestureRecognizer(target: self, action: #selector(onChoseCover))
        self.coverImageView.addGestureRecognizer(tapCover)
        self.coverImageView.isUserInteractionEnabled = true
    }
    
    func onChoseAvatar() {
        self.choseImageType = .avatar
        self.doChoseProfilePictureRoutine()
    }
    
    func onChoseCover() {
        self.choseImageType = .cover
        self.doChoseProfilePictureRoutine()
    }
    
    func doChoseProfilePictureRoutine() {
        let imagePicker = UIImagePickerController()
        
        let alert = UIAlertController(title: Localize(string: "profilePictureChoseSourceTitle"), message: nil, preferredStyle: .actionSheet)
        let camera = UIAlertAction(title: Localize(string: "profilePictureChoseCamera"), style: .default, handler: { action in
            imagePicker.sourceType = .camera
            imagePicker.cameraCaptureMode = .photo
            Router.getTopNavigationController().present(imagePicker, animated: true) {}
        })
        
        let library = UIAlertAction(title: Localize(string: "profilePictureChoseLibrary"), style: .default, handler: { action in
            imagePicker.sourceType = .photoLibrary
            Router.getTopNavigationController().present(imagePicker, animated: true) {}
        })
        
        let cancel = UIAlertAction(title: Localize(string: "editCancel"), style: .cancel, handler: { action in
        })
        
        let isClearNeeded = self.choseImageType == .cover ? StoredUser.shared.user!.background.count != 0 : StoredUser.shared.user!.avatar.count != 0
        
        let clear = UIAlertAction(title: Localize(string: "profilePictureClear"), style: .destructive, handler: { action in
            if(self.choseImageType == .cover) {
                StoredUserManager().clearCover(completion: self.deleteCompletion)
            } else {
                StoredUserManager().clearAvatar(completion: self.deleteCompletion)
            }
        })
        
        alert.addAction(camera)
        alert.addAction(library)
        alert.addAction(cancel)
        if(isClearNeeded) { alert.addAction(clear) }
        
        imagePicker.allowsEditing = self.choseImageType == .avatar
        imagePicker.delegate = self
        
        Router.getTopNavigationController().present(alert, animated: true) {}
    }
    
    // MARK: Delete completion
    
    func deleteCompletion(isSuccess: Bool, message: String) -> Void {
        UIHelpers.shared.closeAlert()
        
        if isSuccess {
            if(self.choseImageType == .cover) {
                self.coverImageView.image = #imageLiteral(resourceName: "back")
                StoredUser.shared.user!.background = []
            } else {
                self.avatarImageView.image = #imageLiteral(resourceName: "profile")
                StoredUser.shared.user!.avatar = []
            }
            StoredUser.shared.save()
        } else {
            UIHelpers.shared.showErrorAlert(text: message, isAutoClosable: true)
        }
    }
    
    // MARK: Upload completion
    
    func uploadCompletion(isSuccess: Bool, message: String) -> Void {
        UIHelpers.shared.closeAlert()
        
        if isSuccess {
            // nothing
        } else {
            UIHelpers.shared.showErrorAlert(text: message, isAutoClosable: true)
        }
    }
    
    // MARK: Image Picker Delegate
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        UIHelpers.shared.showLoadingAlert(text: Localize(string: "save"))
        
        let imageKey = self.choseImageType == .avatar ? UIImagePickerControllerEditedImage : UIImagePickerControllerOriginalImage
        
        if let pickedImage = info[imageKey] as? UIImage {
            let imageData = UIImagePNGRepresentation(pickedImage)
            let paths = NSSearchPathForDirectoriesInDomains(FileManager.SearchPathDirectory.documentDirectory,
                                                            FileManager.SearchPathDomainMask.userDomainMask,
                                                            true)
            let docs: String = paths[0]
            let fullPath = docs + "/temp.png"
            
            do {
                try _ = imageData?.write(to: URL.init(fileURLWithPath: fullPath))
            } catch {
                // nothing
            }
            
            DispatchQueue.main.async {
                switch self.choseImageType {
                case .avatar:
                    self.avatarImageView.image = pickedImage
                    StoredUserManager().uploadAvatar(file: URL.init(fileURLWithPath: fullPath), completion: self.uploadCompletion)
                    break
                case .cover:
                    self.coverImageView.image = pickedImage
                    StoredUserManager().uploadCover(file: URL.init(fileURLWithPath: fullPath), completion: self.uploadCompletion)
                    break
                default:
                    break
                }
            }
        }
        
        self.delegate?.setSaveButtonEnabled(isEnabled: true)
        Router.getTopNavigationController().dismiss(animated: true, completion: nil)
    }
}
