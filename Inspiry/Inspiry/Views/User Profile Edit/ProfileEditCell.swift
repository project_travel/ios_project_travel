//
//  ProfileEditCell.swift
//  Inspiry
//
//  Created by Alexander Danilyak on 31/08/16.
//  Copyright © 2016 Inspiry. All rights reserved.
//

import Foundation
import UIKit

enum EditCellType {
    case firstname
    case lastname
    case sex
    case birthdate
}

class ProfileEditCell: UITableViewCell, UIPickerViewDelegate, UIPickerViewDataSource {
    @IBOutlet weak var editItemNameLabel: UILabel!
    @IBOutlet weak var editItemTextField: UITextField!
    @IBOutlet weak var containerView: UIView!
    
    let genders: [GenderType] = [.male, .female, .notset]
    let datePicker = UIDatePicker()
    
    func setup(type: EditCellType) {
        self.containerView.isUserInteractionEnabled = true
        self.containerView.layer.cornerRadius = 13
        self.containerView.layer.borderWidth = 1
        self.setBorderColor(color: UIHelpers.gray())
        
        switch type {
        case .firstname:
            self.editItemNameLabel.text = Localize(string: "editFirstname")
            self.editItemTextField.text = StoredUser.shared.user!.firstname
            self.editItemTextField.returnKeyType = .next
            break
            
        case .lastname:
            self.editItemNameLabel.text = Localize(string: "editSurname")
            self.editItemTextField.text = StoredUser.shared.user!.surname
            self.editItemTextField.returnKeyType = .next
            break
            
        case .sex:
            self.editItemNameLabel.text = Localize(string: "editGender")
            self.editItemTextField.tintColor = UIColor.clear
            self.editItemTextField.text = StoredUser.shared.user!.gender == .notset ? Localize(string: "not set") : Localize(string: StoredUser.shared.user!.gender.rawValue)
            let pickerView = UIPickerView()
            pickerView.backgroundColor = UIColor.white
            pickerView.delegate = self
            self.editItemTextField.inputView = pickerView
            break
            
        case .birthdate:
            self.editItemNameLabel.text = Localize(string: "editBirthDate")
            self.editItemTextField.tintColor = UIColor.clear
            self.editItemTextField.text = StoredUser.shared.user!.birthDate == DefaultBirthDate ? Localize(string: "not set") : StoredUser.shared.user!.birthDate?.simple
            self.datePicker.backgroundColor = UIColor.white
            self.datePicker.datePickerMode = .date
            let date = StoredUser.shared.user!.birthDate == DefaultBirthDate ? Date() : StoredUser.shared.user!.birthDate
            self.datePicker.setDate(date!, animated: false)
            self.datePicker.addTarget(self, action: #selector(onDateChanged), for: .valueChanged)
            self.editItemTextField.inputView = datePicker
            break
        }
    }
    
    func setBorderColor(color: UIColor) {
        self.containerView.layer.borderColor = color.cgColor
    }
    
    // MARK: Picker Delegate & Data Source
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return self.genders.count
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        if(self.genders[row] == .notset) {
            return Localize(string: "not set")
        }
        return Localize(string: self.genders[row].rawValue)
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        self.editItemTextField.text = self.genders[row] == .notset ? Localize(string: "not set") : Localize(string: self.genders[row].rawValue)
        self.editItemTextField.sendActions(for: .editingChanged)
    }
    
    // MARK: Date Picker
    
    func onDateChanged() {
        self.editItemTextField.text = self.datePicker.date.simple
        self.editItemTextField.sendActions(for: .editingChanged)
    }
}
