//
//  SettingsChangeCell.swift
//  Inspiry
//
//  Created by Alexander Danilyak on 14/09/16.
//  Copyright © 2016 Inspiry. All rights reserved.
//

import Foundation
import UIKit

enum SettingsChangeType {
    case oldPassword
    case newPassword
    case againPassword
    case newEmail
}

class SettingsChangeCell: UITableViewCell {
    @IBOutlet weak var changeTitle: UILabel!
    @IBOutlet weak var changeTextField: UITextField!
    @IBOutlet weak var containerView: UIView!
    
    func update(with type: SettingsChangeType) {
        self.containerView.isUserInteractionEnabled = false
        
        self.containerView.layer.cornerRadius = 13
        self.containerView.layer.borderWidth = 1
        self.setBorderColor(color: UIHelpers.gray())
        
        switch type {
        case .oldPassword:
            self.changeTextField.isSecureTextEntry = true
            self.changeTitle.text = Localize(string: "oldPasswordPlaceholder")
            break
        case .newPassword:
            self.changeTextField.isSecureTextEntry = true
            self.changeTitle.text = Localize(string: "newPasswordPlaceholder")
            break
        case .againPassword:
            self.changeTextField.isSecureTextEntry = true
            self.changeTitle.text = Localize(string: "newPasswordConfirmPlaceholder")
            break
        case .newEmail:
            self.changeTextField.keyboardType = .emailAddress
            self.changeTitle.text = Localize(string: "newEmailPlaceholder")
        }
    }
    
    func setBorderColor(color: UIColor) {
        self.containerView.layer.borderColor = color.cgColor
    }
}
