//
//  SettingsCell.swift
//  Inspiry
//
//  Created by Alexander Danilyak on 14/09/16.
//  Copyright © 2016 Inspiry. All rights reserved.
//

import Foundation
import UIKit

class SettingsCell: UITableViewCell {
    @IBOutlet weak var settingsIcon: UIImageView!
    @IBOutlet weak var settingsLabel: UILabel!
    
    func update(with icon: UIImage, label: String) {
        self.settingsIcon.image = icon
        self.settingsLabel.text = label
    }
}
