//
//  CommentCell.swift
//  Inspiry
//
//  Created by Alexander Danilyak on 05/08/16.
//  Copyright © 2016 Inspiry. All rights reserved.
//

import Foundation
import MGSwipeTableCell
import UIKit
import AlamofireImage

class CommentCell: MGSwipeTableCell {
    var comment: Comment?
    var author: User?
    
    @IBOutlet weak var profileImageView: UIImageView!
    @IBOutlet weak var usernameLabel: UILabel!
    @IBOutlet weak var commentTextLabel: UILabel!
    @IBOutlet weak var dateLabel: UILabel!
    
    func setAll() {
        self.profileImageView.layer.cornerRadius = 7.0
        self.profileImageView.layer.masksToBounds = true
        
        usernameLabel.text = (comment?.owner)!
        commentTextLabel.text = comment?.text
        dateLabel.text = timeAgoLabel(date: comment!.created)
        
        self.loadAuthor()
    }
    
    override func prepareForReuse() {
        usernameLabel.text = ""
        commentTextLabel.text = ""
        dateLabel.text = ""
        self.profileImageView.image = #imageLiteral(resourceName: "profile")
    }
    
    private func loadAuthor() {
        UserManager().getUser(with: self.comment!.owner, completion: authorLoadedCompletion)
    }
    
    private func authorLoadedCompletion(isSuccess: Bool, errorDescription: String, authorUser: User?) -> Void {
        if(isSuccess) {
            self.author = authorUser!
            if(self.author!.avatar.count > 0) {
                let url = PhotoSizeCollection.getBest(photos: self.author!.avatar)!.url
                self.loadAuthorImage(url: url)
            }
        } else {
            print("[ERROR] can't load user " + self.comment!.owner)
        }
    }
    
    private func loadAuthorImage(url: URL) {
        let request = URLRequest.init(url: url)
        ImageDownloader.default.download(request) { response in
            DispatchQueue.main.async(execute: {
                self.profileImageView.image = response.result.value
            })
            if(response.result.value != nil) {
                UIHelpers.shared.imageCache.add(response.result.value!, for: request)
            }
        }
    }
}
