//
//  TextFiledWithInsets.swift
//  Inspiry
//
//  Created by Alexander Danilyak on 06/09/16.
//  Copyright © 2016 Inspiry. All rights reserved.
//

import Foundation
import UIKit

class TextFieldWithInsets: UITextField {
    override func textRect(forBounds bounds: CGRect) -> CGRect {
        return bounds.insetBy(dx: 2, dy: 1)
    }
    
    override func editingRect(forBounds bounds: CGRect) -> CGRect {
        return bounds.insetBy(dx: 2, dy: 1)
    }
}
