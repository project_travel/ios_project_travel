//
//  UIHelpers.swift
//  Inspiry
//
//  Created by Alexander Danilyak on 27/07/16.
//  Copyright © 2016 Inspiry. All rights reserved.
//

import UIKit
import AlamofireImage

class UIHelpers {
    static let shared: UIHelpers = UIHelpers()
    
    var imageCache = AutoPurgingImageCache()
    
    var alertWindow: UIWindow?
    var mainWindow: UIWindow?
    
    static var isMenuSetupComplete: Bool = false
    
    init() {
        self.alertWindow = UIWindow.init(frame: CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width, height: 20))
        self.alertWindow?.windowLevel = UIWindowLevelStatusBar
        self.alertWindow?.isHidden = false;
        self.alertWindow?.makeKeyAndVisible()
        self.alertWindow?.rootViewController = AlertsController.shared
        
        mainWindow?.makeKeyAndVisible()
    }
    
    static func lightGray() -> UIColor {
        return UIColor.init(red: 235.0 / 255.0,
                            green: 235.0 / 255.0,
                            blue: 241.0 / 255.0,
                            alpha: 1.0)
    }
    
    static func gray() -> UIColor {
        return UIColor.init(red: 190.0 / 255.0,
                            green: 190.0 / 255.0,
                            blue: 190.0 / 255.0,
                            alpha: 1.0)
    }
    
    static func darkGray() -> UIColor {
        return UIColor.init(red: 88.0 / 255.0,
                            green: 88.0 / 255.0,
                            blue: 88.0 / 255.0,
                            alpha: 1.0)
    }
    
    static func green(alpha: CGFloat = 1.0) -> UIColor {
        return UIColor.init(red: 31.0 / 255.0,
                            green: 245.0 / 255.0,
                            blue: 157.0 / 255.0,
                            alpha: alpha)
    }
    
    static func destructiveColor() -> UIColor {
        return UIColor.init(red: 255.0 / 255.0,
                            green: 140.0 / 255.0,
                            blue: 105.0 / 255.0,
                            alpha: 1.0)
    }
    
    static func navBarFont() -> UIFont {
        let size: CGFloat = UIHelpers.isPhone5ScreenSize() || UIHelpers.isPhone4ScreenSize() ? 17.0 : 19.0
        return UIFont.systemFont(ofSize: size, weight: UIFontWeightSemibold)
    }
    
    static func menuCellFont() -> UIFont {
        let size: CGFloat = UIHelpers.isPhone5ScreenSize() || UIHelpers.isPhone4ScreenSize() ? 19.0 : 22.0
        return UIFont.systemFont(ofSize: size, weight: UIFontWeightSemibold)
    }
    
    static func searchQueryFont() -> UIFont {
        let size: CGFloat = UIHelpers.isPhone5ScreenSize() || UIHelpers.isPhone4ScreenSize() ? 15.0 : 19.0
        return UIFont.systemFont(ofSize: size, weight: UIFontWeightSemibold)
    }
    
    static func setupNavBar() {
        let navAppearance = UINavigationBar.appearance()
        navAppearance.tintColor = UIHelpers.green()
        navAppearance.titleTextAttributes = [NSForegroundColorAttributeName: UIHelpers.darkGray(),
                                             NSFontAttributeName: UIHelpers.navBarFont()]
    }
    
    static func getLabel(with text: String) -> UILabel {
        let label: UILabel = UILabel.init(frame: CGRect.init(x: 0, y: 0, width: 200, height: 44))
        label.text = text
        label.font = UIFont.systemFont(ofSize: 16.0, weight: UIFontWeightRegular)
        label.textColor = UIHelpers.lightGray()
        label.textAlignment = .center
        return label
    }
    
    func showLoadingAlert(text: String!) {
        AlertsController.shared.showLoading(text: text)
    }
    
    func showErrorAlert(text: String!, isAutoClosable: Bool) {
        AlertsController.shared.showError(text: text, isAutoClosable: isAutoClosable)
    }
    
    func showNotificationAlert(text: String!, isAutoClosable: Bool) {
        AlertsController.shared.showNotification(text: text, isAutoClosable: isAutoClosable)
    }
    
    func showBigNotification(body: String, userInfo: [NSObject: AnyObject], isAutoClosable: Bool) {
        AlertsController.shared.showBigNotification(body: body, userInfo: userInfo, isAutoClosable: isAutoClosable)
    }
    
    func closeAlert() {
        AlertsController.shared.closeAlert()
    }
    
    //MARK: Screen Size
    
    static func isPhone4ScreenSize() -> Bool {
        return UIScreen.main.bounds.height == 480.0
    }
    
    static func isPhone5ScreenSize() -> Bool {
        return UIScreen.main.bounds.height == 568.0
    }
    
    static func isPhone6ScreenSize() -> Bool {
        return UIScreen.main.bounds.height == 667.0
    }
    
    static func isPhonePlusScreenSize() -> Bool {
        return UIScreen.main.bounds.height == 736.0
    }
    
    //MARK: In App Browser
    
    static func inAppBrowser(with urlString: String) -> UINavigationController {
        let url = URL(string: urlString)!
        return UIHelpers.inAppBrowser(with: url)
    }
    
    static func inAppBrowser(with url: URL) -> UINavigationController {
        let tripStoryboard: UIStoryboard? = UIStoryboard(name: "Trip", bundle: nil)
        let navInAppBrowser = tripStoryboard?.instantiateViewController(withIdentifier: "navInAppBrowser") as! UINavigationController
        (navInAppBrowser.viewControllers[0] as! InAppBrowser).url = url
        return navInAppBrowser
    }
    
    //MARK: Trip More
    
    enum MoreFromType {
        case profile
        case trip
        case post
    }
    
    static func onMore(moreType: MoreFromType, data: String, completion: @escaping (Bool, String) -> Void) {
        
        var title: String, message: String, addTitle: String
        switch moreType {
        case .profile:
            title = Localize(string: "blackListAddUserTitle")
            message = Localize(string: "blackListAddUserMessage")
            addTitle = Localize(string: "blackListAdd")
            break
        default:
            title = Localize(string: "blackListReportContentTitle")
            message = Localize(string: "blackListReportContentMessage")
            addTitle = Localize(string: "blackListReportYes")
            break
        }
        
        let alert = UIAlertController(title: title,
                                      message: message,
                                      preferredStyle: .actionSheet)
        
        let add = UIAlertAction(title: addTitle, style: .destructive) { (action) in
            switch moreType {
            case .profile: BlackListModel.addUserToBlackList(username: data, completion: completion); break
            case .trip: BlackListModel.addTripToBlackList(tripId: data, completion: completion); break
            case .post: BlackListModel.addPostToBlackList(postId: data, completion: completion); break
            }
        }
        
        let cancel = UIAlertAction(title: Localize(string: "blackListCancel"), style: .cancel, handler: nil)
        
        alert.addAction(add)
        alert.addAction(cancel)
        
        Router.getTopMostVisibleController().present(alert, animated: true, completion: nil)
    }
    
    //MARK: Open Profile
    
    static func getProfileController(with user: User, and isMenuButtonNeeded: Bool = false) -> UIViewController? {
        if user.inBlacklist {
            let alert = UIAlertController(title: Localize(string: "blackListUserBlockedTitle"),
                                          message: Localize(string: "blackListUserBlockedMessage"),
                                          preferredStyle: .alert)
            
            let cancel = UIAlertAction(title: Localize(string: "blackListCancel"), style: .cancel, handler: nil)
            
            alert.addAction(cancel)
            
            Router.getTopMostVisibleController().present(alert, animated: true, completion: nil)
            return nil
        }
        
        let mainStoryboard: UIStoryboard? = UIStoryboard(name: "Main", bundle: nil)
        let profileController = mainStoryboard?.instantiateViewController(withIdentifier: "profile") as! ProfileController
        profileController.user = user
        profileController.isMyProfile = user.username == StoredUser.shared.user!.username
        profileController.isMenuButtonNeeded = isMenuButtonNeeded
        return profileController
    }
    
    //MARK: Image Loader
    
    func loadImage(for request: URLRequest, with completion: @escaping (Bool, UIImage) -> Void) {
        var image = imageCache.image(for: request)
        if image == nil {
            ImageDownloader.default.download(request) { response in
                image = response.result.value
                DispatchQueue.main.async {
                    if image != nil {
                        self.imageCache.add(image!, for: request)
                        completion(true, image!)
                    } else {
                        completion(false, UIImage())
                    }
                }
            }
        } else {
            DispatchQueue.main.async { completion(true, image!) }
        }
    }
}
