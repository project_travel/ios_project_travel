//
//  PostCell.swift
//  Inspiry
//
//  Created by Alexander Danilyak on 03/08/16.
//  Copyright © 2016 Inspiry. All rights reserved.
//

import Foundation
import UIKit
import AlamofireImage

class PostWithPhotoCell: UITableViewCell {
    var post: Post? = nil
    
    @IBOutlet weak var photoImageView: UIImageView!
    @IBOutlet weak var containerView: RoundedView!
    
    @IBOutlet weak var textView: UITextView!
    @IBOutlet weak var socialImageView: UIImageView!
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var locationImageView: UIImageView!
    @IBOutlet weak var locationLabel: UILabel!
    @IBOutlet weak var moreButton: UIButton!
    
    func updateWithPost(post: Post) {
        self.post = post
        self.moreButton.isHidden = self.post!.owner == StoredUser.shared.user!.username
        
        self.loadCover()
        self.setData()
    }
 
    private func loadCover() {
        if post == nil { return }
        
        if(post!.photos.count == 0) {
            return
        }
        
        let best = PhotoWrap.getBestQuality(photos: post!.photos)
        let request = URLRequest.init(url: best!.url)
        UIHelpers.shared.loadImage(for: request, with: completion)
    }
    
    func completion(isSuccess: Bool, image: UIImage) -> Void {
        self.photoImageView.image = isSuccess ? image : #imageLiteral(resourceName: "noImage")
    }
    
    private func setData() {
        if post == nil { return }
        
        self.photoImageView.layer.cornerRadius = 13.0
        self.photoImageView.layer.masksToBounds = true
        let tapOnPhoto = UITapGestureRecognizer.init(target: self, action: #selector(onPhoto))
        self.photoImageView.addGestureRecognizer(tapOnPhoto)
        self.photoImageView.isUserInteractionEnabled = true
        
        self.textView.text = self.post!.text
        self.textView.isEditable = false
        self.textView.tintColor = UIHelpers.green()
        self.dateLabel.text = postDateLabel(date: post!.created)
        
        self.socialImageView.image = UIImage.init(named: self.post!.source.type.rawValue + "Icon")
        
        let tapOnScoialImage = UITapGestureRecognizer.init(target: self, action: #selector(onSocial))
        self.socialImageView.addGestureRecognizer(tapOnScoialImage)
        self.socialImageView.isUserInteractionEnabled = true
        let tapOnScoialName = UITapGestureRecognizer.init(target: self, action: #selector(onSocial))
        self.dateLabel.addGestureRecognizer(tapOnScoialName)
        self.dateLabel.isUserInteractionEnabled = true
        
        self.locationImageView.isHidden = self.post!.location == nil
        self.locationLabel.isHidden = self.post!.location == nil
        if(self.post!.location != nil) {
            let locationString: String = String.init(format: "%.1f %.1f", self.post!.location!.lat, self.post!.location!.lon)
            self.locationLabel.text = locationString
        }
    }
    
    override func prepareForReuse() {
        self.textView.text = ""
        self.locationLabel.text = ""
        self.photoImageView.image = #imageLiteral(resourceName: "noImage")
    }
    
    // MARK: Navigation
    
    func onSocial() {
        if self.post == nil { return }
        Router.getTopMostVisibleController().present(UIHelpers.inAppBrowser(with: self.post!.source.url), animated: true, completion: nil)
    }
    
    func onLocation() {
        
    }
    
    func onPhoto() {
        let tripStoryboard: UIStoryboard? = UIStoryboard(name: "Trip", bundle: nil)
        let zoomImageController: ZoomImageController = tripStoryboard?.instantiateViewController(withIdentifier: "zoomImage") as! ZoomImageController
        
        zoomImageController.image = self.photoImageView.image
        zoomImageController.post = self.post!
        
        Router.getTopNavigationController().present(zoomImageController, animated: true, completion: nil)
    }
    
    @IBAction func onMore(_ sender: AnyObject) {
        UIHelpers.onMore(moreType: .post, data: self.post!.id, completion: completion)
    }
    
    func completion(isSuccess: Bool, errorDescription: String) -> Void {
        if isSuccess {
            NotificationCenter.default.post(name: tripUpdateNotification, object: self, userInfo: nil)
            
            let alert = UIAlertController(title: Localize(string: "blackListCompletionTitle"),
                                          message: Localize(string: "blackListCompletionMessage"),
                                          preferredStyle: .actionSheet)
            let cancel = UIAlertAction(title: Localize(string: "ok"), style: .cancel, handler: nil)
            alert.addAction(cancel)
            Router.getTopNavigationController().present(alert, animated: true, completion: nil)
        }
    }
}
