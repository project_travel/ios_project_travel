//
//  FFCell.swift
//  Inspiry
//
//  Created by Alexander Danilyak on 11/09/16.
//  Copyright © 2016 Inspiry. All rights reserved.
//

import Foundation
import UIKit
import AlamofireImage

class FFCell: UITableViewCell {
    var user: User?
    
    @IBOutlet weak var usernameLabel: UILabel!
    @IBOutlet weak var fullnameLabel: UILabel!
    @IBOutlet weak var profileImageView: UIImageView!
    
    func update(with user: User) {
        self.user = user
        
        self.usernameLabel.text = self.user!.username
        let spaceIfNeeded: String = self.user!.firstname!.isEmpty ? "" : " "
        self.fullnameLabel.text = self.user!.firstname! + spaceIfNeeded + self.user!.surname!
        
        self.profileImageView.layer.cornerRadius = 13.0
        self.profileImageView.layer.masksToBounds = true
        
        self.loadProfileImage()
    }
    
    func loadProfileImage() {
        if(self.user!.avatar.count > 0) {
            let url = PhotoSizeCollection.getBest(photos: self.user!.avatar)!.url
            let request = URLRequest.init(url: url)
            UIHelpers.shared.loadImage(for: request, with: completion)
        }
    }
    
    func completion(isSuccess: Bool, image: UIImage) -> Void {
        self.profileImageView.image = isSuccess ? image : #imageLiteral(resourceName: "profile")
    }
    
    override func prepareForReuse() {
        self.usernameLabel.text = ""
        self.fullnameLabel.text = ""
        self.profileImageView.image = #imageLiteral(resourceName: "profile")
    }
}
