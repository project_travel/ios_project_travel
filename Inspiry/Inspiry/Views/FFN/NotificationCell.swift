//
//  NotificationCell.swift
//  Inspiry
//
//  Created by Alexander Danilyak on 21/09/2016.
//  Copyright © 2016 Inspiry. All rights reserved.
//

import Foundation
import UIKit
import AlamofireImage

class NotificationCell: UITableViewCell {
    var notification: NotificationData?
    var username: String?
    var user: User?
    
    @IBOutlet weak var profileImageView: UIImageView!
    @IBOutlet weak var usernameLabel: UILabel!
    @IBOutlet weak var dateAgoLabel: UILabel!
    @IBOutlet weak var notificationLabel: UILabel!
    
    func update(with notification: NotificationData) {
        self.profileImageView.layer.cornerRadius = 7.0
        self.profileImageView.layer.masksToBounds = true
        
        self.notification = notification
        self.setUsername()
        self.dateAgoLabel.text = timeAgoLabel(date: self.notification!.created)
        self.notificationLabel.text = self.notification!.message
        
        self.loadAuthor()
    }
    
    private func setUsername() {
        switch self.notification!.type {
        case .newLike:
            self.username = self.notification!.username
            break
        case .newComment:
            self.username = self.notification!.username
            break
        case .newFollower:
            self.username = self.notification!.followerUsername
            break
        case .newTripsExtracted:
            self.username = StoredUser.shared.user!.username
            break
        }
        self.usernameLabel.text = self.username
    }
    
    override func prepareForReuse() {
        self.usernameLabel.text = ""
        self.dateAgoLabel.text = ""
        self.notificationLabel.text = ""
        self.profileImageView.image = #imageLiteral(resourceName: "profile")
    }
    
    private func loadAuthor() {
        UserManager().getUser(with: username!, completion: authorLoadedCompletion)
    }
    
    private func authorLoadedCompletion(isSuccess: Bool, errorDescription: String, authorUser: User?) -> Void {
        if(isSuccess) {
            self.user = authorUser!
            if(self.user!.avatar.count > 0) {
                let url = PhotoSizeCollection.getBest(photos: self.user!.avatar)!.url
                self.loadAuthorImage(url: url)
            }
        } else {
            let name = self.notification!.type == .newFollower ? self.notification!.followerUsername : self.notification!.username
            print("[ERROR] can't load user " + name)
        }
    }
    
    private func loadAuthorImage(url: URL) {
        let request = URLRequest.init(url: url)
        UIHelpers.shared.loadImage(for: request, with: completion)
    }
    
    func completion(isSuccess: Bool, image: UIImage) -> Void {
        self.profileImageView.image = isSuccess ? image : #imageLiteral(resourceName: "profile")
    }
}
