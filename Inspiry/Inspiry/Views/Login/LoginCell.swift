//
//  LoginCell.swift
//  Inspiry
//
//  Created by Alexander Danilyak on 16/08/16.
//  Copyright © 2016 Inspiry. All rights reserved.
//

import Foundation
import UIKit

enum LoginCellType {
    case name
    case password
}

class LoginCell: UITableViewCell {
    
    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var iconImageView: UIImageView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var textField: UITextField!
    
    func setup(for type: LoginCellType) {
        self.containerView.isUserInteractionEnabled = false
        
        self.containerView.layer.cornerRadius = 13
        self.containerView.layer.borderWidth = 1
        self.setBorderColor(color: UIHelpers.gray())
        
        switch type {
        case .name:
            self.iconImageView.image = #imageLiteral(resourceName: "loginIcon")
            self.titleLabel.text = Localize(string: "login")
            break
        case .password:
            self.iconImageView.image = #imageLiteral(resourceName: "passwordIcon")
            self.titleLabel.text = Localize(string: "password")
            self.textField.isSecureTextEntry = true
            break
        }
    }
    
    func setBorderColor(color: UIColor) {
        self.containerView.layer.borderColor = color.cgColor
    }
}
