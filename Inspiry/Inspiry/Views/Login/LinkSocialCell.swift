//
//  LinkSocialCell.swift
//  Inspiry
//
//  Created by Alexander Danilyak on 19/08/16.
//  Copyright © 2016 Inspiry. All rights reserved.
//

import Foundation
import UIKit

class LinkSocialCell: UITableViewCell {
    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var iconImageView: UIImageView!
    @IBOutlet weak var linkLabel: UILabel!
    @IBOutlet weak var linkedLabel: UILabel!
    @IBOutlet weak var linkedAsNameLabel: UILabel!
    @IBOutlet weak var linkedContainer: UIView!
    
    func setup(for network: SocialNetwork) {
        self.linkLabel.text = Localize(string: "linkSocialLink")
        self.linkedLabel.text = Localize(string: "linkSocialLinked")
        
        self.containerView.layer.cornerRadius = 13
        self.containerView.layer.borderWidth = 1
        self.setBorderColor(color: UIHelpers.gray())
        
        self.titleLabel.text = network.name.rawValue
        
        let type: SocialNetworkName = network.name
        switch type {
        case .instagram:
            self.iconImageView.image = #imageLiteral(resourceName: "instagramIcon")
            break
        case .twitter:
            self.iconImageView.image = #imageLiteral(resourceName: "twitterIcon")
            break
        case .facebook:
            self.iconImageView.image = #imageLiteral(resourceName: "facebookIcon")
            break
        }
        
        self.linkedAsNameLabel.text = network.username
        self.linkedContainer.isHidden = !network.isLinked
        self.linkLabel.isHidden = network.isLinked
    }
    
    func setBorderColor(color: UIColor) {
        self.containerView.layer.borderColor = color.cgColor
    }
}
