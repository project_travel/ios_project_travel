//
//  SignUpCell.swift
//  Inspiry
//
//  Created by Alexander Danilyak on 17/08/16.
//  Copyright © 2016 Inspiry. All rights reserved.
//

import Foundation
import UIKit

enum SignUpCellType: Int {
    case name = 0
    case email = 1
    case password = 2
    case confirmPassword = 3
}

class SignUpCell: UITableViewCell {
    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var textField: UITextField!
    @IBOutlet weak var iconImageView: UIImageView!
    
    func setup(for type: SignUpCellType) {
        self.containerView.isUserInteractionEnabled = false
        
        self.containerView.layer.cornerRadius = 13
        self.containerView.layer.borderWidth = 1
        self.setBorderColor(color: UIHelpers.gray())
        
        switch type {
        case .name:
            self.iconImageView.image = #imageLiteral(resourceName: "loginIcon")
            self.titleLabel.text = Localize(string: "user")
            break
        case .email:
            self.iconImageView.image = #imageLiteral(resourceName: "emailIcon")
            self.titleLabel.text = Localize(string: "email")
            self.textField.keyboardType = .emailAddress
            break
        case .password:
            self.iconImageView.image = #imageLiteral(resourceName: "passwordIcon")
            self.titleLabel.text = Localize(string: "password")
            self.textField.isSecureTextEntry = true
            break
        case .confirmPassword:
            self.iconImageView.image = #imageLiteral(resourceName: "passwordIcon")
            self.titleLabel.text = Localize(string: "confirmPassword")
            self.textField.isSecureTextEntry = true
            break
        }
    }
    
    func setBorderColor(color: UIColor) {
        self.containerView.layer.borderColor = color.cgColor
    }
}
