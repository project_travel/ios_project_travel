//
//  TripPartnersAdCell.swift
//  Inspiry
//
//  Created by Alexander Danilyak on 11/09/16.
//  Copyright © 2016 Inspiry. All rights reserved.
//

import Foundation
import UIKit

class TripPartnersAdCell: UITableViewCell {
    var ad: TripPartnersAd?
    
    @IBOutlet weak var separatorView: UIView!
    @IBOutlet weak var typeImageView: UIImageView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var subtitleLabel: UILabel!
    
    func update(with ad: TripPartnersAd, isSeparatorNeeded: Bool = false) {
        self.ad = ad
        
        let book = self.ad!.source == .booking ? Localize(string: "bookHotelIn") : Localize(string: "bookFlightTo")
        let countryNameIfExist = self.ad!.country.isEmpty ? "" : ", " + self.ad!.country
        
        self.titleLabel.text = String.init(format: book, self.ad!.city) + countryNameIfExist
        self.subtitleLabel.text = String.init(format: Localize(string: "showOn"), self.ad!.source.rawValue) + ".com"
        
        self.typeImageView.image = self.ad!.source == .booking ? #imageLiteral(resourceName: "accommodation") : #imageLiteral(resourceName: "flight")
        
        self.separatorView.isHidden = !isSeparatorNeeded
    }
    
    func onAdTouched() {
        if self.ad == nil { return }
        Router.getTopMostVisibleController().present(UIHelpers.inAppBrowser(with: self.ad!.url), animated: true, completion: nil)
    }
    
    override func prepareForReuse() {
        self.titleLabel.text = ""
        self.subtitleLabel.text = ""
    }
}
