//
//  TripCountriesCell.swift
//  Inspiry
//
//  Created by Alexander Danilyak on 05/09/16.
//  Copyright © 2016 Inspiry. All rights reserved.
//

import Foundation
import UIKit

class TripCountriesCell: UITableViewCell {
    @IBOutlet weak var countriesTitleLabel: UILabel!
    @IBOutlet weak var countriesLabel: UILabel!
    
    @IBOutlet weak var oneCountryContainer: RoundedView!
    @IBOutlet weak var oneCountryImageView: UIImageView!
    
    @IBOutlet weak var fourCountriesContainer: UIView!
    @IBOutlet var fourCountriesImageContainers: [RoundedView]!
    @IBOutlet var fourCountriesImageViews: [UIImageView]!
    
    var trip: Trip?
    
    func update(with trip: Trip) {
        self.trip = trip
        
        let isSingleCoutry: Bool = self.trip!.countries.count == 1
        self.setCountries(isSingle: isSingleCoutry)
    }
    
    private func setCountries(isSingle: Bool) {
        if self.trip == nil { return }
        
        self.countriesTitleLabel.text = isSingle ? Localize(string: "tripCountry") : Localize(string: "tripCountries")
        
        // Setup
        self.oneCountryImageView.layer.cornerRadius = 13.0
        self.oneCountryImageView.layer.masksToBounds = true
        for imageView in fourCountriesImageViews {
            imageView.layer.cornerRadius = 7.0
            imageView.layer.masksToBounds = true
        }
        
        // Setting visibility
        self.oneCountryContainer.isHidden = !isSingle
        for container in fourCountriesImageContainers {
            container.isHidden = isSingle
        }
        
        // Setting data
        self.countriesLabel.text = ""
        
        if(isSingle) {
            self.oneCountryImageView.image = UIImage.init(named: trip!.countries.first!.id)
            self.countriesLabel.text = trip!.countries.first!.value
        } else {
            for index in 0...3 {
                let imageView: UIImageView = self.fourCountriesImageViews[index]
                let country: TripContry? = self.trip!.countries.count > index ? self.trip?.countries[index] : nil
                if(country != nil) {
                    imageView.image = UIImage.init(named: country!.id)
                    let commaIfNeeded: String = index == 0 ? "" : ", "
                    self.countriesLabel.text = self.countriesLabel.text! + commaIfNeeded + country!.value
                } else {
                    imageView.isHidden = true
                }
            }
        }
    }
}
