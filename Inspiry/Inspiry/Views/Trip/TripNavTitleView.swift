//
//  TripNavTitleView.swift
//  Inspiry
//
//  Created by Alexander Danilyak on 06/09/16.
//  Copyright © 2016 Inspiry. All rights reserved.
//

import Foundation
import UIKit

class TripNavTitleView: UIView, UITextFieldDelegate {
    @IBOutlet var contentView: UIView!
    @IBOutlet weak var navTitleTextField: TextFieldWithInsets!
    @IBOutlet weak var dateLabel: UILabel!
    
    var trip: Trip?
    var isMyTrip: Bool = false
    
    var delegate: TripTitleEditingProtocol?
    
    var isInEditingMode: Bool = false
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.commonInit()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        self.commonInit()
    }
    
    private func commonInit() {
        Bundle.main.loadNibNamed("TripNavTitleView", owner: self, options: nil)
        guard let content = contentView else { return }
        content.frame = self.bounds
        content.autoresizingMask = [.flexibleHeight, .flexibleWidth]
        self.addSubview(content)
        
        self.navTitleTextField.layer.cornerRadius = 7.0
        self.navTitleTextField.layer.masksToBounds = true
        
        self.navTitleTextField.delegate = self
    }
    
    func update(with trip: Trip?, isMyTrip: Bool) {
        self.isMyTrip = isMyTrip
        self.navTitleTextField.isUserInteractionEnabled = self.isMyTrip
        if(trip == nil) {
            self.navTitleTextField.text = isMyTrip ? Localize(string: "tripAddTitle") : Localize(string: "tripNoTitle")
            self.dateLabel.text = ""
        } else {
            self.trip = trip
            self.setTitle()
            self.dateLabel.text = durationLabel(from: self.trip!.start, to: self.trip!.end)
        }
    }
    
    private func setTitle() {
        if(self.trip!.name.isEmpty) {
            self.navTitleTextField.text = isMyTrip ? Localize(string: "tripAddTitle") : Localize(string: "tripNoTitle")
        } else {
            self.navTitleTextField.text = self.trip!.name
        }
    }
    
    // MARK: Text Field Delegate
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        UIView.animate(withDuration: 0.1) {
            self.navTitleTextField.backgroundColor = UIHelpers.lightGray()
        }
        self.isInEditingMode = true
        self.delegate?.titleEditingStarted()
        return true
    }
    
    // MARK: Title Actions
    
    func saveTitle() {
        self.endEditing(true)
        UIView.animate(withDuration: 0.1) { 
            self.navTitleTextField.backgroundColor = UIColor.clear
        }
        UIHelpers.shared.showLoadingAlert(text: Localize(string: "save"))
        TripManager().update(with: [apiKeyTripName: self.navTitleTextField.text!], and: self.trip!.id, completion: completion)
    }
    
    func cancel() {
        setTitle()
        self.endEditing(true)
        UIView.animate(withDuration: 0.1) {
            self.navTitleTextField.backgroundColor = UIColor.clear
        }
    }
    
    func completion(isSuccess: Bool, errorDescription: String?) -> Void {
        if(isSuccess) {
            self.trip?.name = self.navTitleTextField.text!
            UIHelpers.shared.closeAlert()
        } else {
            UIHelpers.shared.showErrorAlert(text: errorDescription, isAutoClosable: true)
        }
    }
}
