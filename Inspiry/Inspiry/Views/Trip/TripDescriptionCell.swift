//
//  TripDescriptionCell.swift
//  Inspiry
//
//  Created by Alexander Danilyak on 05/09/16.
//  Copyright © 2016 Inspiry. All rights reserved.
//

import Foundation
import UIKit

class TripDescriptionCell: UITableViewCell, UITextViewDelegate {
    @IBOutlet weak var tripDescriptionLabel: UITextView!
    
    var trip: Trip?
    var isMyTrip: Bool = false
    
    var delegate: TripDescriptionEditingProtocol?
    
    func update(with trip: Trip, isMyTrip: Bool) {
        self.trip = trip
        self.isMyTrip = isMyTrip
        
        self.tripDescriptionLabel.layer.cornerRadius = 11.0
        self.tripDescriptionLabel.layer.masksToBounds = true
        self.tripDescriptionLabel.tintColor = UIHelpers.green()
        self.tripDescriptionLabel.isEditable = self.isMyTrip
        self.tripDescriptionLabel.delegate = self
        
        self.setText()
    }
    
    func setText() {
        if self.trip == nil { return }
        
        let text = self.trip!.description
        let isDescriptionExist = !text.isEmpty
        let noDescriptionText = self.isMyTrip ? Localize(string: "tripAddDescription") : Localize(string: "tripNoDescription")
        self.tripDescriptionLabel.text = isDescriptionExist ? text : noDescriptionText
        self.tripDescriptionLabel.textColor = isDescriptionExist ? UIHelpers.darkGray() : UIHelpers.gray()
    }
    
    func setEditingUI(isActive: Bool) {
        UIView.animate(withDuration: 0.1) { 
            self.tripDescriptionLabel.backgroundColor = isActive ? UIHelpers.lightGray() : UIColor.clear
            let isDescriptionExist = !self.tripDescriptionLabel.text.isEmpty
            self.tripDescriptionLabel.textColor = isActive || isDescriptionExist ? UIHelpers.darkGray() : UIHelpers.gray()
        }
    }
    
    // MARK: Text View Delegate
    
    func textViewShouldBeginEditing(_ textView: UITextView) -> Bool {
        self.setEditingUI(isActive: true)
        self.delegate?.descriptionEditingStarted()
        return true
    }
    
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        self.delegate?.tableUpdates()
        return true
    }
    
    // MARK: Actions
    
    func saveDescription() {
        self.endEditing(true)
        self.setEditingUI(isActive: false)
        UIHelpers.shared.showLoadingAlert(text: Localize(string: "save"))
        TripManager().update(with: [apiKeyTripDescription: self.tripDescriptionLabel.text], and: self.trip!.id, completion: completion)
    }
    
    func cancel() {
        self.endEditing(true)
        self.setEditingUI(isActive: false)
        self.setText()
        self.delegate?.tableUpdates()
    }
    
    // MARK: Completion
    
    func completion(isSuccess: Bool, errorDescription: String?) -> Void {
        if(isSuccess) {
            self.trip?.description = self.tripDescriptionLabel.text
            UIHelpers.shared.closeAlert()
        } else {
            UIHelpers.shared.showErrorAlert(text: errorDescription, isAutoClosable: true)
        }
    }
}
