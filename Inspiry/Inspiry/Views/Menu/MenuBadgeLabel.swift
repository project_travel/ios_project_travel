//
//  MenuBadgeLabel.swift
//  Inspiry
//
//  Created by Alexander Danilyak on 30/08/16.
//  Copyright © 2016 Inspiry. All rights reserved.
//

import Foundation
import UIKit

class MenuBadgeLabel: UILabel {
    override func drawText(in rect: CGRect) {
        let insets = UIEdgeInsetsMake(0, 5, 0, 5)
        super.drawText(in: UIEdgeInsetsInsetRect(rect, insets))
    }
}
