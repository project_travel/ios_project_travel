//
//  MenuCell.swift
//  Inspiry
//
//  Created by Alexander Danilyak on 30/08/16.
//  Copyright © 2016 Inspiry. All rights reserved.
//

import Foundation
import UIKit

class MenuCell: UITableViewCell {
    @IBOutlet weak var menuItemLabel: UILabel!
    
    @IBOutlet weak var badgeLabel: MenuBadgeLabel!
    
    func setup(itemName: String!, badgeCount: Int, isCurrent: Bool) {
        self.menuItemLabel.text = itemName
        self.badgeLabel.isHidden = badgeCount <= 0
        self.badgeLabel.text = String(badgeCount)
        self.badgeLabel.layer.cornerRadius = 8.0
        self.badgeLabel.layer.masksToBounds = true
        
        self.menuItemLabel.font = UIHelpers.menuCellFont()
        
        self.menuItemLabel.textColor = isCurrent == true ? UIHelpers.green() : UIHelpers.darkGray()
    }
}
