//
//  AppDelegate.swift
//  Inspiry
//
//  Created by Alexander Danilyak on 11/07/16.
//  Copyright © 2016 Inspiry. All rights reserved.
//

import UIKit
import SwiftyJSON
import UserNotifications
import Amplitude_iOS

let OneSignalAppId = "7ca18f59-7578-4a6b-92ae-ff2297149ff4"
let AmplitudeDebugAppId = "bb650055eeaac86fd17e3df389b2537d"
let AmplitudeProductionAppId = "81c6f9620879888d9ff888ea0c6678ee"

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate, GIDSignInDelegate {

    var window: UIWindow?

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey : Any]? = nil) -> Bool {
        let string: String? = UserDefaults.standard.getAccessToken()
        if(string != nil) {
        }
        
        UIHelpers.setupNavBar()
        networkReachabilitySetup()
        
        // One Signal Push
        _ = OneSignal(launchOptions: launchOptions, appId: OneSignalAppId, handleNotification: { (notification) in
            print("[NOTIFICATION]" + String(describing: notification))
            if(notification.2 == false) {
                Router.handleRemotePush(body: notification.0!, userInfo: notification.1! as [NSObject : AnyObject])
            } else {
                UIHelpers.shared.showBigNotification(body: notification.0!, userInfo: notification.1! as [NSObject : AnyObject], isAutoClosable: true)
            }
        })
        
        OneSignal.defaultClient().enable(inAppAlertNotification: false)
        OneSignal.defaultClient().idsAvailable { (_userId, _token) in
            userId = _userId == nil ? "" : _userId!
            pushToken = _token == nil ? "" : _token!
        }
        
        // Google Sign In
        // Initialize sign-in
        var configureError: NSError?
        GGLContext.sharedInstance().configureWithError(&configureError)
        assert(configureError == nil, "Error configuring Google services: \(configureError)")
        GIDSignIn.sharedInstance().delegate = self
        
        self.window = UIWindow(frame: UIScreen.main.bounds)
        UIHelpers.shared.mainWindow = self.window
        
        if(UserAuth.shared.state == .noUser) {
            let loginStoryboard: UIStoryboard? = UIStoryboard(name: "Login", bundle: nil)
            self.window?.rootViewController = loginStoryboard?.instantiateInitialViewController()
        } else if(UserAuth.shared.state == .needVerification) {
            let loginStoryboard: UIStoryboard? = UIStoryboard(name: "Login", bundle: nil)
            let navLoginController = loginStoryboard?.instantiateInitialViewController() as! UINavigationController
            let verificationController = loginStoryboard?.instantiateViewController(withIdentifier: "verify")
            navLoginController.pushViewController(verificationController!, animated: false)
            self.window?.rootViewController = navLoginController
        } else {
            Amplitude.instance().setUserId(StoredUser.shared.user!.username)
            
            let navController: UINavigationController = UINavigationController()
            let mainStoryboard: UIStoryboard? = UIStoryboard(name: "Main", bundle: nil)
            let afterLaunchController: AfterLaunchController = mainStoryboard?.instantiateViewController(withIdentifier: "afterLaunch") as! AfterLaunchController
            navController.viewControllers = [afterLaunchController]
            
            self.window?.rootViewController = navController
        }
        
        self.window?.makeKeyAndVisible()
        
        // Amplitude Analytics
        #if DEBUG
            Amplitude.instance().initializeApiKey(AmplitudeDebugAppId)
        #else
            Amplitude.instance().initializeApiKey(AmplitudeProductionAppId)
        #endif
        Amplitude.instance().trackingSessionEvents = true
        
        return true
    }

    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }

    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }
    
    // MARK: URL Schemes
    
    func application(_ app: UIApplication, open url: URL, options: [UIApplicationOpenURLOptionsKey : Any] = [:]) -> Bool {
        if url.absoluteString.contains("com.googleusercontent") {
            return GIDSignIn.sharedInstance().handle(url,
                                                     sourceApplication: options[UIApplicationOpenURLOptionsKey.sourceApplication] as? String,
                                                     annotation: options[UIApplicationOpenURLOptionsKey.annotation])
        } else {
            StoredLinkedSocialNetworks.shared.handleResponse(url: url)
            return true
        }
    }
    
    // MARK: Google Sign In Delegate
    
    func sign(_ signIn: GIDSignIn!, didSignInFor user: GIDGoogleUser!, withError error: Error!) {
        if (error == nil) {
            // Perform any operations on signed in user here.
            let googleUserId = user.userID                  // For client-side use only!
            //let idToken = user.authentication.idToken     // Safe to send to the server
            let givenName = user.profile.givenName
            let familyName = user.profile.familyName
            let email = user.profile.email
            NotificationCenter.default.post(name: signInGoogleCompleted, object: self, userInfo: [apiKeyFirstname: givenName as Any,
                                                                                                  apiKeySurname: familyName as Any,
                                                                                                  apiKeyEmail: email as Any,
                                                                                                  apiKeyGoogleUserId: googleUserId as Any])
        } else {
            print("\(error.localizedDescription)")
        }
    }
    
    func sign(_ signIn: GIDSignIn!, didDisconnectWith user: GIDGoogleUser!, withError error: Error!) {
        
    }
}

