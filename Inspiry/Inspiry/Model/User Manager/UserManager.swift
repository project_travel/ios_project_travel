//
//  UserManager.swift
//  Inspiry
//
//  Created by Alexander Danilyak on 01/08/16.
//  Copyright © 2016 Inspiry. All rights reserved.
//

import Foundation
import SwiftyJSON
import Alamofire

class UserManager {
    
    func getUser(with username: String, completion: ((Bool, String, User?) -> Void)?) {
        if UserDefaults.standard.getAccessToken() != nil {
            Alamofire.request(mainURL + users + "/" + username,
                              method: .get,
                              headers: getAuthHeader()).validate().responseJSON { response in
                                switch response.result {
                                case .success(let json):
                                    let result = JSON(json)
                                    let user = User.init(_jsonResponse: result)
                                    if (completion != nil) {
                                        completion!(true, "", user)
                                    }
                                    break
                                    
                                case .failure(_):
                                    let errorDescription: String = errorFromResponse(response: response)
                                    if (completion != nil) {
                                        completion!(false, errorDescription, nil)
                                    }
                                    break
                                }
            }
        } else {
            if (completion != nil) {
                completion!(false, Localize(string: "noAccessToken"), nil)
            }
        }
    }
    
    // Follow user with provided username
    // Completion: isSuccess, isPending, errorDescription
    func followUser(with username: String, completion: @escaping (Bool, Bool, String) -> Void) {
        if UserDefaults.standard.getAccessToken() != nil {
            Alamofire.request(mainURL + users + me + following + "/" + username,
                              method: .post,
                              headers: getAuthHeader()).validate().responseJSON { response in
                                switch response.result {
                                case .success(_):
                                    
//                                    let index = StoredUser.shared.user!.following.index(where: { oneFollowing -> Bool in
//                                        return oneFollowing == username
//                                    })
//                                    if index == nil {
//                                        StoredUser.shared.user!.following.append(username)
//                                    }
                                    
                                    completion(true, false, "")
                                    
                                    break
                                    
                                case .failure(_):
                                    let errorDescription: String = errorFromResponse(response: response)
                                    completion(false, false, errorDescription)
                                    break
                                }
            }
        } else {
            completion(false, false, Localize(string: "noAccessToken"))
        }
    }
    
    // Unfollow
    // Same as follow, see above
    func unfollowUser(with username: String, completion: @escaping (Bool, String) -> Void) {
        if UserDefaults.standard.getAccessToken() != nil {
            Alamofire.request(mainURL + users + me + following + "/" + username,
                              method: .delete,
                              headers: getAuthHeader()).validate().responseJSON { response in
                                switch response.result {
                                case .success(_):
                                    completion(true, "")
                                    break
                                    
                                case .failure(_):
                                    let errorDescription: String = errorFromResponse(response: response)
                                    completion(false, errorDescription)
                                    break
                                }
            }
        } else {
            completion(false, Localize(string: "noAccessToken"))
        }
    }
}
