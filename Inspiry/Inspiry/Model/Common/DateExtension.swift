//
//  DateExtension.swift
//  Inspiry
//
//  Created by Alexander Danilyak on 13/07/16.
//  Copyright © 2016 Inspiry. All rights reserved.
//

import Foundation

// About format you can read here:
// http://www.faqs.org/rfcs/rfc3339.html

extension Date {
    struct formatter {
        static let rfc3339: DateFormatter = {
            let formatter = DateFormatter()
            formatter.calendar = Calendar(identifier: .iso8601)
            formatter.locale = Locale.current
            formatter.timeZone = TimeZone(secondsFromGMT: 0)
            formatter.dateFormat = "yyyy'-'MM'-'dd'T'HH':'mm':'ss'Z'"
            return formatter
        
        }()
        
        static let simple: DateFormatter = {
            let formatter = DateFormatter()
            formatter.calendar = Calendar(identifier: .iso8601)
            formatter.locale = Locale.current
            formatter.timeZone = TimeZone(secondsFromGMT: 0)
            formatter.dateFormat = "dd'.'MM'.'yyyy"
            return formatter
            
        }()
        
        static let tdMonthYear: DateFormatter = {
            let formatter = DateFormatter()
            formatter.calendar = Calendar(identifier: .iso8601)
            formatter.locale = Locale.current
            formatter.timeZone = TimeZone(secondsFromGMT: 0)
            formatter.dateFormat = "MMM', 'yyyy"
            return formatter
            
        }()
        
        static let tdMonth: DateFormatter = {
            let formatter = DateFormatter()
            formatter.calendar = Calendar(identifier: .iso8601)
            formatter.locale = Locale.current
            formatter.timeZone = TimeZone(secondsFromGMT: 0)
            formatter.dateFormat = "MMM"
            return formatter
            
        }()
        
        static let tdYear: DateFormatter = {
            let formatter = DateFormatter()
            formatter.calendar = Calendar(identifier: .iso8601)
            formatter.locale = Locale.current
            formatter.timeZone = TimeZone(secondsFromGMT: 0)
            formatter.dateFormat = "yyyy"
            return formatter
            
        }()
        
        static let tdDayMonthYear: DateFormatter = {
            let formatter = DateFormatter()
            formatter.calendar = Calendar(identifier: .iso8601)
            formatter.locale = Locale.current
            formatter.timeZone = TimeZone(secondsFromGMT: 0)
            formatter.dateFormat = "dd' 'MMM', 'yy"
            return formatter
        }()
    }
    
    var rfc3339: String {
        return formatter.rfc3339.string(from: self)
    }
    
    var simple: String {
        return formatter.simple.string(from: self)
    }
    
    var tdMonthYear: String {
        return formatter.tdMonthYear.string(from: self)
    }
    
    var tdMonth: String {
        return formatter.tdMonth.string(from: self)
    }
    
    var tdYear: String {
        return formatter.tdYear.string(from: self)
    }
    
    var tdDayMonthYear: String {
        return formatter.tdDayMonthYear.string(from: self)
    }
}

extension String {
    var dateRcf3339: Date? {
        return Date.formatter.rfc3339.date(from: self)
    }
}

func durationLabel(from startDate: Date, to finishDate: Date) -> String {
    let calendar = Calendar.current
    let startComponents: DateComponents = calendar.dateComponents([.year, .month], from: startDate)
    let finishComponents: DateComponents = calendar.dateComponents([.year, .month], from: finishDate)
    
    let isInOneYear = startComponents.year! == finishComponents.year!
    let isOnlyMonth = startComponents.month! == finishComponents.month! && isInOneYear
    
    if(isOnlyMonth == true) {
        return startDate.tdMonthYear
    } else if(isInOneYear == true) {
        return startDate.tdMonth + " - " + finishDate.tdMonth + ", " + startDate.tdYear
    } else {
        return startDate.tdMonth + ", " + startDate.tdYear + " - " + finishDate.tdMonth + ", " + finishDate.tdYear
    }
}

func postDateLabel(date: Date) -> String {
    return date.tdDayMonthYear
}

func timeAgoLabel(date: Date) -> String {
    let now = Date()
    let calendar = Calendar.current
    let units = Set<Calendar.Component>([.year, .month, .weekOfYear, .day, .hour, .minute, .second])
    let descriptions: [String] = ["YAgo", "MoAgo", "WAgo", "DAgo", "HAgo", "MinAgo", "Now"]
    let diffComponents = calendar.dateComponents(units, from: date, to: now)
    
    if(diffComponents.year != 0) {
        return String(diffComponents.year!) + Localize(string: descriptions[0])
    }
    
    if(diffComponents.month != 0) {
        return String(diffComponents.month!) + Localize(string: descriptions[1])
    }
    
    if(diffComponents.weekOfYear != 0) {
        return String(diffComponents.weekOfYear!) + Localize(string: descriptions[2])
    }
    
    if(diffComponents.day != 0) {
        return String(diffComponents.day!) + Localize(string: descriptions[3])
    }
    
    if(diffComponents.hour != 0) {
        return String(diffComponents.hour!) + Localize(string: descriptions[4])
    }
    
    if(diffComponents.minute != 0) {
        return String(diffComponents.minute!) + Localize(string: descriptions[5])
    }
    
    return Localize(string: descriptions[6])
}
