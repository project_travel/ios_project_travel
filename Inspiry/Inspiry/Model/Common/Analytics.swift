//
//  Analytics.swift
//  Inspiry
//
//  Created by Alexander Danilyak on 29/09/2016.
//  Copyright © 2016 Inspiry. All rights reserved.
//

import Foundation
import UIKit
import Amplitude_iOS

// Event names
let eventScreenOpened = "screenOpened"

// Parameters names
let screenName = "screenName"

let profileUsername = "username"

// Screens names
let screenLogin = "login"
let screenForgot = "forgotPassword"
let screenSignUp = "signUp"
let screenVerify = "verifyEmail"
let screenLinkSocial = "linkSocial"
let screenSearch = "globalSearch"
let screenSearchChooseCity = "globalSearchChoseCity"
let screenSearchChooseCountry = "globalSearchChoseCountry"
let screenNotifications = "notifications"
let screenFollowers = "followers"
let screenFollowing = "following"
let screenProfile = "profile"
let screenEditProfile = "editProfile"
let screenPosts = "posts"
let screenZoomedImage = "zoomImage"
let screenMenu = "menu"
let screenComments = "comments"
let screenMap = "map"
let screenSettings = "settings"
let screenPrivacy = "privacyPolicy"
let screenTerms = "termsOfService"
let screenChangePassword = "changePassword"
let screenChangeEmail = "changeEmail"

class Analytics {
    static let shared: Analytics = Analytics()
    
    func logScreen(name: String) {
        Amplitude.instance().logEvent(eventScreenOpened, withEventProperties: [screenName: name])
    }
    
    func logScreen(name: String, parameters:[String: String]) {
        var updatedParameters = parameters
        updatedParameters[screenName] = name
        Amplitude.instance().logEvent(eventScreenOpened, withEventProperties: updatedParameters)
    }
}
