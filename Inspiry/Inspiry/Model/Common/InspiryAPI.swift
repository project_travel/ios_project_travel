//
//  InspiryAPI.swift
//  Inspiry
//
//  Created by Alexander Danilyak on 12/07/16.
//  Copyright © 2016 Inspiry. All rights reserved.
//

import Foundation
import SwiftyJSON
import Alamofire
import UIKit

let language = Locale.current.languageCode

// URL constants for Inspiry.me API

let mainURL: String = {
    #if DEBUG
        return "https://api.inspiry.me/v1"
    #else
        return "https://api.inspiry.me"
    #endif
}()

let tokens = "/tokens"

let resend_verification = "/verification"

let access = "/access"
let refresh = "/refresh"

let users = "/users"
let trips = "/trips"

let me = "/me"
let password = "/password"

let following = "/following"
let followers = "/followers"
let notifications = "/notifications"

let likes = "/likes"

let avatar = "/avatar"
let background = "/background"

let posts = "/posts"

let comments = "/comments"

let social_medias = "/social_medias"

let recommendations = "/recommendations"

let blacklist = "/blacklist"

// Privacy & Terms Links
let termsLink: String = {
    if(language == "ru") {
        return "http://inspiry.me/ru/terms"
    } else {
        return "http://inspiry.me/en/terms"
    }
}()

let privacyLink: String = {
    if(language == "ru") {
        return "http://inspiry.me/ru/policy"
    } else {
        return "http://inspiry.me/en/policy"
    }
}()

// Redirect in app url
let redirect = "inspiry://"

// Error keys
let errorCodeKey = "code"
let errorMessageKey = "message"

// Error codes
enum InspiryErrorCode: Int {
    case tokenExpired = 1
    case invalidToken = 2
    case invalidEmail = 3
    case invalidNickname = 4
    case userDoesNotExist = 5
    case internalError = 6
    case invalidContent = 7
    case emptyFields = 8
    case forbidden = 9
    case invalidQuery = 10
    case badRequest = 11
    case methodNotAllowed = 12
    case denied = 13
}

// Parsing response into human-readable description
func errorFromResponse(response: DataResponse<Any>) -> String {
    // If response.response equals nil something went wrong (maybe network connection was lost, or canceled due to timeout)
    if(response.response == nil) {
        print(response.debugDescription)
        return Localize(string: "errorConnectionWasLost")
    }
    
    let statusCode: Int = response.response!.statusCode
    
    if(statusCode == 500) {
        print(response.debugDescription)
        return Localize(string: "error500")
    }
    
    if(statusCode == 404) {
        print(response.debugDescription)
        return Localize(string: "error404")
    }
    
    let jsonString = String(data: response.data!, encoding: String.Encoding.utf8)!
    let parsedError = JSON.parse(jsonString)
    let description = parsedError[errorMessageKey].stringValue
    
    return description
}

func networkReachabilitySetup() {
    let manager = NetworkReachabilityManager(host: mainURL)
    
    manager?.listener = { status in
        if(status != .reachable(.wwan) && status != .reachable(.ethernetOrWiFi)) {
            UIHelpers.shared.showErrorAlert(text: Localize(string: "alertNoConnection"), isAutoClosable: true)
        }
        print("Network Status Changed: \(status)")
    }
    
    manager?.startListening()
}

// Constant keys in the API responses

let apiKeyHeaderAuth = "authorization"
let apiKeyBearer = "Bearer"

let apiKeyAccessToken = "accessToken"
let apiKeyRefreshToken = "refreshToken"
let apiKeyLogin = "login"
let apiKeyPassword = "password"
let apiKeyPasswordAgain = "passwordAgain"

let apiKeyUsername = "username"
let apiKeyEmail = "email"
let apiKeySurname = "surname"
let apiKeyFirstname = "firstname"
let apiKeyBirthdate = "birthDate"
let apiKeyGender = "gender"
let apiKeyStatus = "status"
let apiKeyRegistrationDate = "registrationDate"
let apiKeyFollowers = "followers"
let apiKeyFollowing = "following"
let apiKeyHasFollowed = "hasFollowed"
let apiKeyInBlacklist = "inBlacklist"
let apiKeyAvatar = "avatar"
let apiKeyBackground = "background"

let apiKeyNewPassword = "newPassword"
let apiKeyOldPassword = "oldPassword"

let apiKeyApp = "app"
let apiKeyDevice = "device"
let apiKeyPush = "push"

let apiKeySocialMedia = "social_media"
let apiKeyRedirectURL = "redirect_url"

let apiKeyTripId = "id"
let apiKeyTripName = "name"
let apiKeyTripOwners = "owners"
let apiKeyTripStart = "start"
let apiKeyTripEnd = "end"
let apiKeyTripCountries = "countries"
let apiKeyTripCities = "cities"
let apiKeyTripAvatar = "avatar"
let apiKeyTripLikes = "likes"
let apiKeyTripHasLiked = "hasLiked"
let apiKeyTripHashtags = "hashtags"
let apiKeyTripModified = "modified"
let apiKeyTripDescription = "description"
let apiKeyTripCountryId = "id"
let apiKeyTripCountryValue = "value"
let apiKeyTripAd = "ad"
let apiKeyAdSource = "source"
let apiKeyAdCity = "city"
let apiKeyAdCoutry = "coutry"
let apiKeyAdOrigin = "origin"
let apiKeyAdUrl = "url"

let apiKeyTripsOffset = "offset"
let apiKeyTripsSize = "size"

let apiKeyAvatarWidth = "width"
let apiKeyAvatarHeight = "height"
let apiKeyAvatarType = "type"
let apiKeyAvatarUrl = "url"

let apiKeyQueryString = "q"

let apiKeyPostId = "id"
let apiKeyPostOwner = "owner"
let apiKeyPostText = "text"
let apiKeyPostLocation = "location"
let apiKeyPostLocationType = "type"
let apiKeyPostLocationCoordinates = "coordinates"
let apiKeyPostVisible = "visible"
let apiKeyPostSource = "source"
let apiKeyPostSourceName = "name"
let apiKeyPostSourceUrl = "url"
let apiKeyPostCreated = "created"
let apiKeyPostModified = "modified"
let apiKeyPostHashtags = "hashtags"
let apiKeyPostPhoto = "photo"

let apiKeyPostsOffset = "offset"
let apiKeyPostsSize = "size"

let apiKeyCommentId = "id"
let apiKeyCommentOwner = "username"
let apiKeyCommentCreated = "created"
let apiKeyCommentModified = "modified"
let apiKeyCommentText = "text"
let apiKeyComment = "comment"

let apiKeyNotificationType = "type"
let apiKeyNotificationCreated = "created"
let apiKeyNotificationFailed = "isUpdateFailed"
let apiKeyNotificationMessage = "message"
let apiKeyNotificationNewIds = "newIds"
let apiKeyNotificationUpdatedIds = "updatedIds"
let apiKeyNotificationTripId = "tripId"
let apiKeyNotificationTripName = "tripName"
let apiKeyNotificationUsername = "username"
let apiKeyNotificationFollowerUsername = "followerUsername"

let apiKeyOffset = "offset"
let apiKeySize = "size"

let apiKeyBlackListPost = "post"
let apiKeyBlackListTrip = "trip"

let apiKeyGoogleUserId = "googleId"

let apiKeyRefreshRecommendations = "refresh"

// Unique key for iOS app
let inspiryiOSApp: String = {
    #if DEBUG
        return "kUF27xUbpwmcjc1eT07xj7x4Vuj41Ps7"
    #else
        return "mXuoh6LJ3stA3kwGYm5y1JE2T7H3T6B0"
    #endif
}()

// Unique iOS device id
let uuid = UIDevice.current.identifierForVendor!.uuidString
var userId: String = ""
var pushToken: String = ""

func getAuthHeader() -> [String: String]? {
    if UserDefaults.standard.getAccessToken() != nil {
        return [apiKeyHeaderAuth: apiKeyBearer + " " + UserDefaults.standard.getAccessToken()!]
    } else {
        return nil
    }
}

func getBearerToken() -> String {
    return apiKeyBearer + " " + UserDefaults.standard.getAccessToken()!
}
