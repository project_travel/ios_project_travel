//
//  LocalizationHelper.swift
//  Inspiry
//
//  Created by Alexander Danilyak on 22/07/16.
//  Copyright © 2016 Inspiry. All rights reserved.
//

import Foundation

func Localize(string: String!) -> String {
    return NSLocalizedString(string, tableName: "Localization", bundle: Bundle.main, value: "", comment: "")
}
