//
//  Generators.swift
//  Inspiry
//
//  Created by Alexander Danilyak on 04/11/2016.
//  Copyright © 2016 Inspiry. All rights reserved.
//

import Foundation

func generatePassword(length: Int = 10) -> String {
    
    let letters : NSString = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789"
    let len = UInt32(letters.length)
    
    var randomString = ""
    
    for _ in 0 ..< length {
        let rand = arc4random_uniform(len)
        var nextChar = letters.character(at: Int(rand))
        randomString += NSString(characters: &nextChar, length: 1) as String
    }
    
    return randomString
}
