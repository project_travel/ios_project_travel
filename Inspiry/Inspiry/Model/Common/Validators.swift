//
//  Validators.swift
//  Inspiry
//
//  Created by Alexander Danilyak on 13/07/16.
//  Copyright © 2016 Inspiry. All rights reserved.
//

import Foundation

// Check string on being an email
func isStringEmail(isEmailString: String?) -> Bool {
    if(isEmailString == nil) {
        return false
    }
    
    let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,}"
    
    let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
    return emailTest.evaluate(with: isEmailString)
}

func isValidUsername(isUsernameString: String?) -> Bool {
    if(isUsernameString == nil) {
        return false
    }
    
    let usernameRegEx = "[A-Za-z]+[A-Za-z0-9]{3,19}"
    
    let usernameTest = NSPredicate(format:"SELF MATCHES %@", usernameRegEx)
    return usernameTest.evaluate(with: isUsernameString)
}
