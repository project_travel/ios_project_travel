//
//  AuthorizationManager.swift
//  Inspiry
//
//  Created by Alexander Danilyak on 11/07/16.
//  Copyright © 2016 Inspiry. All rights reserved.
//

import Foundation
import Alamofire
import SwiftyJSON
import Amplitude_iOS

// Manager for user authroziration

class AuthManager {
    
    // MARK: Auth
    
    // Auth user with login and password
    // If refresh token is needed, request should contain app and device fields in the body
    func login(login: String!,
               password: String!,
               needRefreshToken: Bool!,
               completion:@escaping (Bool, String?) -> Void) {
        
        var parameters: [String: String] = [apiKeyLogin: login, apiKeyPassword: password]
        if(needRefreshToken == true) {
            parameters[apiKeyApp] = inspiryiOSApp
            parameters[apiKeyDevice] = uuid
            parameters[apiKeyPush] = userId
        }
        print(parameters)
        
        Alamofire.request(mainURL + tokens + access, method: .post, parameters: parameters, encoding: JSONEncoding.default).validate().responseJSON { response in
            switch response.result {
            case .success(let json):
                let result = JSON(json)
                
                if(needRefreshToken == true) {
                    UserDefaults.standard.saveRefreshToken(_refreshToken: result[apiKeyRefreshToken].stringValue)
                }
                UserDefaults.standard.saveAccessToken(_accessToken: result[apiKeyAccessToken].stringValue)
                
                completion(true, nil)
                break
            
            case .failure(_):
                let errorDescription: String = errorFromResponse(response: response)
                completion(false, errorDescription)
                break
            }
        }
    }
    
    
    // Auth user with access or refresh tokens
    func loginWithToken(tokenKey: String!, completion:((Bool, String?) -> Void)?) {
        var parameters: [String: String]!
        if(tokenKey == inspiryAccessTokenKey) {
            parameters = [apiKeyAccessToken: UserDefaults.standard.getAccessToken()!]
        } else {
            parameters = [apiKeyRefreshToken: UserDefaults.standard.getRefreshToken()!]
        }
        
        Alamofire.request(mainURL + tokens + access, method: .post, parameters: parameters, encoding: JSONEncoding.default).validate().responseJSON { response in
            switch response.result {
            case .success(let json):
                let result = JSON(json)
                UserDefaults.standard.saveAccessToken(_accessToken: result[apiKeyAccessToken].stringValue)
                if(completion != nil) {
                    completion!(true, nil)
                }
                break
                
            case .failure(_):
                let errorDescription: String = errorFromResponse(response: response)
                if(completion != nil) {
                    completion!(false, errorDescription)
                }
                break
            }
        }
    }
    
    func loginWithGmailAndGoogleID(email: String!, googleId: String!, completion:((Bool, String?) -> Void)?) {
        let parameters = [apiKeyLogin: email,
                          apiKeyGoogleUserId: googleId,
                          apiKeyApp: inspiryiOSApp,
                          apiKeyDevice: uuid,
                          apiKeyPush: userId]
        
        Alamofire.request(mainURL + tokens + access, method: .post, parameters: parameters, encoding: JSONEncoding.default).validate().responseJSON { response in
            switch response.result {
            case .success(let json):
                let result = JSON(json)
                UserDefaults.standard.saveAccessToken(_accessToken: result[apiKeyAccessToken].stringValue)
                UserDefaults.standard.saveRefreshToken(_refreshToken: result[apiKeyRefreshToken].stringValue)
                if(completion != nil) {
                    completion!(true, nil)
                }
                break
                
            case .failure(_):
                let errorDescription: String = errorFromResponse(response: response)
                if(completion != nil) {
                    completion!(false, errorDescription)
                }
                break
            }
        }
    }
    
    
    // Create a refresh token with login and password
    func createRefreshToken(login: String!,
                            password: String!,
                            completion:((Bool, String?) -> Void)?) {
        
        let parameters: [String: String] = [apiKeyLogin: login,
                                               apiKeyPassword: password,
                                               apiKeyApp: inspiryiOSApp,
                                               apiKeyDevice: uuid,
                                               apiKeyPush: userId]
        print(parameters)
        Alamofire.request(mainURL + tokens + refresh, method: .post, parameters: parameters, encoding: JSONEncoding.default).validate().responseJSON { response in
            switch response.result {
            case .success(let json):
                let result = JSON(json)
                UserDefaults.standard.saveRefreshToken(_refreshToken: result[apiKeyRefreshToken].stringValue)
                if(completion != nil) {
                    completion!(true, nil)
                }
                break
                
            case .failure(_):
                let errorDescription: String = errorFromResponse(response: response)
                if(completion != nil) {
                    completion!(false, errorDescription)
                }
                break
            }
        }
    }
    
    
    // MARK: Registration
    
    // Prepare body parameters for registration request
    func parametersForRegister(username: String!,
                               email: String!,
                               password: String!,
                               googleId: String?,
                               firstname: String?,
                               surname: String?,
                               birthDate: Date?,
                               gender: String?,
                               status: String?) -> [String: String] {
        var parameters: [String: String] = [apiKeyUsername: username,
                                               apiKeyEmail: email,
                                               apiKeyPassword: password,
                                               apiKeyApp: inspiryiOSApp,
                                               apiKeyDevice: uuid,
                                               apiKeyPush: userId]
        
        if(!((googleId ?? "").isEmpty)) {
            parameters[apiKeyGoogleUserId] = googleId
        }
        
        if(!((firstname ?? "").isEmpty)) {
            parameters[apiKeyFirstname] = firstname
        }
        
        if(!((surname ?? "").isEmpty)) {
            parameters[apiKeySurname] = surname
        }
        
        if(!((status ?? "").isEmpty)) {
            parameters[apiKeyStatus] = status
        }
        
        if(!((gender ?? "").isEmpty)) {
            parameters[apiKeyGender] = gender
        }
        
        if(birthDate != nil) {
            parameters[apiKeyBirthdate] = birthDate!.rfc3339
        }
        
        return parameters
    }
    
    
    // Register user with a set of input data
    func register(username: String!,
                  email: String!,
                  password: String!,
                  googleId: String?,
                  firstname: String?,
                  surname: String?,
                  birthDate: Date?,
                  gender: String?,
                  status: String?,
                  completion:@escaping (Bool, String?) -> Void) {
        
        let parameters = self.parametersForRegister(username: username, email: email, password: password, googleId: googleId, firstname: firstname, surname: surname, birthDate: birthDate, gender: gender, status: status)
        
        let headers = ["X-Agent":"mobile"]
        
        Alamofire.request(mainURL + users, method: .post, parameters: parameters, encoding: JSONEncoding.default, headers: headers).validate().responseJSON { response in
            switch response.result {
            case .success(let json):
                let result = JSON(json)
                let accessToken = result[apiKeyAccessToken].stringValue
                let refreshToken = result[apiKeyRefreshToken].stringValue
                
                UserDefaults.standard.saveBothTokens(_accessToken: accessToken, _refreshToken: refreshToken)
                StoredUser.shared.set(username: username, email: email)
                
                Amplitude.instance().setUserId(username)
                
                completion(true, nil)
                break
                
            case .failure(_):
                let errorDescription: String = errorFromResponse(response: response)
                completion(false, errorDescription)
                break
            }
        }
    }
    
    // Register user with username, email, password only
    func register(username: String!,
                  email: String!,
                  password: String!,
                  googleId: String? = nil,
                  completion:@escaping (Bool, String?) -> Void) {
        
        self.register(username: username,
                      email: email,
                      password: password,
                      googleId: googleId,
                      firstname: nil,
                      surname: nil,
                      birthDate: nil,
                      gender: nil,
                      status: nil,
                      completion: completion)
    }
    
    // MARK: Forgot Password
    
    func forgot(email: String!, completion:((Bool, String?) -> Void)?) {
        Alamofire.request(mainURL + password, method: .post, parameters: [email: email], encoding: JSONEncoding.default).validate().responseJSON { response in
            switch response.result {
            case .success(_):
                if(completion != nil) {
                    completion!(true, nil)
                }
                break
                
            case .failure(_):
                let errorDescription: String = errorFromResponse(response: response)
                if(completion != nil) {
                    completion!(false, errorDescription)
                }
                break
            }
        }
    }
    
    // MARK: Resend Verification
    
    func resendVerification(email: String!, completion:((Bool, String?) -> Void)?) {
        Alamofire.request(mainURL + resend_verification, method: .post, parameters: [email: email], encoding: JSONEncoding.default).validate().responseJSON { response in
            switch response.result {
            case .success(_):
                if(completion != nil) {
                    completion!(true, nil)
                }
                break
                
            case .failure(_):
                let errorDescription: String = errorFromResponse(response: response)
                if(completion != nil) {
                    completion!(false, errorDescription)
                }
                break
            }
        }
    }
}
