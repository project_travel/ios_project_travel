//
//  UserAuthorizationState.swift
//  Inspiry
//
//  Created by Alexander Danilyak on 11/07/16.
//  Copyright © 2016 Inspiry. All rights reserved.
//

import Foundation

let inspiryAccessTokenKey = "inspiryAccessToken";
let inspiryRefreshTokenKey = "inspiryRefreshToken";

// Struct for user state traking
// User needs both, refresh and access tokens, to work with system
// Both tokens should be valid (not expired)
enum UserAuthState {
    case noUser
    case hasBothTokens
    case needAccessToken
    case needVerification
}

// Singletone class for traking user authorization state
class UserAuth {
    static let shared = UserAuth()
    
    var state: UserAuthState
    
    private init() {
        let hasAccess = UserDefaults.standard.getAccessToken() != nil ? !(UserDefaults.standard.getAccessToken()!.isEmpty) : false
        let hasRefresh = UserDefaults.standard.getRefreshToken() != nil ? !(UserDefaults.standard.getRefreshToken()!.isEmpty) : false
        
        if(!hasAccess && !hasRefresh) {
            state = .noUser
            return
        } else if(hasRefresh && !hasAccess) {
            state = .needVerification
            return
        } else if(hasAccess && hasRefresh) {
            state = .needAccessToken
            return
        }
        state = .noUser
    }
}


// An User Defaults extension for quick access to tokens
extension UserDefaults {
    
    func getAccessToken() -> String? {
        return UserDefaults.standard.string(forKey: inspiryAccessTokenKey)
    }
    
    func getRefreshToken() -> String? {
        return UserDefaults.standard.string(forKey: inspiryRefreshTokenKey)
    }
    
    func saveAccessToken(_accessToken: String!) {
        UserDefaults.standard.set(_accessToken, forKey: inspiryAccessTokenKey)
        UserAuth.shared.state = .hasBothTokens
    }
    
    func saveRefreshToken(_refreshToken: String!) {
        UserDefaults.standard.set(_refreshToken, forKey: inspiryRefreshTokenKey)
        UserAuth.shared.state = .needAccessToken
    }
    
    func saveBothTokens(_accessToken: String!, _refreshToken: String!) {
        UserDefaults.standard.saveRefreshToken(_refreshToken: _refreshToken)
        UserDefaults.standard.saveAccessToken(_accessToken: _accessToken)
    }
    
    func clearAccessToken() {
        UserDefaults.standard.set(nil, forKey: inspiryAccessTokenKey)
        UserAuth.shared.state = .needAccessToken
    }
    
    func clearRefreshToken() {
        UserDefaults.standard.set(nil, forKey: inspiryRefreshTokenKey)
        UserAuth.shared.state = .noUser
    }
    
    func clearBothTokens() {
        UserDefaults.standard.clearAccessToken()
        UserDefaults.standard.clearRefreshToken()
    }
}
