//
//  LinkSocialManager.swift
//  Inspiry
//
//  Created by Alexander Danilyak on 17/07/16.
//  Copyright © 2016 Inspiry. All rights reserved.
//

import Foundation
import SwiftyJSON
import Alamofire

// Manager for linking social networks to an account

class LinkManager {
    
    // Send a link request to the selected social media
    // If "success" user will be redirected to the Safari for "privacy" confirmation
    // After confirmation user returns to application via url-sceme "inspiry://*social network name*"
    func linkSocialNetwork(socialNetworkName: String!, completion: ((Bool, String?) -> Void)?) {
        if UserDefaults.standard.getAccessToken() != nil {
            Alamofire.request(mainURL + tokens + "/" + socialNetworkName,
                              method: .get,
                              parameters: [apiKeySocialMedia: socialNetworkName,
                                           apiKeyRedirectURL: redirect + socialNetworkName],
                              headers: getAuthHeader()).validate(statusCode: 200..<400).response { response in
                                
                                let error = response.error
                                
                                if(error == nil) {
                                    UIApplication.shared.open((response.response!.url)!, options: [:], completionHandler: nil)
                                    if(completion != nil) {
                                        completion!(true, nil)
                                    }
                                } else {
                                    if(completion != nil) {
                                        completion!(false, error.debugDescription)
                                    }
                                }
            }
        } else {
            if (completion != nil) {
                completion!(false, Localize(string: "noAccessToken"))
            }
        }
    }
    
    func unlinkSocialNetwork(user: User!, socialNetworkName: String!, completion: ((Bool, String?) -> Void)?) {
        if UserDefaults.standard.getAccessToken() != nil {
            var url = mainURL + users
            url += "/" + user.username + social_medias + "/" + socialNetworkName
            
            Alamofire.request(url,
                              method: .delete,
                              headers: getAuthHeader()).validate().responseJSON { response in
                                switch response.result {
                                case .success(_):
                                    completion?(true, nil)
                                    break
                                case .failure(_):
                                    let errorDescription: String = errorFromResponse(response: response)
                                    completion?(false, errorDescription)
                                    break
                                }
            }
        } else {
            completion?(false, Localize(string: "noAccessToken"))
        }
    }
    
    // Get linked networks
    func getSocialNetworks(user: User!, completion: @escaping (Bool, [SocialNetwork]?, String?) -> Void) {
        if UserDefaults.standard.getAccessToken() != nil {
            var url = mainURL + users
            url += "/" + user.username + social_medias
            
            Alamofire.request(url,
                              method: .get,
                              headers: getAuthHeader()).validate().responseJSON { response in
                                switch response.result {
                                case .success(let json):
                                    completion(true, SocialNetwork.parse(json: JSON(json)), nil)
                                    break
                                case .failure(_):
                                    let errorDescription: String = errorFromResponse(response: response)
                                    completion(false, nil, errorDescription)
                                    break
                                }
            }
        } else {
            completion(false, nil, Localize(string: "noAccessToken"))
        }
    }
}
