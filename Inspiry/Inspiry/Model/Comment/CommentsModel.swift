//
//  CommentsModel.swift
//  Inspiry
//
//  Created by Alexander Danilyak on 05/08/16.
//  Copyright © 2016 Inspiry. All rights reserved.
//

import Foundation
import Alamofire
import SwiftyJSON

class CommentsModel {
    var tripId: String!
    
    var stop: Bool = false
    var allComments: [Comment] = []
    
    private var offset: Int = 0
    private var pageSize: Int
    
    init(tripId: String!, pageSize: Int) {
        self.tripId = tripId
        self.pageSize = pageSize
    }
    
    func loadNextPage(completion: @escaping (Bool, Int, Int, String) -> Void) {
        let parameters: [String: String] = [apiKeyPostsSize: String(pageSize),
                                            apiKeyPostsOffset: String(offset)]
        
        Alamofire.request(mainURL + trips + "/" + tripId + comments, method: .get, parameters: parameters, encoding: URLEncoding.default, headers: getAuthHeader()).validate().responseJSON { response in
            switch response.result {
            case .success(let json):
                let result = JSON(json)
                let newComments = Comments.parse(json: result)
                self.stop = newComments.count < self.pageSize
                self.offset += newComments.count
                let lastDisplayed = self.allComments.count
                self.allComments.append(contentsOf: newComments)
                
                completion(true, lastDisplayed, newComments.count, "")
                
                break
                
            case .failure(_):
                let errorDescription: String = errorFromResponse(response: response)
                completion(false, 0, 0, errorDescription)
                break
            }
            
        }
    }
    
    func reload(completion: @escaping (Bool, Int, Int, String) -> Void) {
        allComments.removeAll()
        offset = 0
        loadNextPage(completion: completion)
    }
    
    func addComment(text: String, completion: @escaping (Bool, Int, Int, String) -> Void) {
        Alamofire.request(mainURL + trips + "/" + tripId + comments, method: .post, parameters: Comment.create(text: text), encoding:JSONEncoding.default, headers: getAuthHeader()).validate().responseJSON { response in
            switch response.result {
            case .success(let json):
                let result = JSON(json)
                let newComment = Comment.init(json: result)
                self.offset += 1
                let lastDisplayed = self.allComments.count
                self.allComments.append(newComment)
                
                completion(true, lastDisplayed, 1, "")
                
                break
                
            case .failure(_):
                let errorDescription: String = errorFromResponse(response: response)
                completion(false, 0, 0, errorDescription)
                break
            }
            
        }
    }
    
    func editComment(id: String, text: String, completion: @escaping (Bool, Int, String) -> Void) {
        Alamofire.request(mainURL + trips + "/" + tripId + comments + "/" + id, method: .put, parameters: Comment.create(text: text), encoding: JSONEncoding.default, headers: getAuthHeader()).validate().responseJSON { response in
            switch response.result {
            case .success(_):
                
                let index: Int? = self.allComments.index(where: { (comment) -> Bool in
                    return comment.id == id
                })
                
                if(index != nil) {
                    self.allComments[index!].text = text
                    completion(true, index!, "")
                } else  {
                    completion(false, index!, "No such comment")
                }
                
                break
                
            case .failure(_):
                let errorDescription: String = errorFromResponse(response: response)
                completion(false, -1, errorDescription)
                break
            }
            
        }
    }
    
    func deleteComment(id: String, completion: @escaping (Bool, Int, String) -> Void) {
        Alamofire.request(mainURL + trips + "/" + tripId + comments + "/" + id, method: .delete, encoding: URLEncoding.default, headers: getAuthHeader()).validate().responseJSON { response in
            switch response.result {
            case .success(_):
                
                let index: Int? = self.allComments.index(where: { (comment) -> Bool in
                    return comment.id == id
                })
                
                if(index != nil) {
                    self.allComments.remove(at: index!)
                    completion(true, index!, "")
                } else  {
                    completion(false, index!, "No such comment")
                }
                
                break
                
            case .failure(_):
                let errorDescription: String = errorFromResponse(response: response)
                completion(false, -1, errorDescription)
                break
            }
            
        }
    }
}
