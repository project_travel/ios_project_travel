//
//  Comment.swift
//  Inspiry
//
//  Created by Alexander Danilyak on 05/08/16.
//  Copyright © 2016 Inspiry. All rights reserved.
//

import Foundation
import SwiftyJSON

class Comment {
    var id: String
    var owner: String
    var created: Date
    var modified: Date?
    var text: String
    
    init(json: JSON) {
        id = json[apiKeyCommentId].stringValue
        owner = json[apiKeyCommentOwner].stringValue
        created = json[apiKeyCommentCreated].stringValue.dateRcf3339!
        if json[apiKeyCommentModified] != nil {
            modified = json[apiKeyCommentModified].stringValue.dateRcf3339!
        }
        text = json[apiKeyCommentText].stringValue
    }
    
    static func create(text: String!) -> [String: String] {
        return [apiKeyComment: text]
    }
}

class Comments {
    static func parse(json: JSON) -> [Comment] {
        var comments: [Comment] = []
        for comment in json.arrayValue {
            comments.append(Comment.init(json: comment))
        }
        return comments
    }
}
