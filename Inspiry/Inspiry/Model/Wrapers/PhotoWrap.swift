//
//  Avatar.swift
//  Inspiry
//
//  Created by Alexander Danilyak on 28/07/16.
//  Copyright © 2016 Inspiry. All rights reserved.
//

import Foundation
import SwiftyJSON

class PhotoWrap {
    var width: Int
    var height: Int
    var type: String?
    var url: URL
    
    init(json: JSON) {
        width = Int(json[apiKeyAvatarWidth].stringValue)!
        height = Int(json[apiKeyAvatarHeight].stringValue)!
        type = json[apiKeyAvatarType].stringValue
        url = URL.init(string: json[apiKeyAvatarUrl].stringValue)!
    }
    
    static func getBestQuality(photos: [PhotoWrap]) -> PhotoWrap? {
        var best: PhotoWrap? = nil
        var bestPixelCount: Int = 0
        
        for photo in photos {
            let pixelCount = photo.width * photo.height
            if(pixelCount > bestPixelCount) {
                bestPixelCount = pixelCount
                best = photo
            }
        }
        
        return best
    }
}
