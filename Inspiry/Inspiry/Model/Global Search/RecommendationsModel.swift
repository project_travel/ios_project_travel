//
//  RecommendationsModel.swift
//  Inspiry
//
//  Created by Alexander Danilyak on 01/10/2016.
//  Copyright © 2016 Inspiry. All rights reserved.
//

import Foundation
import SwiftyJSON
import Alamofire

enum RecommendstionType: Int {
    case trips = 0
    case users = 1
}

class RecommendationsModel {
    var recommended: [AnyObject] = [AnyObject]()
    var type: RecommendstionType = .trips
    
    var offset: Int = 0
    var pageSize: Int = 20
    
    var stop: Bool = false
    
    init(pageSize: Int) {
        self.pageSize = pageSize
    }
    
    func loadNextPage(completion: @escaping (Bool, Int, Int, String) -> Void) {
        let parameters: [String: Any] = [apiKeySize: String(self.pageSize),
                                            apiKeyOffset: String(self.offset),
                                            apiKeyRefreshRecommendations: self.offset == 0]
        
        var urlPart: String
        switch type {
        case .trips:
            urlPart = trips
            break
        case .users:
            urlPart = users
            break
        }
        
        let url = mainURL + users + "/" + StoredUser.shared.user!.username + urlPart + recommendations
        
        Alamofire.request(url, method: .get, parameters: parameters, encoding: URLEncoding.default, headers: getAuthHeader()).validate().responseJSON { response in
            switch response.result {
            case .success(let json):
                let result = JSON(json)
                
                let newResults: [AnyObject]// = Trips.parse(json: result)
                switch self.type {
                case .trips:
                    newResults = Trips.parse(json: result)
                    break
                case .users:
                    newResults = Users.parse(json: result)
                    break
                }
                
                self.stop = newResults.count < self.pageSize
                self.offset += newResults.count
                let lastDisplayed = self.recommended.count
                self.recommended.append(contentsOf: newResults)
                
                completion(true, lastDisplayed, newResults.count, "")
                
                break
                
            case .failure(_):
                let errorDescription: String = errorFromResponse(response: response)
                completion(false, 0, 0, errorDescription)
                break
            }
            
        }
    }
    
    func reload(completion: @escaping (Bool, Int, Int, String) -> Void) {
        reset()
        loadNextPage(completion: completion)
    }
    
    func reset() {
        recommended.removeAll()
        offset = 0
    }
}
