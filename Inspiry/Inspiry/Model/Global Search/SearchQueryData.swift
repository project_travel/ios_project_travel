//
//  SearchQueryData.swift
//  Inspiry
//
//  Created by Alexander Danilyak on 17/09/16.
//  Copyright © 2016 Inspiry. All rights reserved.
//

import Foundation

class SearchQueryData {
    var type: SearchType = .trips
    
    // For Users search
    var q: String?
    
    // For Trips search
    var country: String?
    var city: String?
    var start: Date?
    var end: Date?
    var name: String?
    
    func getForParams(size: Int, offset: Int) -> [String: String] {
        var params: [String: String] = [String: String]()
        if(self.type == .trips) {
            if(country != nil) { params[apiKeyTripCountries] = country }
            if(city != nil) { params[apiKeyTripCities] = city }
            if(start != nil) { params[apiKeyTripStart] = start!.rfc3339 }
            if(end != nil) { params[apiKeyTripEnd] = end!.rfc3339 }
            if(name != nil) { params[apiKeyTripName] = name }
        } else {
            if(q != nil) { params[apiKeyQueryString] = q }
        }
        params[apiKeyTripsSize] = String(size)
        params[apiKeyTripsOffset] = String(offset)
        return params
    }
}
