//
//  GlobalSearchModel.swift
//  Inspiry
//
//  Created by Alexander Danilyak on 29/07/16.
//  Copyright © 2016 Inspiry. All rights reserved.
//

import Foundation
import SwiftyJSON
import Alamofire

enum SearchType: Int {
    case trips = 0
    case users = 1
}

class GlobalSearchModel {
    var searchQueryData: SearchQueryData?
    var searchResults: [AnyObject] = [AnyObject]()
    
    var offset: Int = 0
    var pageSize: Int = 20
    
    var stop: Bool = false
    
    init(pageSize: Int, searchData: SearchQueryData) {
        self.pageSize = pageSize
        self.searchQueryData = searchData
    }
    
    func loadNextPage(completion: @escaping (Bool, Int, Int, String) -> Void) {
        let parameters: [String: String] = self.searchQueryData!.getForParams(size: self.pageSize, offset: self.offset)
        
        var urlPart: String
        switch searchQueryData!.type {
        case .trips:
            urlPart = trips
            break
        case .users:
            urlPart = users
            break
        }

        Alamofire.request(mainURL + urlPart, method: .get, parameters: parameters, encoding: URLEncoding.default, headers: getAuthHeader()).validate().responseJSON { response in
            switch response.result {
            case .success(let json):
                let result = JSON(json)
                
                var newResults: [AnyObject]
                switch self.searchQueryData!.type {
                case .trips:
                    newResults = Trips.parse(json: result)
                    break
                case .users:
                    newResults = Users.parse(json: result)
                    break
                }
                
                self.stop = newResults.count < self.pageSize
                self.offset += newResults.count
                let lastDisplayed = self.searchResults.count
                self.searchResults.append(contentsOf: newResults)
                
                completion(true, lastDisplayed, newResults.count, "")
                
                break
                
            case .failure(_):
                let errorDescription: String = errorFromResponse(response: response)
                completion(false, 0, 0, errorDescription)
                break
            }
            
        }
    }
    
    func reload(completion: @escaping (Bool, Int, Int, String) -> Void) {
        reset()
        loadNextPage(completion: completion)
    }
    
    func reset() {
        searchResults.removeAll()
        offset = 0
    }
}
