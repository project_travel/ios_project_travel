//
//  PostsModel.swift
//  Inspiry
//
//  Created by Alexander Danilyak on 03/08/16.
//  Copyright © 2016 Inspiry. All rights reserved.
//

import Foundation
import Alamofire
import SwiftyJSON

class PostsModel {
    var tripId: String!
    
    var stop: Bool = false
    var allPosts: [Post] = []
    
    private var offset: Int = 0
    private var pageSize: Int
    
    init(tripId: String!, pageSize: Int) {
        self.tripId = tripId
        self.pageSize = pageSize
    }
    
    func loadNextPage(completion: @escaping (Bool, Int, Int, String) -> Void) {
        let parameters: [String: String] = [apiKeyPostsSize: String(pageSize),
                                            apiKeyPostsOffset: String(offset)]
                
        Alamofire.request(mainURL + trips + "/" + tripId + posts, method: .get, parameters: parameters, encoding: URLEncoding.default, headers: getAuthHeader()).validate().responseJSON { response in
            switch response.result {
            case .success(let json):
                let result = JSON(json)
                let newPosts = Posts.parse(json: result)
                self.stop = newPosts.count < self.pageSize
                self.offset += newPosts.count
                let lastDisplayed = self.allPosts.count
                self.allPosts.append(contentsOf: newPosts)
                
                completion(true, lastDisplayed, newPosts.count, "")
                
                break
                
            case .failure(_):
                let errorDescription: String = errorFromResponse(response: response)
                completion(false, 0, 0, errorDescription)
                break
            }
        }
    }
    
    func reset() {
        allPosts.removeAll()
        offset = 0
    }
    
    func reload(completion: @escaping (Bool, Int, Int, String) -> Void) {
        allPosts.removeAll()
        offset = 0
        loadNextPage(completion: completion)
    }
}
