//
//  Post.swift
//  Inspiry
//
//  Created by Alexander Danilyak on 03/08/16.
//  Copyright © 2016 Inspiry. All rights reserved.
//

import Foundation
import SwiftyJSON

enum PostSourceType: String {
    case instagram = "instagram"
    case twitter = "twitter"
    case facebook = "facebook"
}

class PostSource {
    var type: PostSourceType
    var url: URL
    
    init(json: JSON) {
        type = PostSourceType(rawValue: json[apiKeyPostSourceName].stringValue)!
        url = URL.init(string: json[apiKeyPostSourceUrl].stringValue)!
    }
}

class PostLocation {
    var type: String
    var lon: Double
    var lat: Double
    
    init(json: JSON) {
        type = json[apiKeyPostLocationType].stringValue
        lon = json[apiKeyPostLocationCoordinates].arrayValue[0].doubleValue
        lat = json[apiKeyPostLocationCoordinates].arrayValue[1].doubleValue
    }
}

class Post {
    
    var id: String
    var owner: String
    
    var text: String
    var isVisible: Bool
    
    var created: Date
    var modified: Date
    
    var source: PostSource
    
    var hashtags: [String]
    
    var photos: [PhotoWrap]
    
    var location: PostLocation?
    
    init(json: JSON) {
        id = json[apiKeyPostId].stringValue
        owner = json[apiKeyPostOwner].stringValue
        
        text = json[apiKeyPostText].stringValue
        isVisible = json[apiKeyPostVisible].boolValue
        
        created = json[apiKeyPostCreated].stringValue.dateRcf3339!
        modified = json[apiKeyPostModified].stringValue.dateRcf3339!
        
        source = PostSource.init(json: json[apiKeyPostSource])
        
        hashtags = []
        for hashtag in json[apiKeyPostHashtags].arrayValue {
            hashtags.append(hashtag.stringValue)
        }
        
        photos = []
        for photo in json[apiKeyPostPhoto].arrayValue {
            photos.append(PhotoWrap.init(json: photo))
        }
        
        location = json[apiKeyPostLocation] == nil ? nil : PostLocation.init(json: json[apiKeyPostLocation])
    }
}

class Posts {
    static func parse(json: JSON) -> [Post] {
        var posts: [Post] = []
        for post in json.arrayValue {
            posts.append(Post.init(json: post))
        }
        return posts
        
    }
}
