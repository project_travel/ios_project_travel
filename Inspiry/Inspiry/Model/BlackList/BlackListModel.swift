//
//  BlackListModel.swift
//  Inspiry
//
//  Created by Alexander Danilyak on 22/10/2016.
//  Copyright © 2016 Inspiry. All rights reserved.
//

import Foundation
import Alamofire
import SwiftyJSON

class BlackListModel {
    var blackList: [User] = []
    
    var pageSize: Int = 50
    var stop: Bool = false
    var offset: Int = 0
    
    func getBlackList(completion: @escaping (Bool, Int, Int, String) -> Void) {
        if UserDefaults.standard.getAccessToken() != nil {
            let parameters: [String: Any] = [apiKeySize: self.pageSize,
                                             apiKeyOffset: self.offset]
            Alamofire.request(mainURL + users + "/" + StoredUser.shared.user!.username + blacklist + users, method: .get, parameters: parameters, encoding: URLEncoding.default, headers: getAuthHeader()).validate().responseJSON { response in
                switch response.result {
                case .success(let json):
                    let result = JSON(json)
                    let users = Users.parse(json: result)
                    
                    self.stop = users.count < self.pageSize
                    self.offset += users.count
                    let lastDisplayed = self.blackList.count
                    
                    self.blackList.append(contentsOf: users)
                    
                    completion(true, lastDisplayed, users.count, "")
                    break
                    
                case .failure(_):
                    let errorDescription: String = errorFromResponse(response: response)
                    completion(false, 0, 0, errorDescription)
                    break
                }
            }
        } else {
            completion(false, 0, 0, Localize(string: "noAccessToken"))
        }
    }
    
    func removeFromBlackList(index: Int, completion: @escaping (Bool, Int, String) -> Void) {
        UIHelpers.shared.showLoadingAlert(text: Localize(string: "wait"))
        if UserDefaults.standard.getAccessToken() != nil {
            let deleteUsername = blackList[index].username!
            Alamofire.request(mainURL + users + "/" + StoredUser.shared.user!.username + blacklist,
                              method: .delete,
                              parameters: [apiKeyUsername: deleteUsername],
                              encoding: JSONEncoding.default,
                              headers: getAuthHeader()).validate().responseJSON { response in
                UIHelpers.shared.closeAlert()
                switch response.result {
                case .success(_):
                    self.blackList.remove(at: index)
                    completion(true, index, "")
                    break
                    
                case .failure(_):
                    let errorDescription: String = errorFromResponse(response: response)
                    completion(false, 0, errorDescription)
                    break
                }
            }
        } else {
            completion(false, 0, Localize(string: "noAccessToken"))
        }
    }
    
    static func addToBlackList(parameters: [String: Any], completion: @escaping (Bool, String) -> Void) {
        UIHelpers.shared.showLoadingAlert(text: Localize(string: "wait"))
        if UserDefaults.standard.getAccessToken() != nil {
            Alamofire.request(mainURL + users + "/" + StoredUser.shared.user!.username + blacklist,
                              method: .post,
                              parameters: parameters,
                              encoding: JSONEncoding.default,
                              headers: getAuthHeader()).validate().responseJSON { response in
                                UIHelpers.shared.closeAlert()
                                switch response.result {
                                case .success(_):
                                    completion(true, "")
                                    break
                                    
                                case .failure(_):
                                    let errorDescription: String = errorFromResponse(response: response)
                                    completion(false, errorDescription)
                                    break
                                }
            }
        } else {
            completion(false, Localize(string: "noAccessToken"))
        }
    }
    
    static func addUserToBlackList(username: String, completion: @escaping (Bool, String) -> Void) {
        BlackListModel.addToBlackList(parameters: [apiKeyUsername: username], completion: completion)
    }
    
    static func addPostToBlackList(postId: String, completion: @escaping (Bool, String) -> Void) {
        BlackListModel.addToBlackList(parameters: [apiKeyBlackListPost: postId], completion: completion)
    }
    
    static func addTripToBlackList(tripId: String, completion: @escaping (Bool, String) -> Void) {
        BlackListModel.addToBlackList(parameters: [apiKeyBlackListTrip: tripId], completion: completion)
    }
}
