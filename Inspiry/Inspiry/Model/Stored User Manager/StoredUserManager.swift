//
//  StoredUserManager.swift
//  Inspiry
//
//  Created by Alexander Danilyak on 19/07/16.
//  Copyright © 2016 Inspiry. All rights reserved.
//

import Foundation
import SwiftyJSON
import Alamofire

// Manager for access and update logged user data

class StoredUserManager {
    
    // Get logged user data and update Stored User
    func getLoggedUser(completion: ((Bool, String, User?) -> Void)?) {
        if UserDefaults.standard.getAccessToken() != nil {
            Alamofire.request(mainURL + users + me,
                              method: .get,
                              headers: getAuthHeader()).validate().responseJSON { response in
                                switch response.result {
                                case .success(let json):
                                    let result = JSON(json)
                                    let user = User.init(_jsonResponse: result)
                                    StoredUser.shared.set(user: user)
                                    StoredLinkedSocialNetworks.update()
                                    if (completion != nil) {
                                        completion!(true, "", user)
                                    }
                                    break
                                    
                                case .failure(_):
                                    let errorDescription: String = errorFromResponse(response: response)
                                    if (completion != nil) {
                                        completion!(false, errorDescription, nil)
                                    }
                                    break
                                }
            }
        } else {
            if (completion != nil) {
                completion!(false, Localize(string: "noAccessToken"), nil)
            }
        }
    }
    
    // Update stored user data on server
    func update(parameters: [String: AnyObject]?, completion: ((Bool, String?) -> Void)?) {
        if UserDefaults.standard.getAccessToken() != nil {
            Alamofire.request(mainURL + users + me,
                              method: .put,
                              parameters: parameters,
                              encoding: JSONEncoding.default,
                              headers: getAuthHeader()).validate().responseJSON { response in
                                switch response.result {
                                case .success(_):
                                    if (completion != nil) {
                                        completion!(true, nil)
                                    }
                                    break
                                    
                                case .failure(_):
                                    let errorDescription: String = errorFromResponse(response: response)
                                    if (completion != nil) {
                                        completion!(false, errorDescription)
                                    }
                                    break
                                }
            }
        } else {
            if (completion != nil) {
                completion!(false, Localize(string: "noAccessToken"))
            }
        }
    }
    
    // Change password
    func changePassword(old: String!, new: String!, completion: ((Bool, String?) -> Void)?) {
        if UserDefaults.standard.getAccessToken() != nil {
            Alamofire.request(mainURL + users + me + password,
                              method: .put,
                              parameters: [apiKeyNewPassword: new,
                                           apiKeyOldPassword: old],
                              encoding: JSONEncoding.default,
                              headers: getAuthHeader()).validate().responseJSON { response in
                                switch response.result {
                                case .success(_):
                                    if (completion != nil) {
                                        completion!(true, nil)
                                    }
                                    break
                                    
                                case .failure(_):
                                    let errorDescription: String = errorFromResponse(response: response)
                                    if (completion != nil) {
                                        completion!(false, errorDescription)
                                    }
                                    break
                                }
            }
        } else {
            if (completion != nil) {
                completion!(false, Localize(string: "noAccessToken"))
            }
        }
    }
    
    func changeEmail(new: String!, completion: ((Bool, String?) -> Void)?) {
        if UserDefaults.standard.getAccessToken() != nil {
            Alamofire.request(mainURL + users + me,
                              method: .put,
                              parameters: [apiKeyEmail: new],
                              encoding: JSONEncoding.default,
                              headers: getAuthHeader()).validate().responseJSON { response in
                                switch response.result {
                                case .success(_):
                                    StoredUser.shared.set(email: new)
                                    if (completion != nil) {
                                        completion!(true, nil)
                                    }
                                    break
                                    
                                case .failure(_):
                                    let errorDescription: String = errorFromResponse(response: response)
                                    if (completion != nil) {
                                        completion!(false, errorDescription)
                                    }
                                    break
                                }
            }
        } else {
            if (completion != nil) {
                completion!(false, Localize(string: "noAccessToken"))
            }
        }
    }
    
    func deleteUser(completion: ((Bool, String?) -> Void)?) {
        if UserDefaults.standard.getAccessToken() != nil {
            Alamofire.request(mainURL + users + me,
                              method: .delete,
                              encoding: JSONEncoding.default,
                              headers: getAuthHeader()).validate().responseJSON { response in
                                switch response.result {
                                case .success(_):
                                    StoredUser.shared.delete()
                                    UserDefaults.standard.deleteLinksToSocialNetworks()
                                    if (completion != nil) {
                                        completion!(true, nil)
                                    }
                                    break
                                    
                                case .failure(_):
                                    let errorDescription: String = errorFromResponse(response: response)
                                    if (completion != nil) {
                                        completion!(false, errorDescription)
                                    }
                                    break
                                }
            }
        } else {
            if (completion != nil) {
                completion!(false, Localize(string: "noAccessToken"))
            }
        }
    }
    
    func clearAvatar(completion: @escaping (Bool, String) -> Void) {
        self.clearImage(destinationUrl: mainURL + users + me + avatar, completion: completion)
    }
    
    func clearCover(completion: @escaping (Bool, String) -> Void) {
        self.clearImage(destinationUrl: mainURL + users + me + background, completion: completion)
    }
    
    func clearImage(destinationUrl: String, completion: @escaping (Bool, String) -> Void) {
        if UserDefaults.standard.getAccessToken() != nil {
            Alamofire.request(destinationUrl,
                              method: .delete,
                              encoding: JSONEncoding.default,
                              headers: getAuthHeader()).validate().responseJSON { response in
                                switch response.result {
                                case .success(_):
                                    completion(true, "")
                                    break
                                    
                                case .failure(_):
                                    let errorDescription: String = errorFromResponse(response: response)
                                    completion(false, errorDescription)
                                    break
                                }
            }
        } else {
            completion(false, Localize(string: "noAccessToken"))
        }
    }
    
    func uploadAvatar(file: URL, completion: @escaping (Bool, String) -> Void) {
        self.uploadImage(imageType: .avatar, destinationUrl: mainURL + users + me + avatar, file: file, completion: completion)
    }
    
    func uploadCover(file: URL, completion: @escaping (Bool, String) -> Void) {
        self.uploadImage(imageType: .cover, destinationUrl: mainURL + users + me + background, file: file, completion: completion)
    }
    
    private func uploadImage(imageType: ChoseImageType, destinationUrl: String, file: URL, completion: @escaping (Bool, String) -> Void) {
        if UserDefaults.standard.getAccessToken() != nil {
            Alamofire.upload(multipartFormData: { multipartFormData in
                multipartFormData.append(file, withName: "file")
                }, usingThreshold: SessionManager.multipartFormDataEncodingMemoryThreshold, to: destinationUrl, method: .post, headers: getAuthHeader(), encodingCompletion: { encodingResult in
                    switch encodingResult {
                    case .success(let upload, _, _):
                        upload.responseJSON { response in
                            switch response.result {
                            case .success(_):
                                let result = JSON(response.result.value!)
                                switch imageType {
                                case .avatar:
                                    StoredUser.shared.set(avatar: result[apiKeyAvatarUrl].stringValue)
                                    break
                                case .cover:
                                    StoredUser.shared.set(avatar: result[apiKeyAvatarUrl].stringValue)
                                    break
                                case .notset:
                                    break
                                }
                                completion(true, "")
                                break
                            case .failure(_):
                                let errorDescription: String = errorFromResponse(response: response)
                                completion(false, errorDescription)
                                break
                            }
                        }
                    case .failure(_):
                        completion(true, "error")
                    }
            })
        } else {
            completion(false, Localize(string: "noAccessToken"))
        }
    }
    
    // Make server harvest data from other social networks and recreate (or add) trips
    func reloadTrips(completion: @escaping (Bool, String?) -> Void) {
        if UserDefaults.standard.getAccessToken() != nil {
            Alamofire.request(mainURL + users + me + trips,
                              method: .put,
                              encoding: JSONEncoding.default,
                              headers: getAuthHeader()).validate().responseJSON { response in
                                switch response.result {
                                case .success(_):
                                    completion(true, nil)
                                    break
                                    
                                case .failure(_):
                                    let errorDescription: String = errorFromResponse(response: response)
                                    completion(false, errorDescription)
                                    break
                                }
            }
        } else {
            completion(false, Localize(string: "noAccessToken"))
        }
    }
}
