//
//  UserTripsModel.swift
//  Inspiry
//
//  Created by Alexander Danilyak on 27/07/16.
//  Copyright © 2016 Inspiry. All rights reserved.
//

import Foundation
import Alamofire
import SwiftyJSON

class UserTripsModel {
    var userName: String!
    
    var stop: Bool = false
    var allTrips: [Trip] = []
    
    private var offset: Int = 0
    private var pageSize: Int
    
    init(userName: String!, pageSize: Int) {
        self.userName = userName
        self.pageSize = pageSize
    }
    
    func loadNextPage(completion: @escaping (Bool, Int, Int, String) -> Void) {
        let parameters: [String: String] = [apiKeyTripsSize: String(pageSize),
                                            apiKeyTripsOffset: String(offset)]
        
        Alamofire.request(mainURL + users + "/" + userName + trips, method: .get, parameters: parameters, encoding: URLEncoding.default, headers: getAuthHeader()).validate().responseJSON { response in
            switch response.result {
            case .success(let json):
                let result = JSON(json)
                let newTrips = Trips.parse(json: result)
                self.stop = newTrips.count < self.pageSize
                self.offset += newTrips.count
                let lastDisplayed = self.allTrips.count
                self.allTrips.append(contentsOf: newTrips)
                
                completion(true, lastDisplayed, newTrips.count, "")
                
                break
                
            case .failure(_):
                let errorDescription: String = errorFromResponse(response: response)
                completion(false, 0, 0, errorDescription)
                break
            }
            
        }
    }
    
    func reset() {
        allTrips.removeAll()
        offset = 0
    }
    
    func reload(completion: @escaping (Bool, Int, Int, String) -> Void) {
        allTrips.removeAll()
        offset = 0
        loadNextPage(completion: completion)
    }
}
