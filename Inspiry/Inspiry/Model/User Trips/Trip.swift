//
//  Trips.swift
//  Inspiry
//
//  Created by Alexander Danilyak on 27/07/16.
//  Copyright © 2016 Inspiry. All rights reserved.
//

import Foundation
import SwiftyJSON

class Trips {
    static func parse(json: JSON) -> [Trip] {
        var trips: [Trip] = []
        for trip in json.arrayValue {
            trips.append(Trip.init(json: trip))
        }
        return trips
    }
}

class TripContry {
    // id = country code
    var id: String!
    // value = localized country name
    var value: String!
    
    init(json: JSON) {
        self.id = (json[apiKeyTripCountryId].stringValue).lowercased()
        self.value = json[apiKeyTripCountryValue].stringValue
    }
}

class TripCountries {
    static func parse(json: JSON) -> [TripContry] {
        var countries: [TripContry] = []
        for country in json.arrayValue {
            countries.append(TripContry.init(json: country))
        }
        return countries
    }
}

class Trip {
    
    var id: String
    var name: String
    var owners: [String]
    var start: Date
    var end: Date
    
    var description: String
    
    var countries: [TripContry]
    var cities: [String]
    
    var photos: [PhotoWrap]
    
    var likes: Int
    var isLikedByMe: Bool
    
    var hashtags: [String]
    
    var modified: Date
    
    var posts: [Post]
    var ad: URL
    
    init(json: JSON) {
        id = json[apiKeyTripId].stringValue
        
        name = json[apiKeyTripName].stringValue
        
        owners = []
        for owner in json[apiKeyTripOwners].arrayValue {
            owners.append(owner.stringValue)
        }
        
        start = json[apiKeyTripStart].stringValue.dateRcf3339!
        end = json[apiKeyTripEnd].stringValue.dateRcf3339!
        
        countries = TripCountries.parse(json: json[apiKeyTripCountries])
        
        photos = []
        for photo in json[apiKeyTripAvatar].arrayValue {
            photos.append(PhotoWrap.init(json: photo))
        }
        
        cities = []
        for city in json[apiKeyTripCities].arrayValue {
            cities.append(city.stringValue)
        }
        
        likes = json[apiKeyTripLikes].intValue
        isLikedByMe = json[apiKeyTripHasLiked].boolValue
        
        hashtags = []
        for hashtag in json[apiKeyTripHashtags].arrayValue {
            hashtags.append(hashtag.stringValue)
        }
        
        modified = Date()//json[apiKeyTripModified].stringValue.dateRcf3339!
        
        description = json[apiKeyTripDescription].stringValue
        
        posts = []
        
        ad = URL.init(string: json[apiKeyTripAd].stringValue)!
    }
    
    func update(with trip: Trip) {
        self.id = trip.id
        self.name = trip.name
        self.owners = trip.owners
        self.start = trip.start
        self.end = trip.end
        self.countries = trip.countries
        self.photos = trip.photos
        self.cities = trip.cities
        self.likes = trip.likes
        self.isLikedByMe = trip.isLikedByMe
        self.hashtags = trip.hashtags
        self.modified = trip.modified
        self.description = trip.description
        self.ad = trip.ad
    }
}
