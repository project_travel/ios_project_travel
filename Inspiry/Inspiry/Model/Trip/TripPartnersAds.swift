//
//  TripPartnersAds.swift
//  Inspiry
//
//  Created by Alexander Danilyak on 11/09/16.
//  Copyright © 2016 Inspiry. All rights reserved.
//

import Foundation
import SwiftyJSON
import Alamofire

enum TripPartnersSource: String {
    case booking = "booking"
    case skyscanner = "skyscanner"
}

class TripPartnersAd {
    var source: TripPartnersSource
    var city: String
    var country: String
    var url: URL
    var origin: Bool
    
    init(json: JSON) {
        self.source = TripPartnersSource.init(rawValue: json[apiKeyAdSource].stringValue)!
        self.city = json[apiKeyAdCity].stringValue
        self.country = json[apiKeyAdCoutry].stringValue
        self.origin = json[apiKeyAdOrigin].boolValue
        self.url = json[apiKeyAdUrl].URL!
    }
}

class TripPartnersAds {
    static func parse(json: JSON) -> [TripPartnersAd] {
        var ads: [TripPartnersAd] = []
        for ad in json.arrayValue {
            ads.append(TripPartnersAd.init(json: ad))
        }
        return ads
    }
}

class TripPartnersAdsManager {
    func loadTripPartnersAds(url: String, completion: @escaping (Bool, [TripPartnersAd]?, String) -> Void) {
        Alamofire.request(url, method: .get, headers: getAuthHeader()).validate().responseJSON { response in
            switch response.result {
            case .success(let json):
                let result = JSON(json)
                let ads = TripPartnersAds.parse(json: result)
                completion(true, ads, "")
                break
                
            case .failure(_):
                let errorDescription: String = errorFromResponse(response: response)
                completion(false, nil, errorDescription)
                break
            }
        }
    }
}
