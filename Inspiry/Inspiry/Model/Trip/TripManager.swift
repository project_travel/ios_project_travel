//
//  TripManager.swift
//  Inspiry
//
//  Created by Alexander Danilyak on 01/08/16.
//  Copyright © 2016 Inspiry. All rights reserved.
//

import Foundation
import SwiftyJSON
import Alamofire

class TripManager {
    
    func loadTrip(with id: String, completion: @escaping (Bool, Trip?, String) -> Void) {
        Alamofire.request(mainURL + trips + "/" + id, method: .get, headers: getAuthHeader()).validate().responseJSON { response in
            switch response.result {
            case .success(let json):
                let result = JSON(json)
                let trip = Trip.init(json: result)
                
                completion(true, trip, "")
                
                break
                
            case .failure(_):
                let errorDescription: String = errorFromResponse(response: response)
                completion(false, nil, errorDescription)
                break
            }
            
        }
    }
    
    func setLike(with isLike: Bool, and id: String, completion: @escaping (Bool, String?) -> Void) {
        
        let method: HTTPMethod = isLike ? .post : .delete
        
        Alamofire.request(mainURL + trips + "/" + id + likes, method: method, headers: getAuthHeader()).validate().responseJSON { response in
            switch response.result {
            case .success(_):
                completion(true, nil)
                break
                
            case .failure(_):
                let errorDescription: String = errorFromResponse(response: response)
                completion(false, errorDescription)
                break
            }
        }
    }
    
    func update(with parameters:[String: String], and id: String, completion: @escaping (Bool, String?) -> Void) {
        Alamofire.request(mainURL + trips + "/" + id, method: .put, parameters: parameters, encoding: JSONEncoding.default, headers: getAuthHeader()).validate().responseJSON { response in
            switch response.result {
            case .success(_):
                completion(true, nil)
                break
                
            case .failure(_):
                let errorDescription: String = errorFromResponse(response: response)
                completion(false, errorDescription)
                break
            }
        }
    }
}
