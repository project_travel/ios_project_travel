//
//  User.swift
//  Inspiry
//
//  Created by Alexander Danilyak on 11/07/16.
//  Copyright © 2016 Inspiry. All rights reserved.
//

import Foundation
import SwiftyJSON

let DefaultBirthDate: Date! = ("0001-01-01T00:00:00Z").dateRcf3339

enum GenderType: String {
    case male = "male"
    case female = "female"
    case notset = ""
}

// Class for storing user properties
class User: NSObject, NSCoding {
    // Username is a unique key for server-side DB
    var username: String!
    
    var email: String!
    var firstname: String?
    var surname: String?
    
    var status: String?
    var birthDate: Date?
    var gender: GenderType!
    
    var registrationDate: Date?
    
    var followers: Int
    var following: Int
    
    var hasFollowed: Bool
    
    var inBlacklist: Bool
    
    var avatar: [Photo]
    var background: [Photo]
    
    init(_username: String?,
         _email: String!,
         _firstname: String?,
         _surname: String?,
         _status: String?,
         _birthDate: Date?,
         _gender: GenderType!,
         _registrationDate: Date?,
         _followers: Int,
         _following: Int,
         _hasFollowed: Bool,
         _inBlacklist: Bool,
         _avatar: [Photo],
         _background: [Photo]) {
        username = _username
        email = _email
        firstname = _firstname
        surname = _surname
        status = _status
        birthDate = _birthDate
        gender = _gender
        registrationDate = _registrationDate
        followers = _followers
        following = _following
        hasFollowed = _hasFollowed
        inBlacklist = _inBlacklist
        avatar = _avatar
        background = _background
    }
    
    convenience init(_username: String!,
         _email: String!) {
        self.init(_username: _username,
                  _email: _email,
                  _firstname: nil,
                  _surname: nil,
                  _status: nil,
                  _birthDate: DefaultBirthDate,
                  _gender: .notset,
                  _registrationDate: nil,
                  _followers: 0,
                  _following: 0,
                  _hasFollowed: false,
                  _inBlacklist: false,
                  _avatar: [],
                  _background: [])
    }
    
    convenience init(_jsonResponse: JSON) {
        var birthDate = (_jsonResponse[apiKeyBirthdate].stringValue).dateRcf3339
        if birthDate == nil { birthDate = DefaultBirthDate }
        
        self.init(_username: _jsonResponse[apiKeyUsername].stringValue,
                  _email: _jsonResponse[apiKeyEmail].stringValue,
                  _firstname: _jsonResponse[apiKeyFirstname].stringValue,
                  _surname: _jsonResponse[apiKeySurname].stringValue,
                  _status: _jsonResponse[apiKeyStatus].stringValue,
                  _birthDate: birthDate,
                  _gender: GenderType(rawValue: _jsonResponse[apiKeyGender].stringValue),
                  _registrationDate: (_jsonResponse[apiKeyRegistrationDate].stringValue).dateRcf3339,
                  _followers: _jsonResponse[apiKeyFollowers].intValue,
                  _following: _jsonResponse[apiKeyFollowing].intValue,
                  _hasFollowed: _jsonResponse[apiKeyHasFollowed].boolValue,
                  _inBlacklist: _jsonResponse[apiKeyInBlacklist].boolValue,
                  _avatar: PhotoSizeCollection.parse(json: _jsonResponse[apiKeyAvatar]),
                  _background: PhotoSizeCollection.parse(json: _jsonResponse[apiKeyBackground]))
    }
    
    func updateWith(dictionary: [String: String]) {
        if dictionary[apiKeyUsername] != nil { self.username = dictionary[apiKeyUsername] }
        if dictionary[apiKeyFirstname] != nil { self.firstname = dictionary[apiKeyFirstname] }
        if dictionary[apiKeySurname] != nil { self.surname = dictionary[apiKeySurname] }
        if dictionary[apiKeyStatus] != nil { self.status = dictionary[apiKeyStatus] }
        if dictionary[apiKeyGender] != nil { self.gender = GenderType(rawValue: dictionary[apiKeyGender]!)  }
        if dictionary[apiKeyBirthdate] != nil { self.birthDate =  (dictionary[apiKeyBirthdate]!).dateRcf3339 }
    }
    
    // MARK: NSCoding
    
    required convenience init?(coder aDecoder: NSCoder) {
        let _username = aDecoder.decodeObject(forKey: apiKeyUsername) as? String,
            _email = aDecoder.decodeObject(forKey: apiKeyEmail) as? String,
            _firstname = aDecoder.decodeObject(forKey: apiKeyFirstname) as? String,
            _surname = aDecoder.decodeObject(forKey: apiKeySurname) as? String,
            _status = aDecoder.decodeObject(forKey: apiKeyStatus) as? String,
            _birthDate = aDecoder.decodeObject(forKey: apiKeyBirthdate) as? Date,
            _gender = aDecoder.decodeObject(forKey: apiKeyGender) as? String,
            _registrationDate = aDecoder.decodeObject(forKey: apiKeyRegistrationDate) as? Date,
            _followers = aDecoder.decodeInteger(forKey: apiKeyFollowers),
            _following = aDecoder.decodeInteger(forKey: apiKeyFollowing),
            _hasFollowed = aDecoder.decodeBool(forKey: apiKeyHasFollowed),
            _inBlacklist = aDecoder.decodeBool(forKey: apiKeyInBlacklist),
            _avatar = aDecoder.decodeObject(forKey: apiKeyAvatar) as? [Photo],
            _background = aDecoder.decodeObject(forKey: apiKeyBackground) as? [Photo]
        
        self.init(_username: _username,
                  _email: _email,
                  _firstname: _firstname,
                  _surname: _surname,
                  _status: _status,
                  _birthDate: _birthDate,
                  _gender: GenderType(rawValue: _gender!),
                  _registrationDate: _registrationDate,
                  _followers: _followers,
                  _following: _following,
                  _hasFollowed: _hasFollowed,
                  _inBlacklist: _inBlacklist,
                  _avatar: _avatar!,
                  _background: _background!)
    }
    
    func encode(with aDecoder: NSCoder) {
        aDecoder.encode(self.username, forKey: apiKeyUsername)
        aDecoder.encode(self.email, forKey: apiKeyEmail)
        aDecoder.encode(self.firstname, forKey: apiKeyFirstname)
        aDecoder.encode(self.surname, forKey: apiKeySurname)
        aDecoder.encode(self.status, forKey: apiKeyStatus)
        aDecoder.encode(self.birthDate, forKey: apiKeyBirthdate)
        aDecoder.encode(self.gender.rawValue, forKey: apiKeyGender)
        aDecoder.encode(self.registrationDate, forKey: apiKeyRegistrationDate)
        aDecoder.encode(self.followers, forKey: apiKeyFollowers)
        aDecoder.encode(self.following, forKey: apiKeyFollowing)
        aDecoder.encode(self.hasFollowed, forKey: apiKeyHasFollowed)
        aDecoder.encode(self.inBlacklist, forKey: apiKeyInBlacklist)
        aDecoder.encode(self.avatar, forKey: apiKeyAvatar)
        aDecoder.encode(self.background, forKey: apiKeyBackground)
    }
}

class Users {
    static func parse(json: JSON) -> [User] {
        var users: [User] = []
        for user in json.arrayValue {
            users.append(User.init(_jsonResponse: user))
        }
        return users
    }
}

// Stored user is a "Logged in" user
// This user is stored in the user defaults
class StoredUser {
    static let shared = StoredUser()
    
    var user: User?
    
    private init() {
        user = UserDefaults.standard.getSavedUser()
    }
    
    func set(username: String!, email: String!) {
        self.user = User.init(_username: username, _email: email)
        self.save()
    }
    
    func set(user: User!) {
        self.user = user
        self.save()
    }
    
    func set(email: String!) {
        self.user?.email = email
        self.save()
    }
    
    func set(avatar: String) {
        //self.user?.avatar = URL.init(string: avatar)
        self.save()
    }
    
    func delete() {
        UserDefaults().clearBothTokens()
        UserAuth.shared.state = .noUser
        
        self.user = nil
        self.save()
    }
    
    func save() {
        if(self.user != nil) {
            UserDefaults.standard.saveUser(_user: self.user!)
        }
    }
}

let inspiryStoredUser = "storedUser"

// Extension for stored user
extension UserDefaults {
    
    func saveUser(_user: User) {
        let data: Data = NSKeyedArchiver.archivedData(withRootObject: _user)
        UserDefaults.standard.set(data, forKey: inspiryStoredUser)
    }
    
    func getSavedUser() -> User? {
        let data: Data? = UserDefaults.standard.data(forKey: inspiryStoredUser)
        if data != nil {
            let user = NSKeyedUnarchiver.unarchiveObject(with: data!) as? User
            return user
        } else {
            return nil
        }
    }
    
}
