//
//  LinkedSocialNetworksData.swift
//  Inspiry
//
//  Created by Alexander Danilyak on 15/07/16.
//  Copyright © 2016 Inspiry. All rights reserved.
//

import Foundation
import SwiftyJSON

enum SocialNetworkName: String {
    case instagram = "instagram"
    case twitter = "twitter"
    case facebook = "facebook"
}

let reloadNotification = Notification.Name("reloadNotification")
let loadNamesNotification = Notification.Name("loadNamesNotification")

// Singleton for traking linked state of external social networks, such as: Instagram, Twitter, etc.
class StoredLinkedSocialNetworks: LinkedSocialNetworks {
    static let shared = LinkedSocialNetworks.init(isForStored: true)
    
    static func set(socialNetworks: [SocialNetwork]!) {
        StoredLinkedSocialNetworks.shared.socialNetworks = socialNetworks
        StoredLinkedSocialNetworks.save()
    }
    
    static func save() {
        UserDefaults.standard.saveLinkedSocialNetworks(socialNetworks: StoredLinkedSocialNetworks.shared.socialNetworks)
    }
    
    static func update() {
        if(StoredUser.shared.user == nil) { return }
        
        LinkManager().getSocialNetworks(user: StoredUser.shared.user!) { (isSuccess, socialNetworks, errorDescription) in
            if(isSuccess) {
                StoredLinkedSocialNetworks.set(socialNetworks: socialNetworks!)
            } else {
                print("[LINK SOCIAL UPDATE]" + errorDescription!)
            }
        }
    }
    
    static func delete() {
        
    }
}

class LinkedSocialNetworks {
    var socialNetworks: [SocialNetwork]!
    
    init(isForStored: Bool) {
        if(isForStored) {
            socialNetworks = UserDefaults.standard.getLinkedSocialNetworks()
        } else {
            socialNetworks = LinkedSocialNetworks.allNetworksNotLinked()
        }
    }
    
    // Set new value for "_socialNetwork" or create if needed
    func updateSocialNetwork(_socialNetwork: SocialNetwork!) {
        let index = socialNetworks.index { (SocialNetwork) -> Bool in
            return SocialNetwork.name == _socialNetwork.name
        }
        
        if(index == nil) {
            socialNetworks.append(_socialNetwork)
        } else {
            socialNetworks[index!] = _socialNetwork
        }
    }
    
    // Method for creation basic structure, where all networks are unlinked
    static func allNetworksNotLinked() -> [SocialNetwork]! {
        let instragram = SocialNetwork.init(_name: .instagram, _isLinked: false)
        let twitter = SocialNetwork.init(_name: .twitter, _isLinked: false)
        let facebook = SocialNetwork.init(_name: .facebook, _isLinked: false)
        
        return [instragram, twitter, facebook]
    }
    
    // Checking response from social networks and send notification that value changed
    func handleResponse(url: URL!) {
        for social in socialNetworks {
            let urlForSocial = redirect + social.name.rawValue
            if(url.absoluteString == urlForSocial) {
                let newSocial = SocialNetwork.init(_name: social.name, _isLinked: true)
                self.updateSocialNetwork(_socialNetwork: newSocial)
            } else if(url.absoluteString.contains(urlForSocial)) {
                let newSocial = SocialNetwork.init(_name: social.name, _isLinked: false)
                self.updateSocialNetwork(_socialNetwork: newSocial)
            }
        }
        NotificationCenter.default.post(name: reloadNotification, object: nil)
        
        LinkManager().getSocialNetworks(user: StoredUser.shared.user, completion: LinkSocialController.updateNamesCompletion)
    }
}

// Social network connectivity representation
class SocialNetwork: NSObject, NSCoding {
    let name: SocialNetworkName!
    var username: String!
    var isLinked: Bool!
    
    init(_name: SocialNetworkName!,
         _isLinked: Bool) {
        name = _name
        isLinked = _isLinked
        username = ""
    }
    
    init(_name: SocialNetworkName!,
         _isLinked: Bool,
         _username: String) {
        name = _name
        isLinked = _isLinked
        username = _username
    }
    
    static func parse(json: JSON) -> [SocialNetwork] {
        let networks = LinkedSocialNetworks.allNetworksNotLinked()
        
        for social in networks! {
            if json.dictionaryValue[social.name.rawValue] != nil {
                social.username = json.dictionaryValue[social.name.rawValue]!.dictionaryValue[apiKeyUsername]?.stringValue
                social.isLinked = true
            } else {
                social.isLinked = false
            }
        }
        NotificationCenter.default.post(name: reloadNotification, object: nil)
        
        return networks!
    }
    
    // MARK: NSCoding
    
    required convenience init?(coder aDecoder: NSCoder) {
        let _name = SocialNetworkName.init(rawValue: (aDecoder.decodeObject(forKey: "socialName") as? String)!),
        _username = aDecoder.decodeObject(forKey: "socialUsername") as? String,
        _isLinked = aDecoder.decodeObject(forKey: "socialIsLinked") as? Bool
        
        self.init(_name: _name,
                  _isLinked: _isLinked!,
                  _username: _username!)
    }
    
    func encode(with aDecoder: NSCoder) {
        aDecoder.encode(self.name.rawValue, forKey: "socialName")
        aDecoder.encode(self.isLinked, forKey: "socialIsLinked")
        aDecoder.encode(self.username, forKey: "socialUsername")
    }
}

let inspiryLinkedSocialNetworks = "linkedSocialNetworks"

// Extension for quick access
extension UserDefaults {
    
    func saveLinkedSocialNetworks(socialNetworks: [SocialNetwork]!) {
        let data: Data = NSKeyedArchiver.archivedData(withRootObject: socialNetworks)
        UserDefaults.standard.set(data, forKey: inspiryLinkedSocialNetworks)
    }
    
    func getLinkedSocialNetworks() -> [SocialNetwork]! {
        let _socialNetworksData = UserDefaults.standard.data(forKey: inspiryLinkedSocialNetworks)
        if _socialNetworksData == nil { return LinkedSocialNetworks.allNetworksNotLinked() }
        
        let _socialNetworks = NSKeyedUnarchiver.unarchiveObject(with: _socialNetworksData!) as! [SocialNetwork]!
        return _socialNetworks
    }
    
    func deleteLinksToSocialNetworks() {
        UserDefaults.standard.removeObject(forKey: inspiryLinkedSocialNetworks)
    }
}
