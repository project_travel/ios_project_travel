//
//  Photo.swift
//  Inspiry
//
//  Created by Alexander Danilyak on 01/09/16.
//  Copyright © 2016 Inspiry. All rights reserved.
//

import Foundation
import SwiftyJSON

class Photo: NSObject, NSCoding {
    var height: Int
    var width: Int
    var type: String
    var url: URL
    
    init(json: JSON) {
        self.height = json[apiKeyAvatarHeight].intValue
        self.width = json[apiKeyAvatarWidth].intValue
        self.type = json[apiKeyAvatarType].stringValue
        self.url = URL(string: json[apiKeyAvatarUrl].stringValue)!
    }
    
    init(_height: Int, _width: Int, _type: String, _url: URL) {
        self.height = _height
        self.width = _width
        self.type = _type
        self.url = _url
    }
    
    // MARK: NSCoding
    
    required convenience init?(coder aDecoder: NSCoder) {
        let _height = aDecoder.decodeInteger(forKey: apiKeyAvatarHeight),
            _width = aDecoder.decodeInteger(forKey: apiKeyAvatarWidth),
            _type = aDecoder.decodeObject(forKey: apiKeyAvatarType) as! String,
            _url = aDecoder.decodeObject(forKey: apiKeyAvatarUrl) as! URL
    
        
        self.init(_height: _height,
                  _width: _width,
                  _type: _type,
                  _url: _url)
    }
    
    func encode(with aDecoder: NSCoder) {
        aDecoder.encode(self.height, forKey: apiKeyAvatarHeight)
        aDecoder.encode(self.width, forKey: apiKeyAvatarWidth)
        aDecoder.encode(self.type, forKey: apiKeyAvatarType)
        aDecoder.encode(self.url, forKey: apiKeyAvatarUrl)
    }
}

class PhotoSizeCollection {
    static func parse(json: JSON) -> [Photo] {
        var photos: [Photo] = []
        for photo in json.arrayValue {
            photos.append(Photo.init(json: photo))
        }
        return photos
    }
    
    static func getBest(photos: [Photo]) -> Photo? {
        var best = photos.first
        for photo in photos {
            if photo.width >= (best?.width)! {
                best = photo
            }
        }
        return best
    }
}
