//
//  UserStatistics.swift
//  Inspiry
//
//  Created by Alexander Danilyak on 24/08/16.
//  Copyright © 2016 Inspiry. All rights reserved.
//

import Foundation

enum StatisticType: String {
    case followers = "followers"
    case following = "following"
}

class Statistic {
    var type: StatisticType
    var count: Int
    
    init(with type: StatisticType, and count: Int) {
        self.type = type
        self.count = count
    }
}

class UserStatistics {
    var statistics: [Statistic]! = []
    
    init(with user: User) {
        self.statistics.append(Statistic.init(with: .followers, and: user.followers))
        self.statistics.append(Statistic.init(with: .following, and: user.following))
    }
}
