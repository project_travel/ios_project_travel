//
//  Notification.swift
//  Inspiry
//
//  Created by Alexander Danilyak on 21/09/2016.
//  Copyright © 2016 Inspiry. All rights reserved.
//

import Foundation
import SwiftyJSON

class NotificationData {
    // Date of notification created
    var created: Date
    var message: String
    
    // Trip id which was liked / commented
    var tripId: String
    var tripName: String
    
    // User who did this action
    var username: String
    var followerUsername: String
    
    // Ids of trips that were updated / added
    var updatedIds: [String]
    var newIds: [String]
    var isUpdateFailed: Bool
    
    var type: PushNotificationType
    
    init(json: JSON) {
        self.type = PushNotificationType(rawValue: json[apiKeyNotificationType].stringValue)!
        self.created = (json[apiKeyNotificationCreated].stringValue).dateRcf3339!
        self.message = json[apiKeyNotificationMessage].stringValue
        
        self.tripId = ""
        self.tripName = ""
        self.username = ""
        self.followerUsername = ""
        self.isUpdateFailed = false
        self.newIds = []
        self.updatedIds = []
        
        switch self.type {
        case .newLike:
            self.tripId = json[apiKeyNotificationTripId].stringValue
            self.tripName = json[apiKeyNotificationTripName].stringValue
            self.username = json[apiKeyNotificationUsername].stringValue
            break
        case .newComment:
            self.tripId = json[apiKeyNotificationTripId].stringValue
            self.tripName = json[apiKeyNotificationTripName].stringValue
            self.username = json[apiKeyNotificationUsername].stringValue
            break
        case .newFollower:
            self.followerUsername = json[apiKeyNotificationFollowerUsername].stringValue
            break
        case .newTripsExtracted:
            self.isUpdateFailed = json[apiKeyNotificationFailed].boolValue
            self.newIds = NotificationData.parseArrayOfStrings(json: json[apiKeyNotificationNewIds])
            self.updatedIds = NotificationData.parseArrayOfStrings(json: json[apiKeyNotificationUpdatedIds])
            break
        }
    }
    
    static func parseArrayOfStrings(json: JSON) -> [String] {
        var array: [String] = []
        for item in json.arrayValue {
            array.append(item.stringValue)
        }
        return array
    }
}

class NotificationsData {
    static func parse(json: JSON) -> [NotificationData] {
        var notifications: [NotificationData] = []
        for notification in json.arrayValue {
            notifications.append(NotificationData.init(json: notification))
        }
        return notifications
    }
}
