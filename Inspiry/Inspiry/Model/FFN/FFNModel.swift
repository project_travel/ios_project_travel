//
//  FFNModel.swift
//  Inspiry
//
//  Created by Alexander Danilyak on 26/07/16.
//  Copyright © 2016 Inspiry. All rights reserved.
//

import Foundation
import Alamofire
import SwiftyJSON

class FFNModel {
    var username: String?
    
    var pageSize: Int = 50
    
    var dataFollowing: [User] = []
    var stopFollowing: Bool = false
    var offsetFollowing: Int = 0
    
    var dataFollowers: [User] = []
    var stopFollowers: Bool = false
    var offsetFollowers: Int = 0
    
    var dataNotifications: [NotificationData] = [NotificationData]()
    var stopNotifications: Bool = false
    var offsetNotifications: Int = 0
    
    var localSearchResultsFollowing: [AnyObject]? = nil
    var localSearchResultsFollowers: [AnyObject]? = nil
    
    init(username: String!, pageSize: Int) {
        self.username = username
        self.pageSize = pageSize
    }
    
    func loadData(for type: FFNType, with completion:@escaping (Bool, Int, Int, String) -> Void) {
        switch type {
        case .following:
            self.getFollowing(username: self.username!, completion: completion)
            break
        case .followers:
            self.getFollowers(username: self.username!, completion: completion)
            break
        case .notifications:
            self.getNotifications(completion: completion)
            break
        case .notset:
            assert(true)
            break
        }
    }
    
    func performSearch(for type: FFNType, with query:String!) {
        switch type {
        case .following:
            self.localSearchResultsFollowing = self.dataFollowing.filter { following in
                return following.username.lowercased().contains(query.lowercased())
            }
            break
        case .followers:
            self.localSearchResultsFollowers = self.dataFollowers.filter { follower in
                return follower.username.lowercased().contains(query.lowercased())
            }
            break
        default:
            assert(true)
        }
    }
    
    // MARK: Following
    
    func getFollowing(username: String!, completion: @escaping (Bool, Int, Int, String) -> Void) {
        if UserDefaults.standard.getAccessToken() != nil {
            let parameters: [String: Any] = [apiKeySize: self.pageSize,
                                                apiKeyOffset: self.offsetFollowing]
            Alamofire.request(mainURL + users + "/" + username + following, method: .get, parameters: parameters, encoding: URLEncoding.default, headers: getAuthHeader()).validate().responseJSON { response in
                                switch response.result {
                                case .success(let json):
                                    let result = JSON(json)
                                    let users = Users.parse(json: result)
                                    
                                    self.stopFollowing = users.count < self.pageSize
                                    self.offsetFollowing += users.count
                                    let lastDisplayed = self.dataFollowing.count
                                    
                                    self.dataFollowing.append(contentsOf: users)
                                    
                                    completion(true, lastDisplayed, users.count, "")
                                    break
                                    
                                case .failure(_):
                                    let errorDescription: String = errorFromResponse(response: response)
                                    completion(false, 0, 0, errorDescription)
                                    break
                                }
            }
        } else {
            completion(false, 0, 0, Localize(string: "noAccessToken"))
        }
    }
    
    // MARK: Followers
    
    func getFollowers(username: String!, completion: @escaping (Bool, Int, Int, String) -> Void) {
        if UserDefaults.standard.getAccessToken() != nil {
            let parameters: [String: Any] = [apiKeySize: self.pageSize, apiKeyOffset: self.offsetFollowers]
            let url = mainURL + users + "/" + username + followers
            Alamofire.request(url, method: .get, parameters: parameters, encoding: URLEncoding.default, headers: getAuthHeader()).validate().responseJSON { response in
                                switch response.result {
                                case .success(let json):
                                    let result = JSON(json)
                                    let users = Users.parse(json: result)
                                    
                                    self.stopFollowers = users.count < self.pageSize
                                    self.offsetFollowers += users.count
                                    let lastDisplayed = self.dataFollowers.count
                                    
                                    self.dataFollowers.append(contentsOf: users)
                                    
                                    completion(true, lastDisplayed, users.count, "")
                                    break
                                    
                                case .failure(_):
                                    let errorDescription: String = errorFromResponse(response: response)
                                    completion(false, 0, 0, errorDescription)
                                    break
                                }
            }
        } else {
            completion(false, 0, 0, Localize(string: "noAccessToken"))
        }
    }
    
    // MARK: Notifications
    // to do - describe special type for notifications
    func getNotifications(completion: @escaping (Bool, Int, Int, String) -> Void) {
        if UserDefaults.standard.getAccessToken() != nil {
            let parameters: [String: Any] = [apiKeySize: self.pageSize,
                                                apiKeyOffset: self.offsetNotifications]
            Alamofire.request(mainURL + users + me + notifications, method: .get, parameters: parameters, encoding: URLEncoding.default, headers: getAuthHeader()).validate().responseJSON { response in
                                switch response.result {
                                case .success(let json):
                                    let result = JSON(json)
                                    let notifications = NotificationsData.parse(json: result)
                                    
                                    self.stopNotifications = notifications.count < self.pageSize
                                    self.offsetNotifications += notifications.count
                                    let lastDisplayed = self.dataNotifications.count
                                    
                                    self.dataNotifications.append(contentsOf: notifications)
                                    
                                    completion(true, lastDisplayed, notifications.count, "")
                                    break
                                    
                                case .failure(_):
                                    let errorDescription: String = errorFromResponse(response: response)
                                    completion(false, 0, 0, errorDescription)
                                    break
                                }
            }
        } else {
            completion(false, 0, 0, Localize(string: "noAccessToken"))
        }
    }
}
