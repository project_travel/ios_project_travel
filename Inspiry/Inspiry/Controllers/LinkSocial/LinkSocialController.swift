//
//  LinkSocialController.swift
//  Inspiry
//
//  Created by Alexander Danilyak on 15/07/16.
//  Copyright © 2016 Inspiry. All rights reserved.
//

import Foundation
import UIKit

class LinkSocialController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var startButton: UIButton!
    @IBOutlet weak var privacyButton: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.navigationItem.title = Localize(string: "linkSocialTitle")
        self.navigationItem.hidesBackButton = true
        
        NotificationCenter.default.addObserver(self, selector: #selector(reload), name: reloadNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(loadNames), name: loadNamesNotification, object: nil)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.startButton.setTitle(Localize(string: "linkSocialStart"), for: .normal)
        self.startButton.layer.cornerRadius = 13
        self.startButton.layer.borderWidth = 2
        self.startButton.layer.borderColor = UIHelpers.green().cgColor
        
        self.privacyButton.setTitle(Localize(string: "linkSocialPrivacy"), for: .normal)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        Analytics.shared.logScreen(name: screenLinkSocial)
    }
    
    // MARK: Table View
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // Facebook hiden
        return StoredLinkedSocialNetworks.shared.socialNetworks.count - 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "linkSocialCell", for: indexPath) as! LinkSocialCell
        cell.setup(for: StoredLinkedSocialNetworks.shared.socialNetworks[indexPath.row])
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.tableView.deselectRow(at: indexPath, animated: true)
        
        UIHelpers.shared.showLoadingAlert(text: Localize(string: "wait"))
        
        LinkManager().linkSocialNetwork(socialNetworkName: StoredLinkedSocialNetworks.shared.socialNetworks[indexPath.row].name.rawValue, completion: { isSuccess, errorDescription in
                UIHelpers.shared.closeAlert()
                self.view.isUserInteractionEnabled = true
        })
    }
    
    func tableView(_ tableView: UITableView, shouldHighlightRowAt indexPath: IndexPath) -> Bool {
        return !StoredLinkedSocialNetworks.shared.socialNetworks[indexPath.row].isLinked
    }
    
    func tableView(_ tableView: UITableView, didHighlightRowAt indexPath: IndexPath) {
        let cell: LinkSocialCell = tableView.cellForRow(at: indexPath) as! LinkSocialCell
        cell.backgroundColor = UIColor.white
        cell.setBorderColor(color: UIHelpers.green())
    }
    
    func tableView(_ tableView: UITableView, didUnhighlightRowAt indexPath: IndexPath) {
        let cell: LinkSocialCell = tableView.cellForRow(at: indexPath) as! LinkSocialCell
        cell.backgroundColor = UIColor.white
        cell.setBorderColor(color: UIHelpers.gray())
    }
    
    
    // MARK: On next
    
    @IBAction func onNext(_ sender: AnyObject) {
        let mainStoryboard: UIStoryboard? = UIStoryboard(name: "Main", bundle: nil)
        let mainController = mainStoryboard?.instantiateInitialViewController() as! UINavigationController
        
        (mainController.viewControllers[0] as! ProfileController).isMyProfile = true
        (mainController.viewControllers[0] as! ProfileController).isMenuButtonNeeded = true
        
        MenuController.currentMenuIndex = 1
        
        UIApplication.shared.delegate?.window??.rootViewController = mainController
        UIApplication.shared.delegate?.window??.makeKeyAndVisible()

    }
    
    // MARK: Privacy
    
    @IBAction func onPrivacy(_ sender: AnyObject) {
        let settingsStoryboard: UIStoryboard? = UIStoryboard(name: "Settings", bundle: nil)
        let webContentController: SettingsWebContentController! = settingsStoryboard?.instantiateViewController(withIdentifier: "settingsWebContent") as! SettingsWebContentController
        
        webContentController.type = .privacy
        
        webContentController.requestToLoad = URLRequest.init(url: URL.init(string: privacyLink)!)
        webContentController.navBarTitle = Localize(string: "settingsPrivacy")
        
        self.navigationController?.pushViewController(webContentController, animated: true)
    }
    
    // MARK: Reload
    
    func reload() {
        self.tableView.reloadData()
    }
    
    func loadNames() {
        if(StoredUser.shared.user == nil) { return }
        
        LinkManager().getSocialNetworks(user: StoredUser.shared.user, completion: LinkSocialController.updateNamesCompletion)
    }
    
    // MARK: Completion
    
    static func updateNamesCompletion(isSuccess: Bool, newSocialNetworks: [SocialNetwork]?, errorDescription: String?) -> Void {
        if(isSuccess) {
            StoredLinkedSocialNetworks.set(socialNetworks: newSocialNetworks!)
            NotificationCenter.default.post(name: reloadNotification, object: nil)
        } else {
            UIHelpers.shared.showErrorAlert(text: errorDescription!, isAutoClosable: true)
        }
    }
}
