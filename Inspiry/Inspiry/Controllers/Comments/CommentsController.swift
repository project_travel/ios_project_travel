//
//  CommentsController.swift
//  Inspiry
//
//  Created by Alexander Danilyak on 05/08/16.
//  Copyright © 2016 Inspiry. All rights reserved.
//

import Foundation
import UIKit
import MGSwipeTableCell

class CommentsController: UIViewController, UITableViewDelegate, UITableViewDataSource, UITextViewDelegate {
    
    var keyboardShown: Bool = false
    var currentKeyBoardHeight: CGFloat = 0
    var differentKeyboardsGap: CGFloat = 0
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var closeButton: UIBarButtonItem!
    
    @IBOutlet weak var commentsToBottom: NSLayoutConstraint!
    @IBOutlet weak var commentsContainer: UIView!
    @IBOutlet weak var commentTextView: UITextView!
    @IBOutlet weak var sendCommentButton: UIButton!
    
    var tripId: String?
    var model: CommentsModel?
    
    var editMode: Bool = false
    var editId: String?
    var editedText: String?
    
    var appeared: Bool = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupKeyboard()
        modelSetup()
        setupTable()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationItem.title = Localize(string: "commentsTitle")
        self.closeButton.title = Localize(string: "commentsClose")
        
        setupCommentTextView()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        self.appeared = true
        
        Analytics.shared.logScreen(name: screenComments)
    }
    
    // MARK: Model
    
    func modelSetup() {
        model = CommentsModel.init(tripId: tripId, pageSize: 30)
        model!.loadNextPage(completion: pageLoaded)
    }
    
    // MARK: Model Completions
    
    func pageLoaded(isSuccess: Bool, lastDisplayed: Int, newCount: Int, errorDescription: String) -> Void {
        
        if(isSuccess) {
            if newCount == 0 { return }
            
            var indexes = [IndexPath]()
            for i in lastDisplayed...(lastDisplayed+newCount-1) {
                indexes.append(IndexPath(row: i, section: 0))
            }
            
            self.tableView.insertRows(at: indexes, with: .none)
            self.tableView.scrollToRow(at: indexes.last!, at: .bottom, animated: true)
        } else {
            UIHelpers.shared.showErrorAlert(text: errorDescription, isAutoClosable: true)
        }
        
        //self.commentTextView.isEnabled = true
        self.sendCommentButton.isEnabled = false
    }
    
    func commentEditCompletion(isSuccess: Bool, index: Int, errorDescription: String) -> Void {
        if(isSuccess) {
            
            let indexPath: IndexPath = IndexPath.init(row: index, section: 0)
            
            self.tableView.reloadRows(at: [indexPath], with: .none)
            
            self.tableView.scrollToRow(at: indexPath, at: .bottom, animated: true)
            
        } else {
            UIHelpers.shared.showErrorAlert(text: errorDescription, isAutoClosable: true)
        }
        
        self.editId = nil
        self.editMode = false
        self.editedText = nil
        //self.textField.isEnabled = true
        self.sendCommentButton.isEnabled = !(self.commentTextView.text?.isEmpty)!
    }
    
    func commentDeleteCompletion(isSuccess: Bool, index: Int, errorDescription: String) -> Void {
        
        if(isSuccess) {
            
            let indexPath: IndexPath = IndexPath.init(row: index, section: 0)
            self.tableView.deleteRows(at: [indexPath], with: .fade)
            self.tableView.reloadData()
            
        } else {
            UIHelpers.shared.showErrorAlert(text: errorDescription, isAutoClosable: true)
        }
        
        //self.textField.isEnabled = true
        self.sendCommentButton.isEnabled = !(self.commentTextView.text?.isEmpty)!
    }
    
    // MARK: Table View
    
    func setupTable() {
        tableView.estimatedRowHeight = 70.0
        tableView.rowHeight = UITableViewAutomaticDimension
        
        // Trick to hide separator lines of not used cells
        self.tableView.tableFooterView = UIView()
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return model == nil ? 0 : model!.allComments.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "commentCell", for: indexPath) as! CommentCell
            
        cell.comment = model!.allComments[indexPath.row]
        cell.setAll()
        
        if(cell.comment?.owner == StoredUser.shared.user?.username) {
            cell.rightButtons = [createEditButton(index: indexPath),
                             createDeleteButton(index: indexPath)]
            cell.rightSwipeSettings.transition = .static
        }
        
        return cell
    }
    
    func createEditButton(index: IndexPath) -> MGSwipeButton {
        return MGSwipeButton.init(title: nil, icon: #imageLiteral(resourceName: "editComment"), backgroundColor: UIHelpers.gray(), padding: 40, callback: {
            MGSwipeTableCell -> Bool in
            
            self.editMode = true
            self.editId = self.model!.allComments[index.row].id
            self.commentTextView.text = self.model!.allComments[index.row].text
            self.sendCommentButton.isEnabled = true
            self.commentTextView.becomeFirstResponder()
            
            return true
        })
    }
    
    func createDeleteButton(index: IndexPath) -> MGSwipeButton {
        return MGSwipeButton.init(title: nil, icon: #imageLiteral(resourceName: "deleteComment"), backgroundColor: UIHelpers.destructiveColor(), callback: {
            MGSwipeTableCell -> Bool in
            
            self.sendCommentButton.isEnabled = false
            let deleteId = self.model!.allComments[index.row].id
            self.model!.deleteComment(id: deleteId, completion: self.commentDeleteCompletion)
            self.commentTextView.resignFirstResponder()
            return true
        })
    }
    
    // MARK: Keyboard
    
    func setupKeyboard() {
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardChangeFrame), name:NSNotification.Name.UIKeyboardWillChangeFrame, object: nil)
    }
    
    func keyboardChangeFrame(notification: Notification) {
        let keyboardFrameEnd = (notification.userInfo?[UIKeyboardFrameEndUserInfoKey] as! NSValue).cgRectValue
        
        let hide: Bool = keyboardFrameEnd.origin.y == UIScreen.main.bounds.size.height
        
        let keyboardHeight = keyboardFrameEnd.size.height
        if(self.appeared != true) {
            self.currentKeyBoardHeight = keyboardHeight
        }
        
        let keyboardAnimationDuration = notification.userInfo?[UIKeyboardAnimationDurationUserInfoKey] as! CGFloat
        
        if(keyboardHeight != self.currentKeyBoardHeight) {
            self.differentKeyboardsGap = keyboardHeight - self.currentKeyBoardHeight
            self.commentsToBottom.constant += self.differentKeyboardsGap
            self.currentKeyBoardHeight = keyboardHeight
        }
        
        let const: CGFloat = hide == true ? -keyboardHeight : keyboardHeight
        if(self.keyboardShown == hide) {
            self.commentsToBottom.constant += const
            self.keyboardShown = !hide
        }
        
        UIView.animate(withDuration: TimeInterval(keyboardAnimationDuration)) {
            self.view.layoutIfNeeded()
        }
        
        if(self.model != nil && self.appeared == true && self.keyboardShown == true) {
            if self.model!.allComments.count == 0 { return }
            self.tableView.scrollToRow(at: IndexPath.init(row: self.model!.allComments.count - 1, section: 0), at: .bottom, animated: false)
        }
    }
    
    // MARK: Text View
    
    func setupCommentTextView() {
        self.sendCommentButton.isEnabled = false
        self.commentTextView.becomeFirstResponder()
        self.commentTextView.autocorrectionType = .no
        self.commentTextView.delegate = self
        
        self.commentTextView.layer.cornerRadius = 13.0
        self.commentTextView.layer.masksToBounds = true
        self.commentTextView.textContainerInset = UIEdgeInsetsMake(7.0, 7.0, 7.0, 7.0)
    }
    
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        self.sendCommentButton.isEnabled = !(self.commentTextView.text?.isEmpty)!
        if(!text.isEmpty) {
            if self.model!.allComments.count == 0 { return true }
            self.tableView.scrollToRow(at: IndexPath.init(row: self.model!.allComments.count - 1, section: 0), at: .bottom, animated: false)
        }
        return true
    }
    
    func textViewDidChange(_ textView: UITextView) {
        self.sendCommentButton.isEnabled = !(self.commentTextView.text?.isEmpty)!
    }
    
    // MARK: Send
    
    @IBAction func onSend(_ sender: AnyObject) {
        //self.textField.isEnabled = false
        self.sendCommentButton.isEnabled = false
        let text = self.commentTextView.text
        self.commentTextView.text = ""
        self.commentTextView.resignFirstResponder()
        if(self.editMode == true) {
            editedText = text
            model!.editComment(id: editId!, text: text!, completion: commentEditCompletion)
        } else {
            model!.addComment(text: text!, completion: pageLoaded)
        }
        if self.model!.allComments.count == 0 { return }
        self.tableView.scrollToRow(at: IndexPath.init(row: self.model!.allComments.count - 1, section: 0), at: .bottom, animated: true)
    }
    
    // MARK: Close
    
    @IBAction func onClose(_ sender: AnyObject) {
        self.commentTextView.resignFirstResponder()
        self.dismiss(animated: true, completion: nil)
    }
    
}
