//
//  ChooseCountryCityController.swift
//  Inspiry
//
//  Created by Alexander Danilyak on 18/09/16.
//  Copyright © 2016 Inspiry. All rights reserved.
//

import Foundation
import UIKit
import SwiftyJSON

enum ChooseType {
    case city
    case country
}

struct Country {
    init(json: JSON) {
        self.translation = json["value"].stringValue
        self.countryCode = json["id"].stringValue
    }
    
    var translation: String
    var countryCode: String
}

struct City {
    init(json: JSON) {
        self.translation = json["value"].stringValue
        self.id = json["id"].stringValue
        self.countryCode = json["countryId"].stringValue
    }
    
    var translation: String
    var id: String
    var countryCode: String
}

class ChooseCountryCityController: UIViewController, UITableViewDelegate, UITableViewDataSource, UISearchResultsUpdating, UISearchControllerDelegate {
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var closeButton: UIBarButtonItem!
    
    var searchQuery: SearchQueryData?
    var type: ChooseType = .country
    
    var dataCities: [City]! = []
    var dataCountries: [Country]! = []
    
    var searchData: [Any]! = []
    
    let searchController = UISearchController(searchResultsController: nil)
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.searchSetup()
        self.loadWithType(type: self.type)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.closeButton.title = Localize(string: "tripCancel")
        self.navigationItem.title = self.type == .country ? Localize(string: "chooseCountry") : Localize(string: "chooseCity")
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        let name = self.type == .city ? screenSearchChooseCity : screenSearchChooseCountry
        Analytics.shared.logScreen(name: name)
        
        self.searchController.isActive = true
    }
    
    func searchSetup() {
        searchController.delegate = self
        searchController.searchResultsUpdater = self
        searchController.dimsBackgroundDuringPresentation = false
        searchController.searchBar.searchBarStyle = .minimal
        searchController.searchBar.backgroundColor = UIColor.white
        searchController.searchBar.tintColor = UIHelpers.green()
        searchController.definesPresentationContext = true
        tableView.tableHeaderView = searchController.searchBar
    }
    
    func loadWithType(type: ChooseType) {
        let lang = Locale.current.languageCode
        if(lang == "ru") {
            let pathCities = Bundle.main.path(forResource: "ru.cities", ofType: "json")
            let pathCountries = Bundle.main.path(forResource: "ru.countries", ofType: "json")
            self.loadRoutine(type: .city, path: pathCities!)
            self.loadRoutine(type: .country, path: pathCountries!)
        } else {
            // Load english in other cases
            let pathCities = Bundle.main.path(forResource: "en.cities", ofType: "json")
            let pathCountries = Bundle.main.path(forResource: "en.countries", ofType: "json")
            self.loadRoutine(type: .city, path: pathCities!)
            self.loadRoutine(type: .country, path: pathCountries!)
        }
    }
    
    private func loadRoutine(type: ChooseType, path: String) {
        var string: String?
        do {
            let url = URL(fileURLWithPath: path)
            string = try String(contentsOf: url)
            
        } catch {
            print("[CHOOSE CONTROLLER] exception while reading file")
            return
        }
        let json = JSON.parse(string!)
        if(type == .city) {
            for item in json.arrayValue {
                self.dataCities.append(City.init(json: item))
            }
        } else {
            for item in json.arrayValue {
                self.dataCountries.append(Country.init(json: item))
            }
        }
    }
    
    // MARK: Table View
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        let count = self.type == .city ? self.dataCities.count : self.dataCountries.count
        return self.isSearchActive() ? self.searchData.count : count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell: ChooseCountryCityCell = tableView.dequeueReusableCell(withIdentifier: "chooseCell", for: indexPath) as! ChooseCountryCityCell
        var name: String
        var isChosen: Bool
        
        (name, isChosen) = self.titleFor(indexPath: indexPath)
        
        cell.update(with: name, isChosen: isChosen)
        return cell
    }
    
    func titleFor(indexPath: IndexPath) -> (String, Bool) {
        var text: String
        var isChosen: Bool
        
        var item: Any
        if(self.isSearchActive()) {
            item = self.searchData[indexPath.row]
        } else {
            item = self.type == .city ? self.dataCities[indexPath.row] : self.dataCountries[indexPath.row]
        }
        
        if(self.type == .city) {
            text = (item as! City).translation + ", " + (item as! City).countryCode
            let alreadyChosen = self.searchQuery!.city
            isChosen = alreadyChosen == nil ? false : alreadyChosen! == (item as! City).translation
        } else {
            text = (item as! Country).translation
            let alreadyChosen = self.searchQuery!.country
            isChosen = alreadyChosen == nil ? false : alreadyChosen! == (item as! Country).translation
        }
        
        return (text, isChosen)
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        if(self.type == .city) {
            self.searchQuery!.city = self.isSearchActive() ? (self.searchData[indexPath.row] as! City).translation : self.dataCities[indexPath.row].translation
        } else {
            self.searchQuery!.country = self.isSearchActive() ? (self.searchData[indexPath.row] as! Country).translation : self.dataCountries[indexPath.row].translation
        }
        
        if isSearchActive() { self.onClose(self) }
        
        self.onClose(self)
    }
    
    // MARK: Search Updating
    
    func updateSearchResults(for searchController: UISearchController) {
        self.filter(query: searchController.searchBar.text!)
        self.tableView.reloadData()
    }
    
    // MARK: Search
    
    func isSearchActive() -> Bool {
        return searchController.isActive && searchController.searchBar.text != ""
    }
    
    func filter(query: String) {
        if(self.type == .city) {
            self.searchData = self.dataCities.filter { item in
                return item.translation.lowercased().contains(query.lowercased())
            }
        } else {
            self.searchData = self.dataCountries.filter { item in
                return (item.translation.lowercased().contains(query.lowercased()) ||
                        item.countryCode.lowercased().contains(query.lowercased()))
            }
        }
    }
    
    // MARK: Search Controller Delegate
    
    func willDismissSearchController(_ searchController: UISearchController) {
        self.tableView.reloadData()
    }
    
    func didPresentSearchController(_ searchController: UISearchController) {
        searchController.searchBar.becomeFirstResponder()
    }
    
    // MARK: On Close
    
    @IBAction func onClose(_ sender: AnyObject) {
        dismiss(animated: true, completion: nil)
    }
    
    // MARK: Status Bar
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .default
    }
    
}
