//
//  GlobalSearchController.swift
//  Inspiry
//
//  Created by Alexander Danilyak on 29/07/16.
//  Copyright © 2016 Inspiry. All rights reserved.
//

import Foundation
import UIKit
import SideMenu

enum SearchDisplayType {
    case search
    case recommendations
}

class GlobalSearchController: UIViewController, UITableViewDelegate, UITableViewDataSource, SearchCellProtocol {
    
    var isMenuButtonNeeded: Bool = true
    var menuButton: UIBarButtonItem? = nil
    
    var type: SearchType = .trips
    var queryData: SearchQueryData = SearchQueryData()
    
    var displayType: SearchDisplayType = .recommendations
    
    var model: GlobalSearchModel?
    var recommendationsModel: RecommendationsModel?
    
    let searchController = UISearchController(searchResultsController: nil)
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var noContentLabel: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.model = GlobalSearchModel.init(pageSize: 25, searchData: self.queryData)
        self.recommendationsModel = RecommendationsModel.init(pageSize: 25)
        
        self.noContentLabel.text = self.displayType == .recommendations ? Localize(string: "noContent") : Localize(string: "nothingWasFound")
        self.recommendationsModel!.loadNextPage(completion: pageLoaded)
        
        self.tableView.estimatedRowHeight = 88.0
        self.tableView.rowHeight = UITableViewAutomaticDimension
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        let queryCell: SearchQueryCell? = tableView.cellForRow(at: IndexPath(row: 0, section: 0)) as? SearchQueryCell
        if(queryCell != nil) {
            queryCell!.setup()
        }
        self.menuButtonInit()
        self.navigationItem.title = Localize(string: "searchTitle")
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        Analytics.shared.logScreen(name: screenSearch)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
    }
    
    deinit {
        searchController.view.removeFromSuperview()
    }
    
    func pageLoaded(isSuccess: Bool, lastDisplayed: Int, newCount: Int, errorDescription: String) -> Void {
        self.noContentLabel.isHidden = !(lastDisplayed == 0 && newCount == 0)
        self.noContentLabel.text = self.displayType == .recommendations ? Localize(string: "noContent") : Localize(string: "nothingWasFound")
        
        if(isSuccess) {
            if newCount == 0 { return }
            
            var indexes = [IndexPath]()
            for i in lastDisplayed...(lastDisplayed+newCount-1) {
                indexes.append(IndexPath(row: i, section: 1))
            }
            
            var insertAnimation: UITableViewRowAnimation = .bottom
            if (lastDisplayed == 0) { insertAnimation = .none }
            
            if(lastDisplayed == 0) {
                self.tableView.reloadSections(IndexSet.init(integer: 1), with: .automatic)
            } else {
                self.tableView.insertRows(at: indexes, with: insertAnimation)
            }
        } else {
            UIHelpers.shared.showErrorAlert(text: errorDescription, isAutoClosable: true)
        }
    }
    
    // MARK: Table View
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if(section == 0) { return 1 }
        return self.displayType == .recommendations ? self.recommendationsModel!.recommended.count : self.model!.searchResults.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if(indexPath.section == 0) {
            let cell: SearchQueryCell = tableView.dequeueReusableCell(withIdentifier: "searchQueryCell", for: indexPath) as! SearchQueryCell
            cell.delegate = self
            cell.searchQueryData = self.queryData
            cell.setup()
            return cell
        } else {
            if(self.displayType == .recommendations) {
                return recommendationCell(indexPath: indexPath)
            } else {
                return searchResultCell(indexPath: indexPath)
            }
        }
    }
    
    func searchResultCell(indexPath: IndexPath) -> UITableViewCell {
        switch self.type {
        case .trips:
            let cell: SearchTripCell = tableView.dequeueReusableCell(withIdentifier: "searchTripCell", for: indexPath) as! SearchTripCell
            let trip = self.model!.searchResults[indexPath.row] as! Trip
            cell.setAll(with: trip)
            return cell
        case .users:
            let cell: FFCell = tableView.dequeueReusableCell(withIdentifier: "searchUserCell", for: indexPath) as! FFCell
            let user = self.model!.searchResults[indexPath.row] as! User
            cell.update(with: user)
            return cell
        }
    }
    
    func recommendationCell(indexPath: IndexPath) -> UITableViewCell {
        switch recommendationsModel!.type {
        case .trips:
            let cell: SearchTripCell = tableView.dequeueReusableCell(withIdentifier: "searchTripCell", for: indexPath) as! SearchTripCell
            let trip = self.recommendationsModel!.recommended[indexPath.row] as! Trip
            cell.setAll(with: trip)
            return cell
        case .users:
            let cell: FFCell = tableView.dequeueReusableCell(withIdentifier: "searchUserCell", for: indexPath) as! FFCell
            let user = self.recommendationsModel!.recommended[indexPath.row] as! User
            cell.update(with: user)
            return cell
        }
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        
        let stop = self.displayType == .recommendations ? self.recommendationsModel!.stop : self.model!.stop
        let lastRow = self.displayType == .recommendations ? (self.recommendationsModel!.recommended.count - 1) : (self.model!.searchResults.count - 1)
        
        if(indexPath.row == lastRow && stop != true) {
            if(self.displayType == .recommendations) {
                self.recommendationsModel!.loadNextPage(completion: pageLoaded)
            } else {
                self.model!.loadNextPage(completion: pageLoaded)
            }
        }
    }
    
    func tableView(_ tableView: UITableView, shouldHighlightRowAt indexPath: IndexPath) -> Bool {
        return indexPath.section != 0
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        if(self.displayType == .recommendations) {
            switch recommendationsModel!.type {
            case .trips:
                let trip = self.recommendationsModel!.recommended[indexPath.row] as! Trip
                self.showTrip(trip: trip)
                break
            case .users:
                let user = self.recommendationsModel!.recommended[indexPath.row] as! User
                self.showProfile(user: user)
                break
            }
        } else {
            switch self.type {
            case .trips:
                let trip = self.model!.searchResults[indexPath.row] as! Trip
                self.showTrip(trip: trip)
                break
            case .users:
                let user = self.model!.searchResults[indexPath.row] as! User
                self.showProfile(user: user)
                break
            }
        }
    }
    
    func showProfile(user: User) {
        let profileController = UIHelpers.getProfileController(with: user)
        if profileController == nil { return }
        self.navigationController!.pushViewController(profileController!, animated: true)
    }
    
    func showTrip(trip: Trip) {
        self.view.isUserInteractionEnabled = false
        
        let tripStoryboard: UIStoryboard? = UIStoryboard(name: "Trip", bundle: nil)
        let postsController: PostsController! = tripStoryboard?.instantiateInitialViewController() as! PostsController
        
        postsController.tripId = trip.id
        postsController.trip = trip
        self.navigationController?.pushViewController(postsController, animated: true)
        self.view.isUserInteractionEnabled = true
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        let title = self.displayType == .recommendations ? Localize(string: "sectionRecommendations") : Localize(string: "sectionSearchResults")
        return section == 0 ? "" : title
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        let size: CGFloat = UIHelpers.isPhone4ScreenSize() || UIHelpers.isPhone5ScreenSize() ? 30.0 : 50.0
        return section == 0 ? 0.0 : size
    }
    
    func tableView(_ tableView: UITableView, willDisplayHeaderView view: UIView, forSection section: Int) {
        (view as! UITableViewHeaderFooterView).backgroundView?.backgroundColor = UIColor.white
        (view as! UITableViewHeaderFooterView).textLabel?.textColor = UIHelpers.darkGray()
        (view as! UITableViewHeaderFooterView).textLabel?.font = UIHelpers.searchQueryFont()
    }
    
    // MARK: Menu
    func menuButtonInit() {
        if isMenuButtonNeeded {
            self.menuButton = UIBarButtonItem.init(image: UIImage.init(named: "menu"), style: .plain, target: self, action:#selector(openMenu))
            self.navigationItem.leftBarButtonItem = menuButton
        }
    }
    
    func openMenu() {
        self.present(SideMenuManager.menuLeftNavigationController!, animated: true, completion: nil)
    }
    
    // MARK: Search Cell Protocol
    
    func tableViewUpdates(isBegin: Bool) {
        isBegin == true ? self.tableView.beginUpdates() : self.tableView.endUpdates()
    }
    
    func searchBarEditining(isStart: Bool) {
        self.navigationController?.setNavigationBarHidden(isStart, animated: true)
    }
    
    func onChooseCityCountry(type: ChooseType) {
        let needToOpenChooseController = type == .country ? self.queryData.country == nil : self.queryData.city == nil
        if(needToOpenChooseController) {
            self.openChooseController(type: type)
        } else {
            let alertController = UIAlertController.init(title: Localize(string: "chooseActionTitle"), message: nil, preferredStyle: .actionSheet)
            
            let change = UIAlertAction.init(title: Localize(string: "chooseChange"), style: .default, handler: { action in
                self.openChooseController(type: type)
            })
            
            let clear = UIAlertAction.init(title: Localize(string: "chooseClear"), style: .destructive, handler: { action in
                type == .country ? (self.queryData.country = nil) : (self.queryData.city = nil)
                self.tableView.reloadSections(IndexSet.init(integer: 0), with: .automatic)
            })
            
            let cancel = UIAlertAction.init(title: Localize(string: "chooseCancel"), style: .cancel, handler: nil)
            
            alertController.addAction(change)
            alertController.addAction(clear)
            alertController.addAction(cancel)
            
            self.present(alertController, animated: true, completion: nil)
        }
    }
    
    func openChooseController(type: ChooseType) {
        let mainStoryboard: UIStoryboard? = UIStoryboard(name: "Main", bundle: nil)
        let navChooseController = mainStoryboard?.instantiateViewController(withIdentifier: "chooseCountryCity") as! UINavigationController
        (navChooseController.viewControllers[0] as! ChooseCountryCityController).type = type
        (navChooseController.viewControllers[0] as! ChooseCountryCityController).searchQuery = self.queryData
        self.present(navChooseController, animated: true, completion: nil)
    }
    
    func onSearch() {
        self.model?.reset()
        self.displayType = .search
        self.tableView.reloadSections(IndexSet.init(integer: 1), with: .automatic)
        self.model?.loadNextPage(completion: pageLoaded)
    }
    
    func onTypeChanged(newType: SearchType) {
        self.type = newType
        self.recommendationsModel?.type = RecommendstionType(rawValue: newType.rawValue)!
        self.queryData.type = newType
        self.model?.reset()
        self.recommendationsModel?.reset()
        self.displayType = .recommendations
        self.tableView.reloadSections(IndexSet.init(integer: 1), with: .automatic)
        self.model?.loadNextPage(completion: pageLoaded)
        self.recommendationsModel?.loadNextPage(completion: pageLoaded)
    }
    
    //MARK: More Button
    
    func addToBlackListCompletion(isSuccess: Bool, errorDescription: String) -> Void {
        if isSuccess {
            model!.reset()
            tableView.reloadData()
            
            let alert = UIAlertController(title: Localize(string: "blackListCompletionTitle"),
                                          message: Localize(string: "blackListCompletionMessage"),
                                          preferredStyle: .actionSheet)
            let cancel = UIAlertAction(title: Localize(string: "ok"), style: .cancel, handler: nil)
            alert.addAction(cancel)
            Router.getTopNavigationController().present(alert, animated: true, completion: nil)
        } else {
            UIHelpers.shared.showErrorAlert(text: errorDescription, isAutoClosable: true)
        }
    }
}
