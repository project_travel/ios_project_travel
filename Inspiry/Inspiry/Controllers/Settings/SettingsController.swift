//
//  SettingsController.swift
//  Inspiry
//
//  Created by Alexander Danilyak on 03/08/16.
//  Copyright © 2016 Inspiry. All rights reserved.
//

import Foundation
import UIKit
import SideMenu
import MessageUI

enum SettingsCellType: Int {
    case blackList = 0
    case password = 1
    case email = 2
    case terms = 3
    case privacy = 4
    case support = 5
    case logOut = 6
    case delete = 7
}

class SettingsController: UITableViewController, MFMailComposeViewControllerDelegate {
    
    let toHighlight: [SettingsCellType] = [.blackList, .password, .email, .terms, .privacy, .support, .logOut, .delete]
    
    @IBOutlet weak var blackListLabel: UILabel!
    @IBOutlet weak var changePasswordLabel: UILabel!
    @IBOutlet weak var changeEmailLabel: UILabel!
    @IBOutlet weak var showTermsLabel: UILabel!
    @IBOutlet weak var showPrivacyLabel: UILabel!
    @IBOutlet weak var supportLabel: UILabel!
    @IBOutlet weak var logOutLabel: UILabel!
    @IBOutlet weak var deleteLabel: UILabel!
    
    
    // MARK: View Controller
    
    override func viewDidLoad() {
        super.viewDidLoad()
        menuButtonInit()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationItem.title = Localize(string: "settingsTitle")
        
        self.blackListLabel.text = Localize(string: "settingsBlackList")
        self.changePasswordLabel.text = Localize(string: "settingsChangePassword")
        self.changeEmailLabel.text = Localize(string: "settingsChangeEmail")
        self.showTermsLabel.text = Localize(string: "settingsTerms")
        self.showPrivacyLabel.text = Localize(string: "settingsPrivacy")
        self.supportLabel.text = Localize(string: "settingsSupport")
        self.logOutLabel.text = Localize(string: "settingsLogOut")
        self.deleteLabel.text = Localize(string: "settingsDelete")
        
        // Trick to hide separator lines of not used cells
        self.tableView.tableFooterView = UIView()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        Analytics.shared.logScreen(name: screenSettings)
    }
    
    // MARK: Actions
    
    @IBAction func showBirthDateChanged(_ sender: AnyObject) {
        // to do
    }
    
    @IBAction func showStatisticsChanged(_ sender: AnyObject) {
        // to do
    }
    
    // MARK: Table View
    
    override func tableView(_ tableView: UITableView, shouldHighlightRowAt indexPath: IndexPath) -> Bool {
        return toHighlight.contains(SettingsCellType(rawValue: indexPath.row)!)
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        
        let cellType: SettingsCellType = SettingsCellType(rawValue: indexPath.row)!
        
        switch cellType {
        case .logOut:
            self.onLogOut()
            break
        case .delete:
            self.onDelete()
            break
        case .support:
            self.onSupport()
            break
        default:
            // See segues in storyboard
            break
        }
    }
    
    // MARK: Navigation
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "terms" {
            (segue.destination as! SettingsWebContentController).requestToLoad = URLRequest.init(url: URL.init(string: termsLink)!)
            (segue.destination as! SettingsWebContentController).navBarTitle = Localize(string: "settingsTerms")
            (segue.destination as! SettingsWebContentController).type = .terms
            return
        }
        
        if segue.identifier == "privacy" {
            (segue.destination as! SettingsWebContentController).requestToLoad = URLRequest.init(url: URL.init(string: privacyLink)!)
            (segue.destination as! SettingsWebContentController).navBarTitle = Localize(string: "settingsPrivacy")
            (segue.destination as! SettingsWebContentController).type = .privacy
            return
        }
    }
    
    // MARK: Menu
    
    func menuButtonInit() {
        let menuButton = UIBarButtonItem.init(image: UIImage.init(named: "menu"), style: .plain, target: self, action: #selector(openMenu))
        self.navigationItem.leftBarButtonItem = menuButton
    }
    
    func openMenu() {
        self.present(SideMenuManager.menuLeftNavigationController!, animated: true, completion: nil)
    }
    
    // MARK: On Log Out
    
    func onLogOut() {
        UserDefaults().clearBothTokens()
        UserAuth.shared.state = .noUser
        UserDefaults.standard.deleteLinksToSocialNetworks()
        
        let loginStoryboard: UIStoryboard? = UIStoryboard(name: "Login", bundle: nil)
        UIApplication.shared.delegate?.window??.rootViewController = loginStoryboard?.instantiateInitialViewController()
        UIApplication.shared.delegate?.window??.makeKeyAndVisible()
    }
    
    // MARK: On Delete
    
    func onDelete() {
        let alert = UIAlertController.init(title: Localize(string: "deleteAreYouSureTitle"), message: Localize(string: "deleteAreYouSureMessage"), preferredStyle: .alert)
        let yes = UIAlertAction.init(title: Localize(string: "deleteYes"), style: .destructive) { action in
            self.onDeleteRoutine()
        }
        
        let cancel = UIAlertAction.init(title: Localize(string: "deleteCancel"), style: .cancel, handler: nil)
        
        alert.addAction(yes)
        alert.addAction(cancel)
        
        self.present(alert, animated: true, completion: nil)
    }
    
    func onDeleteRoutine() {
        UIHelpers.shared.showLoadingAlert(text: Localize(string: "delete"))
        
        StoredUserManager().deleteUser(completion: deleteCompletion)
    }
    
    func deleteCompletion(isSuccess: Bool, message: String?) -> Void {
        if isSuccess {
            let loginStoryboard: UIStoryboard? = UIStoryboard(name: "Login", bundle: nil)
            UIApplication.shared.delegate?.window??.rootViewController = loginStoryboard?.instantiateInitialViewController()
            UIApplication.shared.delegate?.window??.makeKeyAndVisible()
        } else {
            UIHelpers.shared.showErrorAlert(text: message, isAutoClosable: true)
        }
        self.view.isUserInteractionEnabled = true
        UIHelpers.shared.closeAlert()
    }
    
    // MARK: Support
    
    func onSupport() {
        let mailComposer: MFMailComposeViewController = MFMailComposeViewController.init()
        mailComposer.mailComposeDelegate = self
        mailComposer.setSubject("Inspiry Support")
        mailComposer.setToRecipients(["support@inspiry.me"])
        mailComposer.navigationBar.tintColor = UIHelpers.green()
        self.present(mailComposer, animated: true, completion: nil)
    }
    
    // MARK: MFMailComposeViewController
    
    func mailComposeController(_ controller: MFMailComposeViewController, didFinishWith result: MFMailComposeResult, error: Error?) {
        switch result {
        case .sent:
            UIHelpers.shared.showNotificationAlert(text: Localize(string: "alertThankYou"), isAutoClosable: true)
            break
        case .failed:
            UIHelpers.shared.showErrorAlert(text: Localize(string: "alertFailedToSend"), isAutoClosable: true)
            break
        default:
            break
        }
        controller.dismiss(animated: true, completion: nil)
    }
}
