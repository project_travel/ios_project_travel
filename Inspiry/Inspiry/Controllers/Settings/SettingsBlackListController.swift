//
//  SettingsBlackListController.swift
//  Inspiry
//
//  Created by Alexander Danilyak on 22/10/2016.
//  Copyright © 2016 Inspiry. All rights reserved.
//

import UIKit

class SettingsBlackListController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    @IBOutlet weak var tableView: UITableView!
    
    var model: BlackListModel?
    
    var noContentLabel: UILabel = UIHelpers.getLabel(with: Localize(string: "noContent"))
    
    // MARK: View Controller
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setTitle()
        setupModel()
        tableViewSetup()
    }
    
    func tableViewSetup() {
        // Trick to hide separator lines of not used cells
        self.tableView.tableFooterView = UIView()
        
        self.noContentLabel.center = self.view.center
        self.view.addSubview(self.noContentLabel)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    
    func setupModel() {
        self.model = BlackListModel()
        self.loadDataWrap()
    }
    
    // MARK: Title
    
    func setTitle() {
        title = Localize(string: "settingsBlackList")
        self.navigationItem.title = title
    }
    
    // MARK: Data
    
    func loadDataWrap() {
        self.model!.getBlackList(completion: pageLoaded)
    }
    
    func pageLoaded(isSuccess: Bool, lastDisplayed: Int, newCount: Int, errorDescription: String) -> Void {
        self.noContentLabel.isHidden = !(lastDisplayed == 0 && newCount == 0)
        
        if(isSuccess) {
            if newCount == 0 { return }
            
            var indexes = [IndexPath]()
            for i in lastDisplayed...(lastDisplayed+newCount-1) {
                indexes.append(IndexPath(row: i, section: 0))
            }
            
            self.tableView.beginUpdates()
            self.tableView.insertRows(at: indexes, with: .none)
            self.tableView.endUpdates()
        } else {
            UIHelpers.shared.showErrorAlert(text: errorDescription, isAutoClosable: true)
        }
    }
    
    func removedFromBlackList(isSuccess: Bool, index: Int, errorDescription: String) -> Void {
        if(isSuccess) {
            var indexes = [IndexPath]()
            indexes.append(IndexPath(row: index, section: 0))
            
            self.tableView.beginUpdates()
            self.tableView.deleteRows(at: indexes, with: .none)
            self.tableView.endUpdates()
        } else {
            UIHelpers.shared.showErrorAlert(text: errorDescription, isAutoClosable: true)
        }
        
        self.noContentLabel.isHidden = tableView.numberOfRows(inSection: 0) != 0
    }
    
    // MARK: Table View
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.model!.blackList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell: FFCell = self.tableView.dequeueReusableCell(withIdentifier: "ffCell", for: indexPath) as! FFCell
        
        let user = self.model!.blackList[indexPath.row]
        cell.update(with: user)
        
        return cell
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        let alert = UIAlertController(title: Localize(string: "blackListRemoveUserTitle"),
                                      message: Localize(string: "blackListRemoveUserMessage"),
                                      preferredStyle: .actionSheet)
        
        let remove = UIAlertAction(title: Localize(string: "blackListRemove"), style: .destructive) { (action) in
            self.model!.removeFromBlackList(index: indexPath.row, completion: self.removedFromBlackList)
        }
        
        let cancel = UIAlertAction(title: Localize(string: "blackListCancel"), style: .cancel, handler: nil)
        
        alert.addAction(remove)
        alert.addAction(cancel)
        
        self.present(alert, animated: true, completion: nil)
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        var stop: Bool = false
        var lastIndex: Int = 0
        
        stop = self.model!.stop
        lastIndex = self.model!.blackList.count - 1

        if(indexPath.row == lastIndex && stop != true) {
            self.loadDataWrap()
        }
    }
}
