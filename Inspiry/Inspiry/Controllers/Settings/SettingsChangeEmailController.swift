//
//  SettingsChangeEmailController.swift
//  Inspiry
//
//  Created by Alexander Danilyak on 03/08/16.
//  Copyright © 2016 Inspiry. All rights reserved.
//

import Foundation
import UIKit

class SettingsChangeEmailController: UIViewController, UITableViewDelegate, UITableViewDataSource, UITextFieldDelegate {

    var newEmailTextField: UITextField!
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var changeButton: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationItem.title = Localize(string: "settingsChangeEmail")
    
        self.setUpTableView()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.changeButton.setTitle(Localize(string: "changeEmailButtonTitle"), for: .normal)
        
        self.changeButton.layer.cornerRadius = 13.0
        self.changeButton.layer.borderWidth = 2.0
        self.changeButton.layer.masksToBounds = true
        self.changeButton.layer.borderColor = UIHelpers.green().cgColor
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        Analytics.shared.logScreen(name: screenChangeEmail)
    }
    
    func setUpTableView() {
        self.tableView.contentInset = UIEdgeInsetsMake(8, 0, 0, 0)
        
        let tapToHideKeyboard: UITapGestureRecognizer! = UITapGestureRecognizer(target: self, action:#selector(hideKeyboard))
        tapToHideKeyboard.cancelsTouchesInView = false
        self.view.addGestureRecognizer(tapToHideKeyboard)
    }
    
    func hideKeyboard() {
        self.view.endEditing(true)
    }
    
    // MARK: Table View
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell: SettingsChangeCell = tableView.dequeueReusableCell(withIdentifier: "settingsChangeCell", for: indexPath) as! SettingsChangeCell
        cell.changeTextField.delegate = self
        
        switch indexPath.row {
        case 0:
            cell.update(with: .newEmail)
            self.newEmailTextField = cell.changeTextField
            break
        default:
            break
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let cell: SettingsChangeCell = tableView.cellForRow(at: indexPath) as! SettingsChangeCell
        cell.changeTextField.becomeFirstResponder()
    }
    
    func tableView(_ tableView: UITableView, shouldHighlightRowAt indexPath: IndexPath) -> Bool {
        return true
    }
    
    func tableView(_ tableView: UITableView, didHighlightRowAt indexPath: IndexPath) {
        let cell: SettingsChangeCell = tableView.cellForRow(at: indexPath) as! SettingsChangeCell
        cell.backgroundColor = UIColor.white
        cell.setBorderColor(color: UIHelpers.green())
    }
    
    func tableView(_ tableView: UITableView, didUnhighlightRowAt indexPath: IndexPath) {
        let cell: SettingsChangeCell = tableView.cellForRow(at: indexPath) as! SettingsChangeCell
        cell.backgroundColor = UIColor.white
        cell.setBorderColor(color: UIHelpers.gray())
    }
    
    //
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if(textField == newEmailTextField) {
            textField.resignFirstResponder()
            self.onChange(self)
            return true
        }
        
        return true
    }
    
    @IBAction func onChange(_ sender: AnyObject) {
        if(!isStringEmail(isEmailString: newEmailTextField.text)) {
            UIHelpers.shared.showErrorAlert(text: Localize(string: "invalidEmailMessage"), isAutoClosable: true)
            self.view.isUserInteractionEnabled = true
            return
        }
        
        UIHelpers.shared.showLoadingAlert(text: Localize(string: "change"))
        StoredUserManager().changeEmail(new: newEmailTextField.text, completion: completion)
    }
    
    func completion(isSuccess: Bool, errorMessage: String?) -> Void {
        if isSuccess {
            _ = self.navigationController?.popViewController(animated: true)
        } else {
            UIHelpers.shared.showErrorAlert(text: Localize(string: "invalidEmailMessage"), isAutoClosable: true)
        }
        
        self.view.isUserInteractionEnabled = true
        UIHelpers.shared.closeAlert()
    }
}
