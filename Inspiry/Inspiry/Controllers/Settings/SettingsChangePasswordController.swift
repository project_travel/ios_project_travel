//
//  SettingsChangePasswordController.swift
//  Inspiry
//
//  Created by Alexander Danilyak on 03/08/16.
//  Copyright © 2016 Inspiry. All rights reserved.
//

import Foundation
import UIKit

class SettingsChangePasswordController: UIViewController, UITableViewDataSource, UITableViewDelegate, UITextFieldDelegate {
    
    var oldPasswordTextField: UITextField!
    var newPasswordTextField: UITextField!
    var newPasswordConfirmTextField: UITextField!
    
    @IBOutlet weak var changeButton: UIButton!
    @IBOutlet weak var tableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationItem.title = Localize(string: "settingsChangePassword")
        
        self.setUpTableView()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.changeButton.layer.cornerRadius = 13.0
        self.changeButton.layer.borderWidth = 2.0
        self.changeButton.layer.masksToBounds = true
        self.changeButton.layer.borderColor = UIHelpers.green().cgColor
        
        self.changeButton.setTitle(Localize(string: "changePasswordButtonTitle"), for: .normal)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        Analytics.shared.logScreen(name: screenChangePassword)
    }
    
    func setUpTableView() {
        self.tableView.contentInset = UIEdgeInsetsMake(8, 0, 0, 0)
        
        let tapToHideKeyboard: UITapGestureRecognizer! = UITapGestureRecognizer(target: self, action:#selector(hideKeyboard))
        tapToHideKeyboard.cancelsTouchesInView = false
        self.view.addGestureRecognizer(tapToHideKeyboard)
    }
    
    func hideKeyboard() {
        self.view.endEditing(true)
    }
    
    // MARK: Table View 
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 3
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell: SettingsChangeCell = tableView.dequeueReusableCell(withIdentifier: "settingsChangeCell", for: indexPath) as! SettingsChangeCell
        cell.changeTextField.delegate = self
        
        switch indexPath.row {
        case 0:
            cell.update(with: .oldPassword)
            self.oldPasswordTextField = cell.changeTextField
            break
        case 1:
            cell.update(with: .newPassword)
            self.newPasswordTextField = cell.changeTextField
            break
        case 2:
            cell.update(with: .againPassword)
            self.newPasswordConfirmTextField = cell.changeTextField
            break
        default:
            break
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let cell: SettingsChangeCell = tableView.cellForRow(at: indexPath) as! SettingsChangeCell
        cell.changeTextField.becomeFirstResponder()
    }
    
    func tableView(_ tableView: UITableView, shouldHighlightRowAt indexPath: IndexPath) -> Bool {
        return true
    }
    
    func tableView(_ tableView: UITableView, didHighlightRowAt indexPath: IndexPath) {
        let cell: SettingsChangeCell = tableView.cellForRow(at: indexPath) as! SettingsChangeCell
        cell.backgroundColor = UIColor.white
        cell.setBorderColor(color: UIHelpers.green())
    }
    
    func tableView(_ tableView: UITableView, didUnhighlightRowAt indexPath: IndexPath) {
        let cell: SettingsChangeCell = tableView.cellForRow(at: indexPath) as! SettingsChangeCell
        cell.backgroundColor = UIColor.white
        cell.setBorderColor(color: UIHelpers.gray())
    }
    
    //
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if(textField == self.oldPasswordTextField) {
            self.newPasswordTextField.becomeFirstResponder()
        }
        
        if(textField == self.newPasswordTextField) {
            self.newPasswordConfirmTextField.becomeFirstResponder()
        }
        
        if(textField == self.newPasswordConfirmTextField) {
            textField.resignFirstResponder()
            self.onChange(self)
            return true
        }
        
        return true
    }
    
    @IBAction func onChange(_ sender: AnyObject) {
        self.view.isUserInteractionEnabled = false
        
        if(oldPasswordTextField.text!.isEmpty || newPasswordTextField.text!.isEmpty || newPasswordConfirmTextField.text!.isEmpty) {
            UIHelpers.shared.showErrorAlert(text: Localize(string: "settingsChangeFieldsEmpty"), isAutoClosable: true)
            self.view.isUserInteractionEnabled = true
            return
        }
        
        if(newPasswordTextField.text != newPasswordConfirmTextField.text) {
            UIHelpers.shared.showErrorAlert(text: Localize(string: "passwordMismatchMessage"), isAutoClosable: true)
            self.view.isUserInteractionEnabled = true
            return
        }
        
        UIHelpers.shared.showLoadingAlert(text: Localize(string: "change"))
        StoredUserManager().changePassword(old: oldPasswordTextField.text, new: newPasswordTextField.text, completion: completion)
    }
    
    func completion(isSuccess: Bool, errorMessage: String?) -> Void {
        if isSuccess {
            _ = self.navigationController?.popViewController(animated: true)
        } else {
            UIHelpers.shared.showErrorAlert(text: errorMessage, isAutoClosable: true)
        }
        
        self.view.isUserInteractionEnabled = true
        UIHelpers.shared.closeAlert()
    }
}
