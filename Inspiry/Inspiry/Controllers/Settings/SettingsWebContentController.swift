//
//  SettingsWebContentController.swift
//  Inspiry
//
//  Created by Alexander Danilyak on 03/08/16.
//  Copyright © 2016 Inspiry. All rights reserved.
//

import Foundation
import UIKit

enum WebContentType {
    case privacy
    case terms
}

class SettingsWebContentController: UIViewController {
    
    var navBarTitle: String = ""
    var requestToLoad: URLRequest? = nil
    var type: WebContentType = .terms
    
    @IBOutlet weak var webView: UIWebView!
    
    override func viewDidLoad() {
        webView.loadRequest(self.requestToLoad!)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.navigationItem.title = navBarTitle
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        let name = self.type == .terms ? screenTerms : screenPrivacy
        Analytics.shared.logScreen(name: name)
    }
}
