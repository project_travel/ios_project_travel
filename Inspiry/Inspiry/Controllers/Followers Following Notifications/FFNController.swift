//
//  FFNController.swift
//  Inspiry
//
//  Created by Alexander Danilyak on 26/07/16.
//  Copyright © 2016 Inspiry. All rights reserved.
//

import Foundation
import UIKit
import SideMenu

enum FFNType: Int {
    case following = 1
    case followers = 2
    case notifications = 3
    case notset = 0
}

class FFNController: UIViewController, UITableViewDelegate, UITableViewDataSource, UISearchResultsUpdating {
    
    @IBOutlet weak var tableView: UITableView!
    
    var isMenuButtonNeeded: Bool = false
    var menuButton: UIBarButtonItem? = nil
    
    let searchController = UISearchController(searchResultsController: nil)
    
    var type: FFNType = .notset
    
    var user: User?
    var model: FFNModel?
    
    var noContentLabel: UILabel = UIHelpers.getLabel(with: Localize(string: "noContent"))
    
    // MARK: View Controller
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setType()
        setTitle()
        setupModel()
        menuButtonInit()
        tableViewSetup()
        
        // Search Setup
        if(type != .notifications) {
            self.searchSetup()
        }
    }
    
    func searchSetup() {
        searchController.searchResultsUpdater = self
        searchController.dimsBackgroundDuringPresentation = false
        searchController.searchBar.searchBarStyle = .minimal
        searchController.searchBar.tintColor = UIHelpers.green()
        definesPresentationContext = true
        tableView.tableHeaderView = searchController.searchBar
    }
    
    func tableViewSetup() {
        // Trick to hide separator lines of not used cells
        self.tableView.tableFooterView = UIView()
        
        self.noContentLabel.center = self.view.center
        self.view.addSubview(self.noContentLabel)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    
    // MARK: Menu Setup
    
    func menuButtonInit() {
        if isMenuButtonNeeded {
            self.menuButton = UIBarButtonItem.init(image: UIImage.init(named: "menu"), style: .plain, target: self, action:#selector(openMenu))
            self.navigationItem.leftBarButtonItem = menuButton
        }
    }
    
    func openMenu() {
        self.present(SideMenuManager.menuLeftNavigationController!, animated: true, completion: nil)
    }
    
    deinit {
        searchController.view.removeFromSuperview()
    }
    
    func setupModel() {
        self.model = FFNModel.init(username: self.user!.username, pageSize: 50)
        self.loadDataWrap()
    }
    
    func setType() {
        type = .notset
    }
    
    // MARK: Title
    
    func setTitle() {
        var title = ""
        switch type {
        case .following:
            title = Localize(string: "titleFollowing")
            break
        case .followers:
            title = Localize(string: "titleFollowers")
            break
        case .notifications:
            title = Localize(string: "titleNotifications")
            break
        case .notset:
            assert(true)
        }
        self.navigationItem.title = title
    }
    
    // MARK: Data 
    
    func loadDataWrap() {
        self.model!.loadData(for: type, with: pageLoaded)
    }
    
    func pageLoaded(isSuccess: Bool, lastDisplayed: Int, newCount: Int, errorDescription: String) -> Void {
        self.noContentLabel.isHidden = !(lastDisplayed == 0 && newCount == 0)
        
        if(isSuccess) {
            if newCount == 0 { return }
            
            var indexes = [IndexPath]()
            for i in lastDisplayed...(lastDisplayed+newCount-1) {
                indexes.append(IndexPath(row: i, section: 0))
            }
            
            self.tableView.insertRows(at: indexes, with: .none)
        } else {
            UIHelpers.shared.showErrorAlert(text: errorDescription, isAutoClosable: true)
        }
    }
    
    func filterContentForSearchText(searchText: String) {
        self.model!.performSearch(for: type, with: searchText)
        tableView.reloadData()
    }
    
    // MARK: Search Results Updating
    
    func updateSearchResults(for searchController: UISearchController) {
        filterContentForSearchText(searchText: searchController.searchBar.text!)
    }
    
    // MARK: Table View
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        let isSearch = searchController.isActive && searchController.searchBar.text != ""
        
        switch type {
        case .following:
            return isSearch ? (self.model!.localSearchResultsFollowing?.count)! : self.model!.dataFollowing.count
        case .followers:
            return isSearch ? (self.model!.localSearchResultsFollowers?.count)! : self.model!.dataFollowers.count
        case .notifications:
            return self.model!.dataNotifications.count
        case .notset:
            return 0
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        switch type {
        case .following:
            return setupFollowingCell(indexPath: indexPath)
        case .followers:
            return setupFollowerCell(indexPath: indexPath)
        case .notifications:
            return setupNotificationCell(indexPath: indexPath)
        case .notset:
            return UITableViewCell()
        }
    }
    
    func setupFollowingCell(indexPath: IndexPath) -> UITableViewCell {
        let cell: FFCell = self.tableView.dequeueReusableCell(withIdentifier: "ffCell", for: indexPath) as! FFCell
        
        let isSearch = searchController.isActive && searchController.searchBar.text != ""
        let user = isSearch ? (self.model!.localSearchResultsFollowing?[indexPath.row] as! User) : self.model!.dataFollowing[indexPath.row]
        
        cell.update(with: user)
        
        return cell
    }
    
    func setupFollowerCell(indexPath: IndexPath) -> UITableViewCell {
        let cell: FFCell = self.tableView.dequeueReusableCell(withIdentifier: "ffCell", for: indexPath) as! FFCell
        
        let isSearch = searchController.isActive && searchController.searchBar.text != ""
        let user = isSearch ? (self.model!.localSearchResultsFollowers?[indexPath.row] as! User) : self.model!.dataFollowers[indexPath.row]
        
        cell.update(with: user)
        
        return cell
    }
    
    func setupNotificationCell(indexPath: IndexPath) -> UITableViewCell {
        let cell: NotificationCell = self.tableView.dequeueReusableCell(withIdentifier: "notificationCell", for: indexPath) as! NotificationCell
        cell.update(with: self.model!.dataNotifications[indexPath.row])
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        let isSearch = searchController.isActive && searchController.searchBar.text != ""
        switch type {
        case .following:
            let user = isSearch ? (self.model!.localSearchResultsFollowing?[indexPath.row] as! User) : self.model!.dataFollowing[indexPath.row]
            let profileController = UIHelpers.getProfileController(with: user)
            if profileController == nil { return }
            self.navigationController!.pushViewController(profileController!, animated: true)
            break
        case .followers:
            let user = isSearch ? (self.model!.localSearchResultsFollowers?[indexPath.row] as! User) : self.model!.dataFollowers[indexPath.row]
            let profileController = UIHelpers.getProfileController(with: user)
            if profileController == nil { return }
            self.navigationController!.pushViewController(profileController!, animated: true)
            break
        case .notifications:
            break
        case .notset:
            break
        }
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        
        var stop: Bool = false
        var lastIndex: Int = 0
        
        switch type {
        case .following:
            stop = self.model!.stopFollowing
            lastIndex = self.model!.dataFollowing.count - 1
            break
        case .followers:
            stop = self.model!.stopFollowers
            lastIndex = self.model!.dataFollowers.count - 1
            break
        case .notifications:
            stop = self.model!.stopNotifications
            lastIndex = self.model!.dataNotifications.count - 1
            break
        case .notset:
            break
        }
        
        if(indexPath.row == lastIndex && stop != true) {
            self.loadDataWrap()
        }
    }    
}
