//
//  FollowingController.swift
//  Inspiry
//
//  Created by Alexander Danilyak on 31/07/16.
//  Copyright © 2016 Inspiry. All rights reserved.
//

import Foundation
import UIKit

class FollowingController: FFNController {
    override func setType() {
        type = .following
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        Analytics.shared.logScreen(name: screenFollowing, parameters: [profileUsername: self.user!.username])
    }
}
