//
//  FollowerController.swift
//  Inspiry
//
//  Created by Alexander Danilyak on 31/07/16.
//  Copyright © 2016 Inspiry. All rights reserved.
//

import Foundation
import UIKit

class FollowersController: FFNController {
    override func setType() {
        type = .followers
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        Analytics.shared.logScreen(name: screenFollowers, parameters: [profileUsername: self.user!.username])
    }
}
