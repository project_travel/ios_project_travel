//
//  NotificationsController.swift
//  Inspiry
//
//  Created by Alexander Danilyak on 31/07/16.
//  Copyright © 2016 Inspiry. All rights reserved.
//

import Foundation
import UIKit

class NotificationsController: FFNController {
    override func setType() {
        type = .notifications
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        Analytics.shared.logScreen(name: screenNotifications)
    }
    
    override func tableViewSetup() {
        super.tableViewSetup()
        self.tableView.estimatedRowHeight = 70.0
        self.tableView.rowHeight = UITableViewAutomaticDimension
    }
}
