//
//  VefiryEmailController.swift
//  Inspiry
//
//  Created by Alexander Danilyak on 16/08/16.
//  Copyright © 2016 Inspiry. All rights reserved.
//

import Foundation
import UIKit

class VerifyController: UIViewController {
    var isVerified: Bool? = nil
    var timer: Timer? = nil
    
    @IBOutlet weak var nextButton: UIButton!
    @IBOutlet weak var resendButton: UIButton!
    @IBOutlet weak var verifiedImageView: UIImageView!
    
    @IBOutlet weak var verifiedImageSize: NSLayoutConstraint!
    
    @IBOutlet weak var pleaseConfirmLabel: UILabel!
    @IBOutlet weak var confirmHintLabel: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.setVerified(isVerified: false)
        
        nextButton.isEnabled = false
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.navigationItem.title = Localize(string: "emailConfirmationTitle")
        
        self.nextButton.layer.cornerRadius = 13
        self.nextButton.layer.borderWidth = 2
        self.nextButton.layer.borderColor = UIHelpers.green().cgColor
        
        nextButton.setTitleColor(UIHelpers.gray(), for: .disabled)
        nextButton.setTitleColor(UIHelpers.green(), for: .normal)
        
        let color  = self.isVerified! ? UIHelpers.green().cgColor : UIHelpers.gray().cgColor
        self.nextButton.layer.borderColor = color
        
        self.nextButton.setTitle(Localize(string: "nextTitle"), for: .normal)
        self.resendButton.setTitle(Localize(string: "noEmailTitle"), for: .normal)
        self.confirmHintLabel.text = Localize(string: "toConfirm");
        
        if(!self.isVerified!) {
            self.timer = Timer.scheduledTimer(withTimeInterval: 3.0, repeats: true, block: { (timer) in
                if(!self.isVerified!) { self.checkVerification() }
            })
            
        } else {
            self.setVerified(isVerified: true)
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        Analytics.shared.logScreen(name: screenVerify)
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        
        self.timer?.invalidate()
    }
    
    func setVerified(isVerified: Bool) {
        if(self.isVerified == nil) { self.isVerified = isVerified }
        self.isVerified = isVerified
        let const: CGFloat = self.isVerified! ? 70.0 : 0.0
        
        self.verifiedImageSize.constant = const
        UIView.animate(withDuration: 0.1, animations: {
            self.view.layoutIfNeeded()
            let color  = self.isVerified! ? UIHelpers.green().cgColor : UIHelpers.gray().cgColor
            
            if(self.isVerified!) {
                self.pleaseConfirmLabel.text = String(format: Localize(string: "youHaveConfirmed"), StoredUser.shared.user!.email)
                self.confirmHintLabel.alpha = 0
            } else {
                self.pleaseConfirmLabel.text = String(format: Localize(string: "pleaseConfirm"), StoredUser.shared.user!.email)
                self.confirmHintLabel.alpha = 1
            }
            
            self.nextButton.layer.borderColor = color
            
            })
        
        if(self.isVerified!) { self.timer?.invalidate() }
        self.nextButton.isEnabled = self.isVerified!
        
        if(self.isVerified!) { self.navigationItem.hidesBackButton = true }
    }
    
    func checkVerification() {
        AuthManager().loginWithToken(tokenKey: inspiryRefreshTokenKey, completion: { isSuccess, errorDescription in
            if(isSuccess) {
                self.setVerified(isVerified: true)
                self.timer = nil
                UserAuth.shared.state = .hasBothTokens
                print("[AT] " + UserDefaults.standard.getAccessToken()!)
            } else {
                print("[VERIFY FAILED] " + errorDescription!)
            }
        })
    }
    
    @IBAction func onResend(_ sender: AnyObject) {
        AuthManager().resendVerification(email: StoredUser.shared.user!.email, completion: { (isSuccess, errorDescription) in
            if(isSuccess) {
                let text = String(format: Localize(string: "sendTo"), StoredUser.shared.user!.email)
                UIHelpers.shared.showNotificationAlert(text: text, isAutoClosable: true)
            } else {
                UIHelpers.shared.showErrorAlert(text: errorDescription, isAutoClosable: true)
            }
        })
    }
    
    @IBAction func onNext(_ sender: AnyObject) {
        let loginStoryboard: UIStoryboard? = UIStoryboard(name: "Login", bundle: nil)
        let linkSocialController = (loginStoryboard?.instantiateViewController(withIdentifier: "linkSocial"))!
        self.navigationController?.pushViewController(linkSocialController, animated: true)
    }
}
