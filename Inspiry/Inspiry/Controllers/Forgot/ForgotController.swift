//
//  ForgotController.swift
//  Inspiry
//
//  Created by Alexander Danilyak on 20/08/16.
//  Copyright © 2016 Inspiry. All rights reserved.
//

import Foundation
import UIKit

class ForgotController: UIViewController, UITableViewDelegate, UITableViewDataSource, UITextFieldDelegate {
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var sendButton: UIButton!
    
    weak var emailTextField: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.setUpText()
        self.setUpTableView()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.sendButton.layer.cornerRadius = 13
        self.sendButton.layer.borderWidth = 2
        self.sendButton.layer.borderColor = UIHelpers.green().cgColor
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        Analytics.shared.logScreen(name: screenForgot)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        UIHelpers.shared.closeAlert()
    }
    
    func setUpText() {
        self.sendButton.setTitle(Localize(string: "forgotSend"), for: .normal)
    
        self.navigationItem.title = Localize(string: "forgotTitle")
    }
    
    func setUpTableView() {
        let tapToHideKeyboard: UITapGestureRecognizer! = UITapGestureRecognizer(target: self, action:#selector(hideKeyboard))
        tapToHideKeyboard.cancelsTouchesInView = false
        self.view.addGestureRecognizer(tapToHideKeyboard)
    }
    
    func hideKeyboard() {
        self.view.endEditing(true)
    }
    
    //MARK: Table View
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let identifier = "signUpCell"
        
        let cell = tableView.dequeueReusableCell(withIdentifier: identifier, for: indexPath) as! SignUpCell
        
        let type: SignUpCellType = .email
        cell.textField.delegate = self
        cell.setup(for: type)
        self.emailTextField = cell.textField

        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: false)
        let cell: SignUpCell = tableView.cellForRow(at: indexPath) as! SignUpCell
        cell.textField.becomeFirstResponder()
    }
    
    func tableView(_ tableView: UITableView, shouldHighlightRowAt indexPath: IndexPath) -> Bool {
        return true
    }
    
    func tableView(_ tableView: UITableView, didHighlightRowAt indexPath: IndexPath) {
        let cell: SignUpCell = tableView.cellForRow(at: indexPath) as! SignUpCell
        cell.backgroundColor = UIColor.white
        cell.setBorderColor(color: UIHelpers.green())
    }
    
    func tableView(_ tableView: UITableView, didUnhighlightRowAt indexPath: IndexPath) {
        let cell: SignUpCell = tableView.cellForRow(at: indexPath) as! SignUpCell
        cell.backgroundColor = UIColor.white
        cell.setBorderColor(color: UIHelpers.gray())
    }
    
    //MARK: Text Field Delegate
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.emailTextField.resignFirstResponder()
        self.onSend(self)
        return true
    }
    
    // MARK: Actions
    @IBAction func onSend(_ sender: AnyObject) {
        UIHelpers.shared.showLoadingAlert(text: Localize(string: "wait"))
        
        if((self.emailTextField.text?.isEmpty)!) {
            UIHelpers.shared.showErrorAlert(text: Localize(string: "forrgotEmptyEmail"), isAutoClosable: true)
            return
        }
        
        if(!isStringEmail(isEmailString:self.emailTextField.text)) {
            UIHelpers.shared.showErrorAlert(text: Localize(string: "invalidEmailMessage"), isAutoClosable: true)
            return
        }
        
        AuthManager().forgot(email: self.emailTextField.text, completion: { (isSuccess, errorDescription) in
            UIHelpers.shared.closeAlert()
            if(isSuccess) {
                let text = String(format: Localize(string: "sendTo"), self.emailTextField.text!)
                UIHelpers.shared.showNotificationAlert(text: text, isAutoClosable: true)
            } else {
                UIHelpers.shared.showErrorAlert(text: errorDescription, isAutoClosable: true)
            }
        })
    }
}
