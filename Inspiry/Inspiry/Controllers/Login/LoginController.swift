//
//  LoginController.swift
//  Inspiry
//
//  Created by Alexander Danilyak on 11/07/16.
//  Copyright © 2016 Inspiry. All rights reserved.
//

import Foundation
import UIKit

let signInGoogleCompleted = Notification.Name("signInGoogleCompleted")

class LoginController: UIViewController, UITableViewDelegate, UITableViewDataSource, UITextFieldDelegate, GIDSignInUIDelegate {
    @IBOutlet weak var backImageToTop: NSLayoutConstraint!
    
    @IBOutlet weak var tableview: UITableView!
    weak var loginTextField: UITextField!
    weak var passwordTextField: UITextField!
    var keyboardShown: Bool = false
    
    @IBOutlet weak var promoLabel: UILabel!
    @IBOutlet weak var forgotButton: UIButton!
    @IBOutlet weak var signInButton: UIButton!
    @IBOutlet weak var dontHaveAnAccountButton: UIButton!
    
    @IBOutlet weak var loginImageRatio: NSLayoutConstraint!
    @IBOutlet weak var loginImage4SRatio: NSLayoutConstraint!
    
    @IBOutlet weak var googleSignInButton: GIDSignInButton!
    @IBOutlet weak var googleActivityView: UIActivityIndicatorView!
    
    var googleData: [String: String]!
    
    override init(nibName: String?, bundle: Bundle?) {
        super.init(nibName: nibName, bundle: bundle)
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        GIDSignIn.sharedInstance().uiDelegate = self
        
        self.setUpText()
        self.setUpTableView()
        self.setUpKeyboard()
        
        NotificationCenter.default.addObserver(self, selector: #selector(onSignInGoogleCompleted(notification:)), name: signInGoogleCompleted, object: nil)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.navigationBar.isHidden = true
        
        self.promoLabel.text = Localize(string: "loginPromo")
        
        
        // Menu Selected Index
        MenuController.currentMenuIndex = 1
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        Analytics.shared.logScreen(name: screenLogin)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.navigationController?.navigationBar.isHidden = false
        UIHelpers.shared.closeAlert()
    }
    
    override func updateViewConstraints() {
        DispatchQueue.main.async {
            self.loginImageRatio.isActive = !UIHelpers.isPhone4ScreenSize()
            self.loginImage4SRatio.isActive = UIHelpers.isPhone4ScreenSize()
        }
        super.updateViewConstraints()
    }
    
    func setUpText() {
        self.signInButton.setTitle(Localize(string: "signIn"), for: .normal)
        self.dontHaveAnAccountButton.setTitle(Localize(string: "noAccount"), for: .normal)
        self.forgotButton.setTitle(Localize(string: "forgot"), for: .normal)
    }
    
    func setUpTableView() {
        self.tableview.contentInset = UIEdgeInsets.zero
        
        let tapToHideKeyboard: UITapGestureRecognizer! = UITapGestureRecognizer(target: self, action:#selector(hideKeyboard))
        tapToHideKeyboard.cancelsTouchesInView = false
        self.view.addGestureRecognizer(tapToHideKeyboard)
    }
    
    func hideKeyboard() {
        self.view.endEditing(true)
    }
    
    // MARK: Text Field Delegate
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if(textField == loginTextField) {
            passwordTextField.becomeFirstResponder()
        }
        
        if(textField == passwordTextField) {
            textField.resignFirstResponder()
            self.trySignIn(self)
        }
        return true
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        textField.resignFirstResponder()
    }
    
    
    // MARK: Handle button taps
    
    @IBAction func trySignIn(_ sender: AnyObject) {
        if(!self.loginTextField.text!.isEmpty && !self.passwordTextField.text!.isEmpty) {
            UIHelpers.shared.showLoadingAlert(text: Localize(string: "wait"))
            self.doSignIn()
        } else {
            UIHelpers.shared.showErrorAlert(text: Localize(string: "emptyMessage"), isAutoClosable: true)
        }
    }
    
    func doSignIn() {
        AuthManager().login(login: loginTextField.text, password: passwordTextField.text, needRefreshToken: true, completion:
            { isSuccess, errorDescription in
                UIHelpers.shared.closeAlert()
                self.view.isUserInteractionEnabled = true
                
                if(isSuccess == false) {
                    UIHelpers.shared.showErrorAlert(text: errorDescription!, isAutoClosable: true)
                } else {
                    let mainStoryboard: UIStoryboard? = UIStoryboard(name: "Main", bundle: nil)
                    let mainController = mainStoryboard?.instantiateInitialViewController() as! UINavigationController
                    
                    (mainController.viewControllers[0] as! ProfileController).isMyProfile = true
                    (mainController.viewControllers[0] as! ProfileController).isMenuButtonNeeded = true
                    
                    UIApplication.shared.delegate?.window??.rootViewController = mainController
                    UIApplication.shared.delegate?.window??.makeKeyAndVisible()
                }
            }
        )
    }
    
    @IBAction func goToSignUp(_ sender: AnyObject) {
        let loginStoryboard: UIStoryboard? = UIStoryboard(name: "Login", bundle: nil)
        let signUpController: SignUpController! = loginStoryboard?.instantiateViewController(withIdentifier: "signUp") as! SignUpController
        self.navigationController?.pushViewController(signUpController, animated: true)
    }
    
    @IBAction func onForgot(_ sender: AnyObject) {
        let loginStoryboard: UIStoryboard? = UIStoryboard(name: "Login", bundle: nil)
        let forgotController: ForgotController! = loginStoryboard?.instantiateViewController(withIdentifier: "forgot") as! ForgotController
        self.navigationController?.pushViewController(forgotController, animated: true)
    }
    
    // MARK: Table View
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 2
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let identifier = "loginCell"
        
        let cell = tableView.dequeueReusableCell(withIdentifier: identifier, for: indexPath) as! LoginCell
        
        let type: LoginCellType = indexPath.row == 0 ? .name : .password
        cell.setup(for: type)
        
        cell.textField.delegate = self
        if(type == .name) { loginTextField = cell.textField }
        if(type == .password) { passwordTextField = cell.textField }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: false)
        let cell: LoginCell = tableView.cellForRow(at: indexPath) as! LoginCell
        cell.textField.becomeFirstResponder()
    }
    
    func tableView(_ tableView: UITableView, shouldHighlightRowAt indexPath: IndexPath) -> Bool {
        return true
    }
    
    func tableView(_ tableView: UITableView, didHighlightRowAt indexPath: IndexPath) {
        let cell: LoginCell = tableView.cellForRow(at: indexPath) as! LoginCell
        cell.backgroundColor = UIColor.clear
        cell.setBorderColor(color: UIHelpers.green())
    }
    
    func tableView(_ tableView: UITableView, didUnhighlightRowAt indexPath: IndexPath) {
        let cell: LoginCell = tableView.cellForRow(at: indexPath) as! LoginCell
        cell.backgroundColor = UIColor.clear
        cell.setBorderColor(color: UIHelpers.gray())
    }
    
    // MARK: Keyboard
    
    func setUpKeyboard() {
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardChangeFrame), name:NSNotification.Name.UIKeyboardWillChangeFrame, object: nil)
    }
    
    func keyboardChangeFrame(notification: Notification) {
        let keyboardFrameEnd = (notification.userInfo?[UIKeyboardFrameEndUserInfoKey] as! NSValue).cgRectValue
        
        let hide: Bool = keyboardFrameEnd.origin.y == UIScreen.main.bounds.size.height
        
        let keyboardHeight = keyboardFrameEnd.size.height
        
        let keyboardAnimationDuration = notification.userInfo?[UIKeyboardAnimationDurationUserInfoKey] as! CGFloat
        
        let offset = keyboardHeight / 3
        
        let const: CGFloat = hide == true ? offset : -offset
        if(self.keyboardShown == hide) {
            self.backImageToTop.constant += const
            self.keyboardShown = !hide
        }
        
        UIView.animate(withDuration: TimeInterval(keyboardAnimationDuration)) {
            self.view.layoutIfNeeded()
        }
    }
    
    // MARK: Google
    
    func onSignInGoogleCompleted(notification: Notification) {
        googleData = notification.userInfo as! [String: String]
        checkIfUserExistWith(email: googleData[apiKeyEmail], googleId: googleData[apiKeyGoogleUserId])

    }
    
    func checkIfUserExistWith(email: String!, googleId: String!) {
        AuthManager().loginWithGmailAndGoogleID(email: email, googleId: googleId, completion: googleLoginCompletion)
    }
    
    func googleLoginCompletion(isSuccess: Bool, errorDescription: String?) -> Void {
        if isSuccess {
            let mainStoryboard: UIStoryboard? = UIStoryboard(name: "Main", bundle: nil)
            let mainController = mainStoryboard?.instantiateInitialViewController() as! UINavigationController
            
            (mainController.viewControllers[0] as! ProfileController).isMyProfile = true
            (mainController.viewControllers[0] as! ProfileController).isMenuButtonNeeded = true
            
            UIApplication.shared.delegate?.window??.rootViewController = mainController
            UIApplication.shared.delegate?.window??.makeKeyAndVisible()
        } else {
            let loginStoryboard: UIStoryboard? = UIStoryboard(name: "Login", bundle: nil)
            let signUpController: SignUpController! = loginStoryboard?.instantiateViewController(withIdentifier: "signUp") as! SignUpController
            signUpController.googleData = googleData
            signUpController.type = .google
            self.navigationController?.pushViewController(signUpController, animated: true)
        }
    }
}
