//
//  SignUpController.swift
//  Inspiry
//
//  Created by Alexander Danilyak on 13/07/16.
//  Copyright © 2016 Inspiry. All rights reserved.
//

import Foundation
import UIKit

enum SignUpType {
    case ordinary
    case google
}

class SignUpController: UIViewController, UITableViewDelegate, UITableViewDataSource, UITextFieldDelegate {
    
    var type: SignUpType = .ordinary
    var googleData: [String: String]!
    
    @IBOutlet weak var tableView: UITableView!
    weak var usernameTextField: UITextField!
    weak var emailTextField: UITextField!
    weak var passwordTextField: UITextField!
    weak var passwordConfirmTextField: UITextField!
    @IBOutlet weak var signUpButton: UIButton!
    @IBOutlet weak var termOfServiceButton: UIButton!
    
    override init(nibName: String?, bundle: Bundle?) {
        super.init(nibName: nibName, bundle: bundle)
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.setUpText()
        self.setUpTableView()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.signUpButton.layer.cornerRadius = 13
        self.signUpButton.layer.borderWidth = 2
        self.signUpButton.layer.borderColor = UIHelpers.green().cgColor
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        Analytics.shared.logScreen(name: screenSignUp)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        UIHelpers.shared.closeAlert()
    }
    
    func setUpText() {
        self.signUpButton.setTitle(Localize(string: "signUp"), for: .normal)
        self.termOfServiceButton.setTitle(Localize(string: "signUpTerms"), for: .normal)
        self.navigationItem.title = type == .ordinary ? Localize(string: "signUpTitle") : Localize(string: "yourUsername")
    }
    
    func setUpTableView() {
        let tapToHideKeyboard: UITapGestureRecognizer! = UITapGestureRecognizer(target: self, action:#selector(hideKeyboard))
        tapToHideKeyboard.cancelsTouchesInView = false
        self.view.addGestureRecognizer(tapToHideKeyboard)
    }
    
    func hideKeyboard() {
        self.view.endEditing(true)
    }
    
    // MARK: Handle Button Taps
    
    @IBAction func trySignUp(_ sender: AnyObject) {
        let generatedPassword = generatePassword()
        var signUpData: [String: String] = [apiKeyUsername: self.usernameTextField.text!,
                          apiKeyEmail: type == .ordinary ? self.emailTextField.text! : googleData[apiKeyEmail]!,
                          apiKeyPassword: type == .ordinary ? self.passwordTextField.text! : generatedPassword,
                          apiKeyPasswordAgain: type == .ordinary ? self.passwordConfirmTextField.text! : generatedPassword,
                          apiKeyGoogleUserId: type == .ordinary ? "" : googleData[apiKeyGoogleUserId]!]
        
        if(!signUpData[apiKeyUsername]!.isEmpty &&
            !signUpData[apiKeyEmail]!.isEmpty &&
            !signUpData[apiKeyPassword]!.isEmpty &&
            !signUpData[apiKeyPasswordAgain]!.isEmpty) {
            if(isStringEmail(isEmailString: signUpData[apiKeyEmail]!)) {
                if(signUpData[apiKeyPassword]! == signUpData[apiKeyPasswordAgain]!) {
                    if(isValidUsername(isUsernameString: signUpData[apiKeyUsername]!)) {
                        UIHelpers.shared.showLoadingAlert(text: Localize(string: "wait"))
                        self.doSignUp(username: signUpData[apiKeyUsername]!,
                                      email: signUpData[apiKeyEmail]!,
                                      password: signUpData[apiKeyPassword]!,
                                      googleId: signUpData[apiKeyGoogleUserId])
                    } else {
                        UIHelpers.shared.showErrorAlert(text: Localize(string: "invalidUsernameMessage"), isAutoClosable: true)
                    }
                } else {
                    UIHelpers.shared.showErrorAlert(text: Localize(string: "passwordMismatchMessage"), isAutoClosable: true)
                }
            } else {
                UIHelpers.shared.showErrorAlert(text: Localize(string: "invalidEmailMessage"), isAutoClosable: true)
            }
        } else {
            UIHelpers.shared.showErrorAlert(text: Localize(string: "emptyMessage"), isAutoClosable: true)
        }
    }
    
    func doSignUp(username: String, email: String, password: String, googleId: String?) {
        AuthManager().register(username: username,
                               email: email,
                               password: password,
                               googleId: googleId,
                               completion:
            { isSuccess, errorDescription in
                UIHelpers.shared.closeAlert()
                self.view.isUserInteractionEnabled = true
                
                if(isSuccess == false) {
                    UIHelpers.shared.showErrorAlert(text: errorDescription, isAutoClosable: true)
                } else {
//                    let loginStoryboard: UIStoryboard? = UIStoryboard(name: "Login", bundle: nil)
//                    let verificationController = (loginStoryboard?.instantiateViewController(withIdentifier: "verify"))!
//                    self.navigationController?.pushViewController(verificationController, animated: true)
                    let loginStoryboard: UIStoryboard? = UIStoryboard(name: "Login", bundle: nil)
                    let linkSocialController = (loginStoryboard?.instantiateViewController(withIdentifier: "linkSocial"))!
                    self.navigationController?.pushViewController(linkSocialController, animated: true)
                }
            }
        )
    }
    
    @IBAction func onTerms(_ sender: AnyObject) {
        let settingsStoryboard: UIStoryboard? = UIStoryboard(name: "Settings", bundle: nil)
        let webContentController: SettingsWebContentController! = settingsStoryboard?.instantiateViewController(withIdentifier: "settingsWebContent") as! SettingsWebContentController
        
        webContentController.requestToLoad = URLRequest.init(url: URL.init(string: termsLink)!)
        webContentController.navBarTitle = Localize(string: "settingsTerms")
        webContentController.type = .terms
        
        self.navigationController?.pushViewController(webContentController, animated: true)
    }
    
    //MARK: Table View
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return type == .ordinary ? 4 : 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let identifier = "signUpCell"
        
        let cell = tableView.dequeueReusableCell(withIdentifier: identifier, for: indexPath) as! SignUpCell
        
        let type: SignUpCellType = SignUpCellType(rawValue: indexPath.row)!
        cell.textField.delegate = self
        cell.setup(for: type)
        
        switch(type) {
        case .name:
            self.usernameTextField = cell.textField
            break
        case .email:
            self.emailTextField = cell.textField
            break
        case .password:
            self.passwordTextField = cell.textField
            break
        case .confirmPassword:
            self.passwordConfirmTextField = cell.textField
            break
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: false)
        let cell: SignUpCell = tableView.cellForRow(at: indexPath) as! SignUpCell
        cell.textField.becomeFirstResponder()
    }
    
    func tableView(_ tableView: UITableView, shouldHighlightRowAt indexPath: IndexPath) -> Bool {
        return true
    }
    
    func tableView(_ tableView: UITableView, didHighlightRowAt indexPath: IndexPath) {
        let cell: SignUpCell = tableView.cellForRow(at: indexPath) as! SignUpCell
        cell.backgroundColor = UIColor.white
        cell.setBorderColor(color: UIHelpers.green())
    }
    
    func tableView(_ tableView: UITableView, didUnhighlightRowAt indexPath: IndexPath) {
        let cell: SignUpCell = tableView.cellForRow(at: indexPath) as! SignUpCell
        cell.backgroundColor = UIColor.white
        cell.setBorderColor(color: UIHelpers.gray())
    }
    
    //MARK: Text Field Delegate
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if(textField == self.usernameTextField) {
            if type == .google { self.trySignUp(self); return true }
            self.emailTextField.becomeFirstResponder()
        }
        
        if(textField == self.emailTextField) {
            self.passwordTextField.becomeFirstResponder()
        }
        
        if(textField == self.passwordTextField) {
            self.passwordConfirmTextField.becomeFirstResponder()
        }
        
        if(textField == self.passwordConfirmTextField) {
            textField.resignFirstResponder()
            self.trySignUp(self)
            return true
        }
        
        return true
    }
}
