//
//  MapController.swift
//  Inspiry
//
//  Created by Alexander Danilyak on 06/08/16.
//  Copyright © 2016 Inspiry. All rights reserved.
//

import Foundation
import UIKit
import MapKit

class MapController: UIViewController, MKMapViewDelegate {
    
    var posts: [Post]?
    
    @IBOutlet weak var closeButton: UIBarButtonItem!
    @IBOutlet weak var mapView: MKMapView!
    
    // MARK: View Controller
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        mapView.delegate = self
        addAnnotations()
        
        let center = calcMidCoordinate()
        centerMapOnLocation(location: center)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.closeButton.title = Localize(string: "mapClose")
        self.navigationItem.title = Localize(string: "mapTitle")
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        Analytics.shared.logScreen(name: screenMap)
    }
    
    // MARK: Coordinates
    
    func calcMidCoordinate() -> CLLocation {
        var midLat: Double = 0
        var midLon: Double = 0
        
        var count: Int = 0
        
        for post in self.posts! {
            if post.location != nil {
                midLat += post.location!.lat
                midLon += post.location!.lon
                count += 1
            }
        }
        
        midLat = midLat / Double(count)
        midLon = midLon / Double(count)
        
        return CLLocation.init(latitude: midLat, longitude: midLon)
    }
    
    // MARK: Map
    
    let regionRadius: CLLocationDistance = 100000
    
    func centerMapOnLocation(location: CLLocation) {
        let coordinateRegion = MKCoordinateRegionMakeWithDistance(location.coordinate,
                                                                  regionRadius * 2.0, regionRadius * 2.0)
        mapView.setRegion(coordinateRegion, animated: true)
    }
    
    func addAnnotations() {
        for post in self.posts! {
            if post.location != nil {
                mapView.addAnnotation(PostAnnotation.init(post: post))
            }
        }
    }
    
    // MARK: Map View Delegate
    
    func mapView(_ mapView: MKMapView, viewFor annotation: MKAnnotation) -> MKAnnotationView? {
        if let annotation = annotation as? PostAnnotation {
            let identifier = "pin"
            var view: MKAnnotationView
            if let dequeuedView = mapView.dequeueReusableAnnotationView(withIdentifier: identifier) {
                dequeuedView.annotation = annotation
                view = dequeuedView
            } else {
                view = MKAnnotationView(annotation: annotation, reuseIdentifier: identifier)
                view.image = #imageLiteral(resourceName: "inspiryPin")
                view.canShowCallout = true
                view.calloutOffset = CGPoint(x: -5, y: -2)
            }
            return view
        }
        return nil
    }
    
    // MARK: Close 
    
    @IBAction func onClose(_ sender: AnyObject) {
        self.dismiss(animated: true, completion: nil)
    }
    
}

class PostAnnotation: NSObject, MKAnnotation {
    var coordinate: CLLocationCoordinate2D
    var title: String?
    var subtitle: String?
    
    init(post: Post) {
        self.title = post.text
        
        self.subtitle = post.source.type.rawValue
        self.coordinate = CLLocationCoordinate2D.init(latitude: post.location!.lat, longitude: post.location!.lon)
    }
}
