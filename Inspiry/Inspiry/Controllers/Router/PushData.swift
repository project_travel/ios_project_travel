//
//  PushData.swift
//  Inspiry
//
//  Created by Alexander Danilyak on 14/08/16.
//  Copyright © 2016 Inspiry. All rights reserved.
//

import Foundation

let apsPushKey = "aps"
let alertPushKey = "alert"
let bodyPushKey = "body"
let titlePushKey = "title"
let customPushKey = "custom"
let aPushKey = "a"

let typePushKey = "type"
let tripIdPushKey = "tripId"
let followerUsernamePushKey = "followerUsername"
let newIdsPushKey = "newIds"
let updatedIdsPushKey = "updatedIds"
let isUpdateFailedPushKey = "isUpdateFailed"

enum PushNotificationType: String {
    case newLike = "newLike" // open PostsController
    case newComment = "newComment" // open PostsController -> Comments
    case newFollower = "newFollower" // open Followers
    case newTripsExtracted = "newTripsExtracted" // open Profile
}

class PushData {
    var type: PushNotificationType
    var title: String?
    var body: String
    
    // Used for types: NewLike & NewComment
    var tripId: String?
    
    // Used for type: NewFollower
    var followerUsername: String?
    
    // Used for type: NewTripsExctracted
    var newIds: [String]?
    var updatedIds: [String]?
    var isUpdateFailed: Bool?
    
    init(with body: String, and userInfo: [String: AnyObject]) {
        self.body = body
        self.title = userInfo[titlePushKey] as? String
        self.type = PushNotificationType.init(rawValue: userInfo[typePushKey] as! String)!
        
        switch self.type {
        case .newLike:
            self.tripId = userInfo[tripIdPushKey] as? String
            break
        case .newComment:
            self.tripId = userInfo[tripIdPushKey] as? String
            break
        case .newFollower:
            self.followerUsername = userInfo[followerUsernamePushKey] as? String
            break
        case .newTripsExtracted:
            self.newIds = userInfo[newIdsPushKey] as? [String]
            self.updatedIds = userInfo[updatedIdsPushKey] as? [String]
            self.isUpdateFailed = userInfo[isUpdateFailedPushKey] as? Bool
            break
        }
    }
}
