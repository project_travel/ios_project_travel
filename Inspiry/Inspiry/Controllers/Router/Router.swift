//
//  Router.swift
//  Inspiry
//
//  Created by Alexander Danilyak on 12/08/16.
//  Copyright © 2016 Inspiry. All rights reserved.
//

import Foundation
import UIKit

class Router {
    static let shared = Router()
    
    private init() {
    }
    
    static func getTopNavigationController() -> UINavigationController {
        var result: UIViewController = UIApplication.shared.keyWindow!.rootViewController!
        
        while result.presentedViewController != nil {
            result = result.presentedViewController!
        }
        
        let topNavigationController: UINavigationController = result.isKind(of: UINavigationController.classForCoder()) ? result as! UINavigationController : result.navigationController!
        
        return topNavigationController
    }
    
    static func getTopMostVisibleController() -> UIViewController {
        var result: UIViewController = UIApplication.shared.keyWindow!.rootViewController!
        
        while result.presentedViewController != nil {
            result = result.presentedViewController!
        }
        
        if(result.isKind(of: UINavigationController.classForCoder())) {
            if((result as! UINavigationController).topViewController == nil) { return result }
            result = (result as! UINavigationController).topViewController!
        }
        
        return result
    }
    
    static func handleRemotePush(body: String, userInfo: [NSObject : AnyObject]) {
        let push = PushData.init(with: body, and: userInfo as! [String : AnyObject])
        self.handleRemotePush(push: push)
    }
    
    static func handleRemotePush(push: PushData) {
        let topNavigationController: UINavigationController = Router.getTopNavigationController()
        
        switch push.type {
        case .newLike:
            let postsController = setupPostsController(push: push)
            DispatchQueue.main.async {
                topNavigationController.pushViewController(postsController, animated: true)
            }
            break
        case .newComment:
            let postsController = setupPostsController(push: push)
            postsController.openCommentsAfterStart = true
            DispatchQueue.main.async {
                topNavigationController.pushViewController(postsController, animated: true)
            }
            break
        case .newFollower:
            let controller = setupFollowersController()
            DispatchQueue.main.async {
                topNavigationController.pushViewController(controller, animated: true)
            }
            break
        case .newTripsExtracted:
            let controller = UIHelpers.getProfileController(with: StoredUser.shared.user!)
            DispatchQueue.main.async {
                topNavigationController.pushViewController(controller!, animated: true)
            }
            break
        }
    }
    
    static func setupPostsController(push: PushData) -> PostsController {
        let tripStoryboard: UIStoryboard? = UIStoryboard(name: "Trip", bundle: nil)
        let postsController: PostsController! = tripStoryboard?.instantiateInitialViewController() as! PostsController
        
        postsController.tripId = push.tripId
        
        return postsController
    }
    
    static func setupFollowersController() -> FollowersController {
        let mainStoryboard: UIStoryboard? = UIStoryboard(name: "Main", bundle: nil)
        let followersController = mainStoryboard?.instantiateViewController(withIdentifier: "followers") as! FollowersController
        followersController.user = StoredUser.shared.user
        return followersController
    }
}
