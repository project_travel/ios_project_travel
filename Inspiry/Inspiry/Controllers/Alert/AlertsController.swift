//
//  AlertsController.swift
//  Inspiry
//
//  Created by Alexander Danilyak on 19/08/16.
//  Copyright © 2016 Inspiry. All rights reserved.
//

import Foundation
import UIKit
import AlamofireImage

enum AlertState {
    case activity
    case notification
    case error
    case bigNotification
    case closed
}

let closedOffset: CGFloat = -20.0
let normalOffset: CGFloat = 0.0

let bigClosedOffset: CGFloat = -64.0

class AlertsController: UIViewController {
    static let shared = AlertsController(nibName: "AlertsController", bundle: nil)
    
    var state: AlertState = .closed
    var timer: Timer?
    
    @IBOutlet weak var alertLabel: UILabel!
    @IBOutlet weak var alertActivityIndicator: UIActivityIndicatorView!
    
    @IBOutlet weak var topOffset: NSLayoutConstraint!
    @IBOutlet weak var containerView: UIView!
    
    var user: User?
    var trip: Trip?
    var notification: PushData?
    @IBOutlet weak var bigVisualView: UIVisualEffectView!
    @IBOutlet weak var bigContainerView: UIView!
    @IBOutlet weak var bigTopOffset: NSLayoutConstraint!
    @IBOutlet weak var profileImageView: UIImageView!
    @IBOutlet weak var bigNotificationTextLabel: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.alertActivityIndicator.transform = CGAffineTransform.init(scaleX: 0.5, y: 0.5)
        self.view.backgroundColor = UIColor.clear
        
        self.profileImageView.layer.cornerRadius = 11.0
        self.profileImageView.layer.masksToBounds = true
        
        self.profileImageView.isUserInteractionEnabled = false
        self.bigNotificationTextLabel.isUserInteractionEnabled = false
        self.bigVisualView.isUserInteractionEnabled = true
        self.bigContainerView.isUserInteractionEnabled = true
        
        self.setState(state: .closed)
        self.view.layoutIfNeeded()
    }
    
    func setState(state: AlertState) {
        self.state = state
        self.setOffsets()
    }
    
    private func setOffsets() {
        if(self.state == .closed) {
            self.topOffset.constant = closedOffset
            self.bigTopOffset.constant = bigClosedOffset
        } else if(self.state == .bigNotification) {
            self.topOffset.constant = closedOffset
            self.bigTopOffset.constant = normalOffset
        } else {
            self.topOffset.constant = normalOffset
            self.bigTopOffset.constant = bigClosedOffset
        }
    }
    
    // MARK: Loaders / Errors / Etc.
    
    func showLoading(text: String) {
        self.setState(state: .activity)
        self.alertActivityIndicator.isHidden = false
        self.alertActivityIndicator.startAnimating()
        self.alertLabel.text = text
        self.containerView.backgroundColor = UIColor.clear
        UIView.animate(withDuration: 0.2, delay: 0, options: .curveEaseIn, animations: {
            self.alertLabel.textColor = UIColor.darkGray
            self.view.layoutIfNeeded()
            }, completion: nil)
    }
    
    func showError(text: String, isAutoClosable: Bool) {
        self.setState(state: .error)
        self.alertActivityIndicator.isHidden = true
        self.alertLabel.text = text
        UIView.animate(withDuration: 0.3, delay: 0, options: .curveEaseIn, animations: {
            self.containerView.backgroundColor = UIHelpers.destructiveColor()
            self.alertLabel.textColor = UIColor.white
            self.view.layoutIfNeeded()
            }, completion: { bool in
                if(isAutoClosable) {
                    self.timer = Timer.scheduledTimer(withTimeInterval: 3, repeats: false, block: { (timer) in
                        self.closeAlert()
                    })
                }
        })
    }
    
    func showNotification(text: String, isAutoClosable: Bool) {
        self.setState(state: .notification)
        self.alertActivityIndicator.isHidden = true
        self.alertLabel.text = text
        UIView.animate(withDuration: 0.3, delay: 0, options: .curveEaseIn, animations: {
            self.alertLabel.textColor = UIHelpers.darkGray()
            self.view.layoutIfNeeded()
            }, completion: { bool in
                if(isAutoClosable) {
                    self.timer = Timer.scheduledTimer(withTimeInterval: 3, repeats: false, block: { (timer) in
                        self.closeAlert()
                    })
                }
        })
    }
    
    // MARK: Push Notification
    
    private func loadAuthor(username: String) {
        UserManager().getUser(with: username, completion: authorLoadedCompletion)
    }
    
    private func authorLoadedCompletion(isSuccess: Bool, errorDescription: String, user: User?) -> Void {
        if(isSuccess) {
            self.user = user!
            if(self.user!.avatar.count > 0) {
                let url = PhotoSizeCollection.getBest(photos: self.user!.avatar)!.url
                self.loadImage(url: url)
            }
        } else {
            print("[ERROR] can't load user for push alert")
        }
    }
    
    private func loadTripCover(tripId: String) {
        TripManager().loadTrip(with: tripId, completion: tripLoadedCompletion)
    }
    
    private func tripLoadedCompletion(isSuccess: Bool, trip: Trip?, errorDescription: String) -> Void {
        if(isSuccess) {
            self.trip = trip!
            if(self.trip!.photos.count > 0) {
                let url = PhotoWrap.getBestQuality(photos: self.trip!.photos)!.url
                self.loadImage(url: url)
            }
        } else {
            print("[ERROR] can't load trip for push alert")
        }
    }
    
    private func loadImage(url: URL) {
        let request = URLRequest.init(url: url)
        UIHelpers.shared.loadImage(for: request, with: completion)
    }
    
    func completion(isSuccess: Bool, image: UIImage) -> Void {
        self.profileImageView.image = isSuccess ? image : #imageLiteral(resourceName: "profile")
    }
    
    private func setBigNotification() {
        self.bigNotificationTextLabel.text = self.notification!.body
        self.profileImageView.isHidden = false
        
        switch self.notification!.type {
        case .newComment:
            self.loadTripCover(tripId: self.notification!.tripId!)
            break
        case .newLike:
            self.loadTripCover(tripId: self.notification!.tripId!)
            break
        case .newFollower:
            self.loadAuthor(username: self.notification!.followerUsername!)
            break
        case .newTripsExtracted:
            NotificationCenter.default.post(name: tripsUpdatedNotification, object: nil)
            if(self.notification!.newIds!.count > 0) {
                self.loadTripCover(tripId: self.notification!.newIds!.first!)
            } else if(self.notification!.updatedIds!.count > 0) {
                self.loadTripCover(tripId: self.notification!.updatedIds!.first!)
            } else {
                self.profileImageView.isHidden = true
            }
            break
        }
    }
    
    func showBigNotification(body: String, userInfo: [NSObject: AnyObject], isAutoClosable: Bool) {
        self.setState(state: .bigNotification)
        
        self.notification = PushData.init(with: body, and: userInfo as! [String : AnyObject])
        self.setBigNotification()
        
        UIView.animate(withDuration: 0.3, delay: 0, options: .curveEaseIn, animations: {
            self.view.layoutIfNeeded()
            }, completion: { bool in
                if(isAutoClosable) {
                    self.timer = Timer.scheduledTimer(withTimeInterval: 3, repeats: false, block: { (timer) in
                        self.closeAlert()
                    })
                }
        })
    }
    
    @IBAction func onBigNotificationTap(_ sender: AnyObject) {
        UIHelpers.shared.closeAlert()
        if(self.notification == nil) { return }
        Router.handleRemotePush(push: self.notification!)
    }
    
    // MARK: Close
    
    func closeAlert() {
        self.setState(state: .closed)
        self.timer?.invalidate()
        self.timer = nil
        UIView.animate(withDuration: 0.3, delay: 0, options: .curveEaseIn, animations: {
            self.view.layoutIfNeeded()
            }, completion: nil)
    }
}
