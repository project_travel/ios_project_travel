//
//  MenuController.swift
//  Inspiry
//
//  Created by Alexander Danilyak on 26/07/16.
//  Copyright © 2016 Inspiry. All rights reserved.
//

import Foundation
import UIKit
import SideMenu
import MessageUI

class MenuController: UIViewController, UITableViewDelegate, UITableViewDataSource, MFMailComposeViewControllerDelegate {
    @IBOutlet weak var tableView: UITableView!
    
    static var currentMenuIndex: Int = 1
    @IBOutlet weak var knowBetterLabel: UILabel!
    
    let menuCellNames: [String] = ["menuTitleSearch", "menuTitleProfile", "menuTitleFollowing", "menuTitleFollowers", "menuTitleNotifications", "menuTitleSettings"]
    
    let menuControllerToLoad: [String] = ["search", "profile", "following", "followers", "notifications", "settings"]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Trick to hide separator lines of not used cells
        self.tableView.tableFooterView = UIView()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.navigationBar.isHidden = true
        self.tableView.reloadData()
        
        // Offset
        let offsetY = UIScreen.main.bounds.size.height / 6
        self.tableView.contentInset = UIEdgeInsetsMake(offsetY, 0, 0, 0)
        
        // Know Better
        let text = Localize(string: "menuKnowBetter")
        let range = (text as NSString).range(of: "support@inspiry.me")
        
        let attributedString = NSMutableAttributedString(string:text)
        attributedString.addAttribute(NSForegroundColorAttributeName, value: UIHelpers.green() , range: range)
        attributedString.addAttribute(NSFontAttributeName, value: UIHelpers.menuCellFont(), range: NSRange.init(location: 0, length: attributedString.length))
        self.knowBetterLabel.attributedText = attributedString
        
        self.knowBetterLabel.isUserInteractionEnabled = true
        let onKnowBetterTap = UITapGestureRecognizer.init(target: self, action: #selector(onKnowBetter))
        self.knowBetterLabel.addGestureRecognizer(onKnowBetterTap)
        self.knowBetterLabel.isHidden = !MFMailComposeViewController.canSendMail()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(true)
        Analytics.shared.logScreen(name: screenMenu)
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        self.navigationController?.navigationBar.isHidden = false
    }
    
    // MARK: Table View
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1;
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return menuCellNames.count;
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell: MenuCell = tableView.dequeueReusableCell(withIdentifier: "menuCell", for: indexPath) as! MenuCell
        
        cell.setup(itemName: Localize(string: menuCellNames[indexPath.row]), badgeCount: 0, isCurrent: MenuController.currentMenuIndex == indexPath.row)
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        
        MenuController.currentMenuIndex = indexPath.row
        
        if(menuControllerToLoad[indexPath.row] == "settings") {
            let settingsStoryboard: UIStoryboard? = UIStoryboard(name: "Settings", bundle: nil)
            let controller = settingsStoryboard?.instantiateInitialViewController()
            self.navigationController!.pushViewController(controller!, animated: false)
            return
        }
        
        let mainStoryboard: UIStoryboard? = UIStoryboard(name: "Main", bundle: nil)
        let controller = mainStoryboard?.instantiateViewController(withIdentifier: menuControllerToLoad[indexPath.row])
        
        if(menuControllerToLoad[indexPath.row] == "following" || menuControllerToLoad[indexPath.row] == "followers" || menuControllerToLoad[indexPath.row] == "notifications") {
            (controller as! FFNController).user = StoredUser.shared.user!
            (controller as! FFNController).isMenuButtonNeeded = true
        }
        
        self.navigationController!.pushViewController(controller!, animated: false)
    }
    
    // MARK: On Know Better
    
    func onKnowBetter() {
        let mailComposer: MFMailComposeViewController = MFMailComposeViewController.init()
        mailComposer.mailComposeDelegate = self
        mailComposer.setSubject("Inspiry Support")
        mailComposer.setToRecipients(["support@inspiry.me"])
        mailComposer.navigationBar.tintColor = UIHelpers.green()
        self.navigationController?.present(mailComposer, animated: true, completion: nil)
    }
    
    // MARK: MFMailComposeViewController
    
    func mailComposeController(_ controller: MFMailComposeViewController, didFinishWith result: MFMailComposeResult, error: Error?) {
        switch result {
        case .sent:
            UIHelpers.shared.showNotificationAlert(text: Localize(string: "alertThankYou"), isAutoClosable: true)
            break
        case .failed:
            UIHelpers.shared.showErrorAlert(text: Localize(string: "alertFailedToSend"), isAutoClosable: true)
            break
        default:
            break
        }
        controller.dismiss(animated: true, completion: nil)
    }
}
