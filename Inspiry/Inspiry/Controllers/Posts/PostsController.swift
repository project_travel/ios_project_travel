//
//  PostsController.swift
//  Inspiry
//
//  Created by Alexander Danilyak on 04/08/16.
//  Copyright © 2016 Inspiry. All rights reserved.
//

import Foundation
import UIKit

let tripUpdateNotification = Notification.Name("tripUpdate")

enum TripEditingType {
    case title
    case description
    case none
}

protocol TripTitleEditingProtocol {
    func titleEditingStarted()
}

protocol TripDescriptionEditingProtocol {
    func descriptionEditingStarted()
    func tableUpdates()
}

class PostsController: UIViewController, UITableViewDelegate, UITableViewDataSource, TripTitleEditingProtocol, TripDescriptionEditingProtocol {
    @IBOutlet weak var tableView: UITableView!
    
    var lastOffsetY: CGFloat = 0.0
    var toolBarHidden = false
    var toolBarOriginY: CGFloat = 0.0
    var openCommentsAfterStart = false
    
    var navBarTitle: TripNavTitleView?
    
    var editigType: TripEditingType = .none
    
    @IBOutlet weak var toolBar: UIVisualEffectView!
    
    @IBOutlet weak var likeImageView: UIImageView!
    @IBOutlet weak var likeLabel: UILabel!
    
    @IBOutlet weak var mapImageView: UIImageView!
    
    @IBOutlet weak var commentsImageView: UIImageView!
    @IBOutlet weak var commentsLabel: UILabel!
    
    var commentsButton: UIBarButtonItem!
    
    var tripId: String?
    var trip: Trip?
    var isMyTrip: Bool = false
    var model: PostsModel?
    var ads: [TripPartnersAd] = []
    
    // MARK: View Controller
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.tableView.estimatedRowHeight = 55.0
        self.tableView.rowHeight = UITableViewAutomaticDimension
        self.tableView.contentInset = UIEdgeInsetsMake(0, 0, 0, 0)
        
        self.navBarTitle = TripNavTitleView.init(frame: CGRect(x: 0, y: 0, width: 200, height: 40))
        self.navBarTitle?.delegate = self
        self.navigationItem.titleView = self.navBarTitle
        
        commentsButton = UIBarButtonItem(image: #imageLiteral(resourceName: "comments"), style: .plain, target: self, action: #selector(openComments))
        self.navigationItem.rightBarButtonItem = commentsButton
        
        updateTrip(with: tripId!)
        modelSetup()
        toolBarSetup()
        
        if(openCommentsAfterStart) {
            self.openComments()
            openCommentsAfterStart = false
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navBarTitle?.update(with: self.trip, isMyTrip: self.isMyTrip)
        self.toolBarSetupData()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(true)
        Analytics.shared.logScreen(name: screenPosts)
        NotificationCenter.default.addObserver(self, selector: #selector(onUpdate), name: tripUpdateNotification, object: nil)
    }
    
    // MARK: UI Setup
    
    func toolBarSetup() {
        mapImageView.isUserInteractionEnabled = model != nil
        mapImageView.alpha = model != nil ? 1.0 : 0.3
        self.toolBar.layer.cornerRadius = 13
        self.toolBar.layer.masksToBounds = true
        
        let onLikeTap = UITapGestureRecognizer.init(target: self, action: #selector(doLike))
        let onLikeCountTap = UITapGestureRecognizer.init(target: self, action: #selector(doLike))
        
        let onCommentsTap = UITapGestureRecognizer.init(target: self, action: #selector(openComments))
        let onCommentsArrowTap = UITapGestureRecognizer.init(target: self, action: #selector(openComments))
        
        let onMapTap = UITapGestureRecognizer.init(target: self, action: #selector(openMap))
        
        self.likeImageView.addGestureRecognizer(onLikeTap)
        self.likeLabel.addGestureRecognizer(onLikeCountTap)
        
        self.commentsLabel.addGestureRecognizer(onCommentsTap)
        self.commentsImageView.addGestureRecognizer(onCommentsArrowTap)
        
        self.mapImageView.addGestureRecognizer(onMapTap)
        
        self.likeImageView.isUserInteractionEnabled = true
        self.likeLabel.isUserInteractionEnabled = true
        self.commentsImageView.isUserInteractionEnabled = true
        self.commentsLabel.isUserInteractionEnabled = true
        self.mapImageView.isUserInteractionEnabled = true
    }
    
    // MARK: Trip Update Data
    
    func updateTrip(with id: String) {
        TripManager().loadTrip(with: id, completion: updateTripCompletion)
    }
    
    // MARK: Trip Update Data Completion
    
    func updateTripCompletion(isSuccess: Bool, trip: Trip?, errorDescription: String) -> Void {
        if(isSuccess) {
            if trip == nil { return }
            if(self.trip == nil) {
                self.trip = trip
                self.isMyTrip = trip!.owners.first == StoredUser.shared.user!.username
            } else {
                self.trip?.update(with: trip!)
            }
            
            self.navBarTitle?.update(with: self.trip, isMyTrip: self.isMyTrip)
            self.toolBarSetupData()
            self.tableView.reloadSections(IndexSet.init(integer: 0), with: .fade)
            
            TripPartnersAdsManager().loadTripPartnersAds(url: self.trip!.ad.absoluteString, completion: adsLoaded)
        } else {
            UIHelpers.shared.showErrorAlert(text: errorDescription, isAutoClosable: true)
        }
    }
    
    // MARK: Model
    
    func modelSetup() {
        model = PostsModel.init(tripId: tripId, pageSize: 30)
        model!.loadNextPage(completion: pageLoaded)
    }
    
    // MARK: Model Completions
    
    func pageLoaded(isSuccess: Bool, lastDisplayed: Int, newCount: Int, errorDescription: String) -> Void {
        if(isSuccess) {
            if newCount == 0 { return }
            
            var indexes = [IndexPath]()
            for i in lastDisplayed...(lastDisplayed+newCount-1) {
                indexes.append(IndexPath(row: i, section: 1))
            }
            
            self.tableView.insertRows(at: indexes, with: .none)
        } else {
            UIHelpers.shared.showErrorAlert(text: errorDescription, isAutoClosable: true)
        }
    }
    
    // MARK: Ads Completion
    
    func adsLoaded(isSuccess: Bool, ads: [TripPartnersAd]?, errorDescription: String) -> Void {
        if(isSuccess) {
            self.ads = ads!
            self.tableView.reloadSections(IndexSet.init(integer: 2), with: .fade)
        } else {
            UIHelpers.shared.showErrorAlert(text: errorDescription, isAutoClosable: true)
        }
    }
    
    // MARK: Table View
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 3
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // First two cells is static for all trips
        // 1 - Trip Description
        // 2 - Countries
        // 3 - Ads
        
        if(section == 0) {
            return trip == nil ? 0 : 2
        }
        
        if(section == 1) {
           return model == nil ? 0 : model!.allPosts.count
        }
        
        if(section == 2) {
            return ads.count
        }
        
        return 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if(indexPath.section == 0 && indexPath.row == 0) {
            let cell: TripDescriptionCell = tableView.dequeueReusableCell(withIdentifier: "tripDescCell", for: indexPath) as! TripDescriptionCell
            cell.update(with: self.trip!, isMyTrip: self.isMyTrip)
            cell.delegate = self
            return cell
        } else if(indexPath.section == 0 && indexPath.row == 1) {
            let cell: TripCountriesCell = tableView.dequeueReusableCell(withIdentifier: "tripCountriesCell", for: indexPath) as! TripCountriesCell
            cell.update(with: self.trip!)
            return cell
        } else if(indexPath.section == 1) {
            if(self.model!.allPosts[indexPath.row].photos.count == 0) {
                let cell: PostWithTextCell = tableView.dequeueReusableCell(withIdentifier: "postTextCell", for: indexPath) as! PostWithTextCell
                cell.updateWithPost(post: self.model!.allPosts[indexPath.row])
                return cell
            } else {
                let cell: PostWithPhotoCell = tableView.dequeueReusableCell(withIdentifier: "postPhotoCell", for: indexPath) as! PostWithPhotoCell
                cell.updateWithPost(post: self.model!.allPosts[indexPath.row])
                return cell
            }
        } else if(indexPath.section == 2) {
            let cell: TripPartnersAdCell = tableView.dequeueReusableCell(withIdentifier: "adCell", for: indexPath) as! TripPartnersAdCell
            cell.update(with: self.ads[indexPath.row], isSeparatorNeeded: indexPath.row == 0)
            return cell
        }
        
        // can't be here
        return UITableViewCell()
    }
    
    func scrollViewWillBeginDragging(_ scrollView: UIScrollView) {
        lastOffsetY = scrollView.contentOffset.y
        
        self.onCancel()
    }
    
    func scrollViewWillBeginDecelerating(_ scrollView: UIScrollView) {
        let hide = scrollView.contentOffset.y > self.lastOffsetY
        self.showToolBarIfNeeded(isNeedToShow: hide)
    }
    
    func showToolBarIfNeeded(isNeedToShow: Bool) {
        UIView.animate(withDuration: 0.3) {
            if(self.toolBarHidden != isNeedToShow) {
                let const: CGFloat = isNeedToShow ? 46.0 : -46.0
                self.toolBar.frame.origin.y += const
                self.toolBarOriginY = self.toolBar.frame.origin.y
                self.toolBarHidden = isNeedToShow
            }
        }
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        if(indexPath.row == (model!.allPosts.count - 1) && model!.stop != true) {
            model!.loadNextPage(completion: pageLoaded)
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        if(indexPath.section != 2) { return }
        
        let cell: TripPartnersAdCell = tableView.cellForRow(at: indexPath) as! TripPartnersAdCell
        cell.onAdTouched()
    }
    
    // MARK: Buttons Setup
    
    func toolBarSetupData() {
        if trip == nil { return }
        self.likeLabel.text = String(self.trip!.likes)
        self.likeImageView.image = self.trip!.isLikedByMe ? #imageLiteral(resourceName: "likeOn") : #imageLiteral(resourceName: "likeOff")
        
        self.commentsLabel.text = Localize(string: "commentsButtonTitle")
    }
    
    // MARK: Button Actions
    
    func doLike() {
        if(trip == nil) { return }
        
        self.trip!.isLikedByMe = !trip!.isLikedByMe
        self.trip!.likes = trip?.isLikedByMe ==  true ? trip!.likes + 1 : trip!.likes - 1
        
        TripManager().setLike(with: trip!.isLikedByMe, and: trip!.id) { (isSuccess, errorDescription) in
            if(!isSuccess) {
                self.trip!.isLikedByMe = !self.trip!.isLikedByMe
                self.trip!.likes = self.trip!.isLikedByMe ==  true ? self.trip!.likes + 1 : self.trip!.likes - 1
                UIHelpers.shared.showErrorAlert(text: errorDescription, isAutoClosable: true)
                self.toolBarSetupData()
            }
        }
        
        self.toolBarSetupData()
    }
    
    func openComments() {
        let tripStoryboard: UIStoryboard? = UIStoryboard(name: "Trip", bundle: nil)
        let navCommentsController: UINavigationController = tripStoryboard?.instantiateViewController(withIdentifier: "navComments") as! UINavigationController
        
        (navCommentsController.viewControllers[0] as! CommentsController).tripId = tripId
        
        self.present(navCommentsController, animated: true, completion: nil)
    }
    
    func openMap() {
        let tripStoryboard: UIStoryboard? = UIStoryboard(name: "Trip", bundle: nil)
        let navCommentsController: UINavigationController = tripStoryboard?.instantiateViewController(withIdentifier: "navMap") as! UINavigationController
        
        (navCommentsController.viewControllers[0] as! MapController).posts = model?.allPosts
        
        self.present(navCommentsController, animated: true, completion: nil)
    }

    // MARK: Trip Title Editing Protocol
    
    func titleEditingStarted() {
        self.editigType = .title
        self.initButtons()
    }
    
    // MARK: Trip Description Editing Protocol
    
    func descriptionEditingStarted() {
        self.editigType = .description
        self.initButtons()
    }
    
    func tableUpdates() {
        self.tableView.beginUpdates()
        self.tableView.endUpdates()
    }
    
    // MARK: Buttons
    
    func initButtons() {
        let saveButton: UIBarButtonItem = UIBarButtonItem.init(title: Localize(string: "tripSave"), style: .plain, target: self, action: #selector(onSave))
        saveButton.tintColor = UIHelpers.green()
        let cancelButton: UIBarButtonItem = UIBarButtonItem.init(title: Localize(string: "tripCancel"), style: .plain, target: self, action: #selector(onCancel))
        cancelButton.tintColor = UIHelpers.destructiveColor()
        
        self.navigationItem.leftBarButtonItem = cancelButton
        self.navigationItem.rightBarButtonItem = saveButton
    }
    
    func onSave() {
        switch self.editigType {
        case .title:
            self.navBarTitle?.saveTitle()
            break
        case .description:
            (self.tableView.cellForRow(at: IndexPath(row: 0, section: 0)) as! TripDescriptionCell).saveDescription()
            break
        default:
            break
        }
        self.editigType = .none
        
        self.navigationItem.leftBarButtonItem = nil
        self.navigationItem.rightBarButtonItem = commentsButton
    }
    
    func onCancel() {
        switch self.editigType {
        case .title:
            self.navBarTitle?.cancel()
            break
        case .description:
            (self.tableView.cellForRow(at: IndexPath(row: 0, section: 0)) as! TripDescriptionCell).cancel()
            break
        default:
            break
        }
        self.editigType = .none
        
        self.navigationItem.leftBarButtonItem = nil
        self.navigationItem.rightBarButtonItem = commentsButton
    }
    
    func onUpdate() {
        self.model!.reset()
        self.tableView.reloadData()
        self.model!.loadNextPage(completion: pageLoaded)
    }
}
