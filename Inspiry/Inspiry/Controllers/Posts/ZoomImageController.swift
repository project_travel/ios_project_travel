//
//  ZoomImageController.swift
//  Inspiry
//
//  Created by Alexander Danilyak on 08/09/16.
//  Copyright © 2016 Inspiry. All rights reserved.
//

import Foundation
import UIKit

class ZoomImageController: UIViewController, UIScrollViewDelegate {
    
    @IBOutlet weak var closeButton: UIButton!
    @IBOutlet weak var textView: UITextView!
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var socialImageView: UIImageView!
    @IBOutlet weak var postContainerView: UIView!
    
    var post: Post?
    var image: UIImage? = nil
    
    var scrollView: UIScrollView!
    var imageView: UIImageView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let ratio: CGFloat = self.image!.size.height / self.image!.size.width
        let width = UIScreen.main.bounds.width
        imageView = UIImageView.init(frame: CGRect(x: 0, y: 0, width: width, height: width*ratio))
        imageView.image = image!
        
        scrollView = UIScrollView(frame: view.bounds)
        scrollView.backgroundColor = UIColor.black
        scrollView.contentSize = imageView.bounds.size
        scrollView.autoresizingMask = [.flexibleHeight, .flexibleWidth]
        
        scrollView.addSubview(imageView)
        view.addSubview(scrollView)
        view.sendSubview(toBack: scrollView)
        
        scrollView.delegate = self
        
        setZoomScale()
        setPaddings()
        
        setData()
        
        setupGestureRecognizer()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(true)
        Analytics.shared.logScreen(name: screenZoomedImage)
    }
    
    override var prefersStatusBarHidden: Bool {
        get {
            return true
        }  
    }
    
    override var preferredStatusBarUpdateAnimation: UIStatusBarAnimation {
        return .slide
    }
    
    // MARK: Data
    
    private func setData() {
        if post == nil {
            self.postContainerView.isHidden = true
            return
        }
        
        self.textView.text = self.post!.text
        self.textView.isEditable = false
        self.textView.tintColor = UIHelpers.green()
        self.dateLabel.text = postDateLabel(date: post!.created)
        
        self.socialImageView.image = UIImage.init(named: self.post!.source.type.rawValue + "Icon")
        
        let tapOnScoialImage = UITapGestureRecognizer.init(target: self, action: #selector(onSocial))
        self.socialImageView.addGestureRecognizer(tapOnScoialImage)
        self.socialImageView.isUserInteractionEnabled = true
        let tapOnScoialName = UITapGestureRecognizer.init(target: self, action: #selector(onSocial))
        self.dateLabel.addGestureRecognizer(tapOnScoialName)
        self.dateLabel.isUserInteractionEnabled = true
    }
    
    func onSocial() {
        if self.post == nil { return }
        UIApplication.shared.open(self.post!.source.url, options: [:], completionHandler: nil)
    }
    
    // MARK: Zooming
    
    func viewForZooming(in scrollView: UIScrollView) -> UIView? {
        return imageView
    }
    
    override func viewWillLayoutSubviews() {
        setZoomScale()
    }
    
    func scrollViewDidZoom(_ scrollView: UIScrollView) {
        self.setPaddings()
    }
    
    func setPaddings() {
        let imageViewSize = imageView.frame.size
        let scrollViewSize = scrollView.bounds.size
        
        let verticalPadding = imageViewSize.height < scrollViewSize.height ? (scrollViewSize.height - imageViewSize.height) / 2 : 0
        let horizontalPadding = imageViewSize.width < scrollViewSize.width ? (scrollViewSize.width - imageViewSize.width) / 2 : 0
        
        scrollView.contentInset = UIEdgeInsets(top: verticalPadding, left: horizontalPadding, bottom: verticalPadding, right: horizontalPadding)
    }
    
    func setZoomScale() {
        scrollView.minimumZoomScale = 1.0
        scrollView.maximumZoomScale = 4.0
        scrollView.zoomScale = 1.0
    }
    
    func setupGestureRecognizer() {
        let doubleTap = UITapGestureRecognizer(target: self, action: #selector(handleDoubleTap(recognizer:)))
        doubleTap.numberOfTapsRequired = 2
        scrollView.addGestureRecognizer(doubleTap)
    }
    
    func handleDoubleTap(recognizer: UITapGestureRecognizer) {
        
        if (scrollView.zoomScale > scrollView.minimumZoomScale) {
            scrollView.setZoomScale(scrollView.minimumZoomScale, animated: true)
        } else {
            scrollView.setZoomScale(scrollView.maximumZoomScale, animated: true)
        }
    }
    
    // MARK: Buttons
    @IBAction func onClose(_ sender: AnyObject) {
        self.dismiss(animated: true, completion: nil)
    }
}
