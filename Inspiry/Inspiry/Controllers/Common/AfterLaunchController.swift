//
//  AfterLaunchController.swift
//  Inspiry
//
//  Created by Alexander Danilyak on 06/11/2016.
//  Copyright © 2016 Inspiry. All rights reserved.
//

import UIKit

class AfterLaunchController: UIViewController {
    @IBOutlet weak var launchImageView: UIImageView!
    @IBOutlet weak var activityIndicatorView: UIActivityIndicatorView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        navigationController?.setNavigationBarHidden(true, animated: false)
        activityIndicatorView.startAnimating()
        updateToken()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
    }
    
    func updateToken() {
        AuthManager().loginWithToken(tokenKey: inspiryRefreshTokenKey, completion: { isSuccess, errorDescription in
            if(isSuccess) {
                UserAuth.shared.state = .hasBothTokens
                print("[AT] " + UserDefaults.standard.getAccessToken()!)
                self.pushProfileController()
            } else {
                print("[LOGIN FAILED] " + errorDescription!)
            }
        })
    }
    
    func pushProfileController() {
        let profileController = UIHelpers.getProfileController(with: StoredUser.shared.user!, and: true)
        self.navigationController?.setNavigationBarHidden(false, animated: true)
        self.navigationController?.pushViewController(profileController!, animated: true)
    }
}
