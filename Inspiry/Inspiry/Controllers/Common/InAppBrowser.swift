//
//  InAppBrowser.swift
//  Inspiry
//
//  Created by Alexander Danilyak on 22/10/2016.
//  Copyright © 2016 Inspiry. All rights reserved.
//

import UIKit

class InAppBrowser: UIViewController {
    var url: URL!
    @IBOutlet weak var webView: UIWebView!
    
    @IBOutlet weak var closeButton: UIBarButtonItem!
    @IBOutlet weak var openSafariButton: UIBarButtonItem!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        let request = URLRequest(url: url)
        webView.loadRequest(request)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        title = Localize(string: "browserTitle")
        closeButton.title = Localize(string: "browserClose")
        openSafariButton.title = Localize(string: "browserOpenSafari")
    }
    
    @IBAction func onClose(_ sender: AnyObject) {
        dismiss(animated: true, completion: nil)
    }
    
    @IBAction func onOpenSafari(_ sender: AnyObject) {
        UIApplication.shared.open(url, options: [:], completionHandler: nil)
    }
}
