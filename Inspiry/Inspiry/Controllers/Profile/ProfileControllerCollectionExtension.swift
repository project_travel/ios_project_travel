//
//  ProfileControllerCollectionExtension.swift
//  Inspiry
//
//  Created by Alexander Danilyak on 02/08/16.
//  Copyright © 2016 Inspiry. All rights reserved.
//

import Foundation
import UIKit

let tripsUpdatedNotification: Notification.Name = Notification.Name("tripsUpdatedNotification")

extension ProfileController: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout, UIScrollViewDelegate {
    
    func collectionViewSetup() {
        let tripBigCellNib = UINib(nibName: "TripBigCell", bundle:nil)
        self.collectionView.register(tripBigCellNib, forCellWithReuseIdentifier: "tripBigCell")
        
        let tripSmallCellNib = UINib(nibName: "TripSmallCell", bundle:nil)
        self.collectionView.register(tripSmallCellNib, forCellWithReuseIdentifier: "tripSmallCell")
        
        self.updateInsets()
        
        self.collectionView.scrollsToTop = true
        
        if isMyProfile {
            NotificationCenter.default.addObserver(self, selector: #selector(reloadTrips), name: tripsUpdatedNotification, object: nil)
        }
        
        addRefresh()
        
        isCollectionReady = true
    }
    
    func addRefresh() {
        refresh = UIRefreshControl()
        refresh.tintColor = UIHelpers.green(alpha: 0.8)
        refresh.addTarget(self, action: #selector(reloadTrips), for: UIControlEvents.valueChanged)
        collectionView.addSubview(refresh)
        collectionView.alwaysBounceVertical = true
    }
    
    func updateInsets() {
        if(self.layoutType == .list) {
            self.flowLayout.sectionInset = UIEdgeInsets.init(top: 10, left: 0, bottom: 10, right: 0)
        } else {
            self.flowLayout.sectionInset = UIEdgeInsets.init(top: 10, left: 15, bottom: 10, right: 15)
        }
    }
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return model == nil ? 0 : model!.allTrips.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if(self.layoutType == .list) {
            let tripCell = collectionView.dequeueReusableCell(withReuseIdentifier: "tripBigCell", for: indexPath) as! TripBigCell
            tripCell.setAll(with: model!.allTrips[indexPath.row])
            return tripCell
        } else {
            let tripCell = collectionView.dequeueReusableCell(withReuseIdentifier: "tripSmallCell", for: indexPath) as! TripSmallCell
            tripCell.setAll(with: model!.allTrips[indexPath.row])
            return tripCell
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        if(indexPath.row == (model!.allTrips.count - 1) && model!.stop != true) {
            model!.loadNextPage(completion: pageLoaded)
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
        let view = collectionView.dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: "header", for: indexPath)
        if userProfileView != nil { view.addSubview(userProfileView!) }
        view.clipsToBounds = true
        view.frame.size.width = UIScreen.main.bounds.width
        
        return view
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        collectionView.deselectItem(at: indexPath, animated: true)
        self.view.isUserInteractionEnabled = false
        
        let tripStoryboard: UIStoryboard? = UIStoryboard(name: "Trip", bundle: nil)
        let postsController: PostsController! = tripStoryboard?.instantiateInitialViewController() as! PostsController
        
        postsController.tripId = model!.allTrips[indexPath.row].id
        postsController.trip = model!.allTrips[indexPath.row]
        postsController.isMyTrip = self.isMyProfile
        self.navigationController?.pushViewController(postsController, animated: true)
        self.view.isUserInteractionEnabled = true
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        if(self.layoutType == .list) {
            let width: CGFloat = UIScreen.main.bounds.width
            let height: CGFloat = width * 435.0/375.0
            return CGSize.init(width: width, height: height)
        } else {
            let width: CGFloat = (UIScreen.main.bounds.width - 30) / 3
            let height: CGFloat = width
            return CGSize.init(width: width, height: height)
        }
    }
    
    //MARK: More Button
    
    func addToBlackListCompletion(isSuccess: Bool, errorDescription: String) -> Void {
        if isSuccess {
            Router.getTopNavigationController().popViewController(animated: true)
            
            let alert = UIAlertController(title: Localize(string: "blackListCompletionTitle"),
                                          message: Localize(string: "blackListCompletionMessage"),
                                          preferredStyle: .actionSheet)
            let cancel = UIAlertAction(title: Localize(string: "ok"), style: .cancel, handler: nil)
            alert.addAction(cancel)
            Router.getTopNavigationController().present(alert, animated: true, completion: nil)
        } else {
            UIHelpers.shared.showErrorAlert(text: errorDescription, isAutoClosable: true)
        }
    }
    
    //MARK: Scroll
    
    func scrollViewShouldScrollToTop(_ scrollView: UIScrollView) -> Bool {
        return scrollView == self.collectionView
    }
    
    //MARK: Reload
    
    func reloadTrips() {
        isUpdateAll = true
        model!.reset()
        collectionView.reloadData()
        model!.loadNextPage(completion: pageLoaded)
    }
}
