//
//  ProfileController.swift
//  Inspiry
//
//  Created by Alexander Danilyak on 19/07/16.
//  Copyright © 2016 Inspiry. All rights reserved.
//

import Foundation
import UIKit
import SideMenu

class ProfileController: UIViewController, UIImagePickerControllerDelegate, UINavigationControllerDelegate, LayoutSwitchProtocol {
    
    var isMenuButtonNeeded: Bool = false
    var isMyProfile: Bool = false
    var user: User?
    var model: UserTripsModel?
    
    var userProfileView: UserProfileView? = nil
    var menuButton: UIBarButtonItem? = nil
    
    var isCollectionReady: Bool = false
    var isUpdateAll: Bool = false
    
    var layoutType: LayoutType = .list
    
    var refresh: UIRefreshControl!
    
    @IBOutlet weak var flowLayout: UICollectionViewFlowLayout!
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var noContentLabel: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationItem.title = Localize(string: "profile")
        
        getProfile()
        menuSetup()
        menuButtonInit()
        profileViewSetup()
        setupRightButton()
        
        self.noContentLabel.text = Localize(string: "noContent")
        
        if(self.user != nil) {
            modelSetup()
            collectionViewSetup()
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        getProfile()
        if(self.user != nil) {
            self.collectionView.reloadData()
            self.updateProfileUI()
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        if(self.user != nil) {
            self.updateProfileUI()
            Analytics.shared.logScreen(name: screenProfile, parameters: [profileUsername: self.user!.username])
        }
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
    
    // MARK: Model
    
    func modelSetup() {
        model = UserTripsModel(userName: user!.username, pageSize: 20)
        model!.loadNextPage(completion: pageLoaded)
    }
    
    func updateAll() {
        model!.reload(completion: pageLoaded)
        isUpdateAll = true
    }
    
    // MARK: Right button setup
    
    func setupRightButton() {
        if(isMyProfile) {
            let rightButton = self.createReloadButton()
            self.navigationItem.rightBarButtonItem = rightButton
        } else {
            let rightButton = self.createFollowButton()
            self.navigationItem.rightBarButtonItem = rightButton
        }
    }
    
    func createReloadButton() -> UIBarButtonItem {
        let reloadButton = UIBarButtonItem.init(image: UIImage.init(named: "reload"), style: .plain, target: self, action: #selector(onReload))
        reloadButton.isEnabled = true
        return reloadButton
    }
    
    func createFollowButton() -> UIBarButtonItem {
        let isFollow: Bool = self.user!.hasFollowed
        let text = isFollow ? Localize(string: "followingButton") : Localize(string: "followButton")
        let followButton = UIBarButtonItem.init(title: text, style: .plain, target: self, action: #selector(doFollow))
        return followButton
    }
    
    // MARK: Do reload
    
    func onReload() {
        StoredUserManager().reloadTrips { (isSuccess, errorDescription) in
            if(isSuccess) {
                UIHelpers.shared.showNotificationAlert(text: Localize(string: "reloadNotification"), isAutoClosable: true)
            } else {
                UIHelpers.shared.showErrorAlert(text: errorDescription, isAutoClosable: true)
            }
        }
    }
    
    // MARK: Model Completions
    
    func pageLoaded(isSuccess: Bool, lastDisplayed: Int, newCount: Int, errorDescription: String) -> Void {
        self.noContentLabel.isHidden = !(lastDisplayed == 0 && newCount == 0)
        
        refresh.endRefreshing()
        
        if(isSuccess) {
            if newCount == 0 { return }
            
            if(isUpdateAll) {
                collectionView.reloadData()
                isUpdateAll = false
            } else {
                var indexes = [IndexPath]()
                for i in lastDisplayed...(lastDisplayed+newCount-1) {
                    indexes.append(IndexPath(row: i, section: 0))
                }
            
                self.collectionView.insertItems(at: indexes)
            }
        } else {
            UIHelpers.shared.showErrorAlert(text: errorDescription, isAutoClosable: true)
        }
    }
    
    func profileViewSetup() {
        if (userProfileView == nil) {
            userProfileView = UserProfileView.init(frame: CGRect(x: 0, y: 0, width: self.view.frame.width, height: 357))
        }
        
        userProfileView!.edit.addTarget(self, action: #selector(doEdit), for: .touchUpInside)
        userProfileView!.more.addTarget(self, action: #selector(doMore), for: .touchUpInside)
        userProfileView!.delegate = self
        userProfileView!.isUserInteractionEnabled = true
        
        if(self.user != nil) {
            self.userProfileView!.updateWith(user: self.user, isStored: self.isMyProfile)
        }
    }
    
    // MARK: User Profile View
    
    func getProfile() {
        if(isMyProfile) {
            StoredUserManager().getLoggedUser(completion: updateProfile)
        } else {
            UserManager().getUser(with: self.user!.username!, completion: updateProfile)
        }
    }
    
    func updateProfile(isSuccess: Bool, message: String, user: User?) -> Void {
        if(isSuccess) {
            self.user = user
            self.updateProfileUI()
            self.userProfileView?.updateWith(user: user, isStored: self.isMyProfile)
            self.setupRightButton()
            
            if(!isCollectionReady) {
                modelSetup()
                collectionViewSetup()
            }
            
        } else {
            UIHelpers.shared.showErrorAlert(text: message, isAutoClosable: true)
        }
    }
    
    func updateProfileUI() {
        self.navigationItem.title = "@" + self.user!.username!
        self.userProfileView!.updateWith(user: self.user, isStored: self.isMyProfile)
        NotificationCenter.default.post(name: reloadNotification, object: nil)
    }
    
    // MARK: User Profile Actions
    
    func doEdit() {
        let editStoryboard: UIStoryboard? = UIStoryboard(name: "ProfileEdit", bundle: nil)
        let editController: UINavigationController! = editStoryboard?.instantiateInitialViewController() as! UINavigationController
        
        self.present(editController, animated: true, completion: nil)
    }
    
    func doMore() {
        UIHelpers.onMore(moreType: .profile, data: self.user!.username, completion: addToBlackListCompletion)
    }
    
    func doFollow() {
        let isFollow: Bool = self.user!.hasFollowed
        
        self.navigationItem.rightBarButtonItem?.isEnabled = false
        
        if(isFollow) {
            unfollowAlert()
        } else {
            UserManager().followUser(with: self.user!.username!, completion: doFollowCompletion)
        }
    }
    
    func unfollowAlert() {
        let title = String.init(format: Localize(string: "unfollowTitle"), self.user!.username!)
        let message = String.init(format: Localize(string: "unfollowDescription"), self.user!.username!)
        let alert = UIAlertController(title: title, message: message, preferredStyle: .actionSheet)
        let unfollow = UIAlertAction(title: Localize(string: "unfollowUnfollow"), style: .destructive, handler: {
            action in
            UserManager().unfollowUser(with: self.user!.username!, completion: self.doUnfollowCompletion)
        })
        let cancel = UIAlertAction(title: Localize(string: "unfollowCancel"), style: .cancel, handler: {
            action in
            let rightButton = self.createFollowButton()
            self.navigationItem.rightBarButtonItem = rightButton
        })
        alert.addAction(unfollow)
        alert.addAction(cancel)
        self.present(alert, animated: true, completion: nil)
    }
    
    func doFollowCompletion(isSuccess: Bool, isPending: Bool, message: String) {
        self.navigationItem.rightBarButtonItem?.isEnabled = true
        if(isSuccess) {
            self.user!.hasFollowed = !self.user!.hasFollowed
            let rightButton = self.createFollowButton()
            self.navigationItem.rightBarButtonItem = rightButton
            getProfile()
        } else {
            UIHelpers.shared.showErrorAlert(text: message, isAutoClosable: true)
        }
    }
    
    func doUnfollowCompletion(isSuccess: Bool, message: String) {
        if(isSuccess) {
            self.navigationItem.rightBarButtonItem?.isEnabled = true
            self.user!.hasFollowed = !self.user!.hasFollowed
            let rightButton = self.createFollowButton()
            self.navigationItem.rightBarButtonItem = rightButton
            getProfile()
        } else {
            UIHelpers.shared.showErrorAlert(text: message, isAutoClosable: true)
        }
    }
    
    // MARK: Menu Setup
    
    func menuButtonInit() {
        if isMenuButtonNeeded {
            menuButton = UIBarButtonItem.init(image: UIImage.init(named: "menu"), style: .plain, target: self, action:#selector(openMenu))
            self.navigationItem.leftBarButtonItem = menuButton
        }
    }
    
    func openMenu() {
        self.present(SideMenuManager.menuLeftNavigationController!, animated: true, completion: nil)
    }
    
    func menuSetup() {
        if !UIHelpers.isMenuSetupComplete {
            let mainStoryboard: UIStoryboard? = UIStoryboard(name: "Main", bundle: nil)
            _ = SideMenuManager.menuLeftNavigationController = mainStoryboard?.instantiateViewController(withIdentifier: "sideMenu") as? UISideMenuNavigationController
            SideMenuManager.menuPresentMode = .viewSlideOut
            SideMenuManager.menuAnimationBackgroundColor = UIColor.white
            SideMenuManager.menuFadeStatusBar = false
            _ = SideMenuManager.menuAddPanGestureToPresent(self.navigationController!.navigationBar)
            _ = SideMenuManager.menuAddScreenEdgePanGesturesToPresent(self.navigationController!.view)
            SideMenuManager.menuAllowPushOfSameClassTwice = false
            SideMenuManager.menuAllowPopIfPossible = true
            SideMenuManager.menuAnimationTransformScaleFactor = 0.9
            SideMenuManager.menuAnimationPresentDuration = 0.25
            SideMenuManager.menuAnimationDismissDuration = 0.25
            UIHelpers.isMenuSetupComplete = true
        }
    }
    
    // MARK: Layout Switch
    
    func switchTo(type: LayoutType) {
        self.layoutType = type
        self.updateInsets()
        self.collectionView.reloadData()
    }
}
