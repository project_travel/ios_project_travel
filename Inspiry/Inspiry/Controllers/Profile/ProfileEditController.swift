//
//  ProfileEditController.swift
//  Inspiry
//
//  Created by Alexander Danilyak on 20/07/16.
//  Copyright © 2016 Inspiry. All rights reserved.
//

import Foundation
import UIKit

protocol EditSaveButtonProtocol {
    func setSaveButtonEnabled(isEnabled: Bool)
}

class ProfileEditController: UIViewController, UITableViewDelegate, UITableViewDataSource, UITextFieldDelegate, UITextViewDelegate, EditSaveButtonProtocol {
    @IBOutlet weak var cancelButton: UIBarButtonItem!
    @IBOutlet weak var saveButton: UIBarButtonItem!
    @IBOutlet weak var tableView: UITableView!
    
    var statusTextView: UITextView?
    
    var firstnameTextField: UITextField?
    var surnameTextField: UITextField?
    var sexTextField: UITextField?
    var birthdateTextField: UITextField?
    
    @IBOutlet weak var tableToTop: NSLayoutConstraint!
    var keyboardShown: Bool = false
    var keyboardOffset: CGFloat = 0.0
    
    let user: User! = StoredUser.shared.user!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.tableView.estimatedRowHeight = 64.0
        self.tableView.rowHeight = UITableViewAutomaticDimension
        self.tableView.contentInset = UIEdgeInsetsMake(8, 0, 0, 0)
        
        self.navigationItem.title = Localize(string: "editProfile")
        saveButton.title = Localize(string: "editSave")
        cancelButton.title = Localize(string: "editCancel")
        
        saveButton.isEnabled = false
        
        self.setUpKeyboard()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(true)
        Analytics.shared.logScreen(name: screenEditProfile)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.view.endEditing(true)
    }
    
    func createParametersToPass() -> [String: String] {
        var parameters: [String: String] = [:]
        
        parameters[apiKeyFirstname] = self.firstnameTextField?.text
        parameters[apiKeySurname] = self.surnameTextField?.text
        parameters[apiKeyGender] = self.sexTextField?.text == Localize(string: "not set") ? "" : self.sexTextField?.text
        parameters[apiKeyBirthdate] = self.birthdateTextField?.text == Localize(string: "not set") ? "" : Date.formatter.simple.date(from: (self.birthdateTextField?.text)!)?.rfc3339

        parameters[apiKeyStatus] = self.statusTextView?.text
        
        return parameters
    }
    
    func saveRoutine() {
        UIHelpers.shared.showLoadingAlert(text: Localize(string: "save"))
        
        let parameters = self.createParametersToPass()
        
        StoredUserManager().update(parameters: parameters as [String : AnyObject]?) { isSuccess, message in
            UIHelpers.shared.closeAlert()
            self.view.isUserInteractionEnabled = true
            if(isSuccess) {
                StoredUser.shared.user?.updateWith(dictionary: parameters)
                StoredUser.shared.save()
                self.dismiss(animated: true, completion: nil)
            } else {
                UIHelpers.shared.showErrorAlert(text: message, isAutoClosable: true)
            }
        }
    }
    
    // MARK: Table View
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 6
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        switch indexPath.row {
        case 0:
            let cell: ProfileEditImagesCell = tableView.dequeueReusableCell(withIdentifier: "editImagesCell", for: indexPath) as! ProfileEditImagesCell
            cell.delegate = self
            cell.setup()
            return cell
        case 1:
            let cell: ProfileEditStatusCell = tableView.dequeueReusableCell(withIdentifier: "editStatusCell", for: indexPath) as! ProfileEditStatusCell
            cell.setup()
            self.statusTextView = cell.statusTextView
            self.statusTextView?.isScrollEnabled = false
            self.statusTextView?.delegate = self
            self.statusTextView?.contentInset = UIEdgeInsetsMake(-6, -6, -6, -6)
            return cell
        case 2:
            let cell: ProfileEditCell = tableView.dequeueReusableCell(withIdentifier: "editCell", for: indexPath) as! ProfileEditCell
            cell.setup(type: .firstname)
            self.firstnameTextField = cell.editItemTextField
            self.firstnameTextField?.delegate = self
            return cell
        case 3:
            let cell: ProfileEditCell = tableView.dequeueReusableCell(withIdentifier: "editCell", for: indexPath) as! ProfileEditCell
            cell.setup(type: .lastname)
            self.surnameTextField = cell.editItemTextField
            self.surnameTextField?.delegate = self
            return cell
        case 4:
            let cell: ProfileEditCell = tableView.dequeueReusableCell(withIdentifier: "editCell", for: indexPath) as! ProfileEditCell
            cell.setup(type: .sex)
            self.sexTextField = cell.editItemTextField
            self.sexTextField?.delegate = self
            self.sexTextField?.addTarget(self, action: #selector(textField(_:shouldChangeCharactersIn:replacementString:)), for: .editingChanged)
            return cell
        case 5:
            let cell: ProfileEditCell = tableView.dequeueReusableCell(withIdentifier: "editCell", for: indexPath) as! ProfileEditCell
            cell.setup(type: .birthdate)
            self.birthdateTextField = cell.editItemTextField
            self.birthdateTextField?.delegate = self
            self.birthdateTextField?.addTarget(self, action: #selector(textField(_:shouldChangeCharactersIn:replacementString:)), for: .editingChanged)
            return cell
        default:
            return UITableViewCell()
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: false)
        
        if(indexPath.row == 1) {
            let cell: ProfileEditStatusCell = tableView.cellForRow(at: indexPath) as! ProfileEditStatusCell
            cell.statusTextView.becomeFirstResponder()
        } else if(indexPath.row > 1) {
            let cell: ProfileEditCell = tableView.cellForRow(at: indexPath) as! ProfileEditCell
            cell.editItemTextField.becomeFirstResponder()
        }
    }
    
    func tableView(_ tableView: UITableView, shouldHighlightRowAt indexPath: IndexPath) -> Bool {
        return true
    }
    
    func tableView(_ tableView: UITableView, didHighlightRowAt indexPath: IndexPath) {
        switch indexPath.row {
        case 0:
            break
        case 1:
            let cell: ProfileEditStatusCell = tableView.dequeueReusableCell(withIdentifier: "editStatusCell", for: indexPath) as! ProfileEditStatusCell
            cell.backgroundColor = UIColor.clear
            cell.setBorderColor(color: UIHelpers.green())
            break
        case 2:
            let cell: ProfileEditCell = tableView.dequeueReusableCell(withIdentifier: "editCell", for: indexPath) as! ProfileEditCell
            cell.backgroundColor = UIColor.clear
            cell.setBorderColor(color: UIHelpers.green())
            break
        case 3:
            let cell: ProfileEditCell = tableView.dequeueReusableCell(withIdentifier: "editCell", for: indexPath) as! ProfileEditCell
            cell.backgroundColor = UIColor.clear
            cell.setBorderColor(color: UIHelpers.green())
            break
        case 4:
            let cell: ProfileEditCell = tableView.dequeueReusableCell(withIdentifier: "editCell", for: indexPath) as! ProfileEditCell
            cell.backgroundColor = UIColor.clear
            cell.setBorderColor(color: UIHelpers.green())
            break
        case 5:
            let cell: ProfileEditCell = tableView.dequeueReusableCell(withIdentifier: "editCell", for: indexPath) as! ProfileEditCell
            cell.backgroundColor = UIColor.clear
            cell.setBorderColor(color: UIHelpers.green())
            break
        default:
            break
        }
    }
    
    func tableView(_ tableView: UITableView, didUnhighlightRowAt indexPath: IndexPath) {
        switch indexPath.row {
        case 0:
            break
        case 1:
            let cell: ProfileEditStatusCell = tableView.dequeueReusableCell(withIdentifier: "editStatusCell", for: indexPath) as! ProfileEditStatusCell
            cell.backgroundColor = UIColor.clear
            cell.setBorderColor(color: UIHelpers.gray())
            break
        case 2:
            let cell: ProfileEditCell = tableView.dequeueReusableCell(withIdentifier: "editCell", for: indexPath) as! ProfileEditCell
            cell.backgroundColor = UIColor.clear
            cell.setBorderColor(color: UIHelpers.gray())
            break
        case 3:
            let cell: ProfileEditCell = tableView.dequeueReusableCell(withIdentifier: "editCell", for: indexPath) as! ProfileEditCell
            cell.backgroundColor = UIColor.clear
            cell.setBorderColor(color: UIHelpers.gray())
            break
        case 4:
            let cell: ProfileEditCell = tableView.dequeueReusableCell(withIdentifier: "editCell", for: indexPath) as! ProfileEditCell
            cell.backgroundColor = UIColor.clear
            cell.setBorderColor(color: UIHelpers.gray())
            break
        case 5:
            let cell: ProfileEditCell = tableView.dequeueReusableCell(withIdentifier: "editCell", for: indexPath) as! ProfileEditCell
            cell.backgroundColor = UIColor.clear
            cell.setBorderColor(color: UIHelpers.gray())
            break
        default:
            break
        }
    }
    
    // MARK: Nav Bar Button Actions
    
    @IBAction func onCancel(_ sender: AnyObject) {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func onSave(_ sender: AnyObject) {
        saveRoutine()
    }
    
    // MARK: Text View Delegate
    
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        self.saveButton.isEnabled = true
        
        self.tableView.beginUpdates()
        self.tableView.endUpdates()
        
        if(text.characters.count == 0) {
            if(textView.text.characters.count != 0) {
                return true
            }
        } else if(textView.text.characters.count > ProfileEditStatusCell.maximumCount - 1) {
            return false
        }
        return true
    }
    
    func textViewDidChange(_ textView: UITextView) {
        let cell: ProfileEditStatusCell = self.tableView.cellForRow(at: IndexPath.init(row: 1, section: 0)) as! ProfileEditStatusCell
        cell.updateCounter()
    }
    
    // MARK: Text Field Delegate
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if(textField == self.firstnameTextField) {
            self.surnameTextField?.becomeFirstResponder()
        } else if(textField == self.surnameTextField) {
            self.sexTextField?.becomeFirstResponder()
        } else if(textField == sexTextField) {
            self.birthdateTextField?.becomeFirstResponder()
        }
        return true
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        self.saveButton.isEnabled = true
        return true
    }
    
    // MARK: Keyboard
    
    func setUpKeyboard() {
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardChangeFrame), name:NSNotification.Name.UIKeyboardWillChangeFrame, object: nil)
    }
    
    func keyboardChangeFrame(notification: Notification) {
        let keyboardFrameEnd = (notification.userInfo?[UIKeyboardFrameEndUserInfoKey] as! NSValue).cgRectValue
        
        let hide: Bool = keyboardFrameEnd.origin.y == UIScreen.main.bounds.size.height
        
        let keyboardHeight = keyboardFrameEnd.size.height
        
        let keyboardAnimationDuration = notification.userInfo?[UIKeyboardAnimationDurationUserInfoKey] as! CGFloat
        
        let additionalOffset: CGFloat = (self.birthdateTextField!.isFirstResponder && UIScreen.main.bounds.width == 320) == true ? 64.0 : 0.0
        let offset = keyboardHeight / 2 + additionalOffset
        
        let const: CGFloat = hide == true ? offset : -offset
        if(self.keyboardShown == hide) {
            self.tableToTop.constant += const
            self.keyboardShown = !hide
        }
        
        UIView.animate(withDuration: TimeInterval(keyboardAnimationDuration)) {
            self.view.layoutIfNeeded()
        }
    }
    
    // MARK: Edit Save Button Protocol
    
    func setSaveButtonEnabled(isEnabled: Bool) {
        self.saveButton.isEnabled = isEnabled
    }
}
